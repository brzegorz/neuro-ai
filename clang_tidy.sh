python run-clang-tidy.py -fix -p . -header-filter='inc/*' -quiet -checks='\
performance-*, \
cppcoreguidelines-*,
modernize-*,
portability-*,
cert-*,
clang-analyzer-*,
misc-*,
readability-redundant-control-flow,
llvm-include-order,
google-runtime-int,
readability-convert-member-functions-to-static,
-clang-analyzer-osx-*
-cppcoreguidelines-special-member-functions,
-cppcoreguidelines-avoid-magic-numbers, 
-cppcoreguidelines-non-private-member-variables-in-classes, 
-cppcoreguidelines-explicit-virtual-functions, 
-misc-non-private-member-variables-in-classes, 
' | egrep '(warning|note|error)'

