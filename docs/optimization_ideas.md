##### General:
    - Find a way to copy collections of unique pointers more efficiently
    - Tiles are unique pointers. Wouldn't plain objects and references be better?
##### Army:
    - Switch from vector to array, as army size should be known upfront
##### Board:
    - Eliminate tile_indexes cache
    - Execute validity check through arithmetic operations instead of a lookup
##### Player:
    - Assign unique numbers to players and use those for comparisons instead of
      names
##### Monte Carlo Tree Search:
    - The pointer-based, recursive structure seems problematic. E.g. I don't
      know how to preallocate memory. Most of the time is spent on object
      construction
###### UsageFunctions and BattleActions:
    - Finding architecture in which those don't create virtual function calls
      should be beneficial.
