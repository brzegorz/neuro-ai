#Compiler and Linker
CC          := g++

#The Target Binary Program
TARGET      := neuro-ai
TEST		:= neuro-ai-tests
#src and inc subfolders
MODULES		:= tile_implementation \
			   tile_implementation/battle_actions  \
			   tile_implementation/usage_functions \
			   tile_implementation/passive_bonuses \
			   tile_implementation/innate_functions \
			   player_implementation \
			   player_implementation/player_logic \
			   game_flow \
			   game_flow/game_states \
			   guis monte_carlo_trees  tests \
			   utils army_builders  \
			   
#The Directories, Source, Includes, Objects, Binary
SRCDIR      := src
SRCTESTDIR  := srctests
INCDIR      := inc
BUILDDIR    := obj
PROFILEDIR  := profile
TARGETDIR   := bin
TESTDIR		:= bintests
SRCEXT      := cpp
DEPEXT      := d
OBJEXT      := o

#Flags, Libraries and Includes
INC         := $(addprefix -I $(INCDIR)/,$(MODULES))		\
			   -I $(INCDIR) 								\
			   -I /usr/local/include 
LIB         := -lm -flto
CXXFLAGS    := -std=c++17 -Wall -Wextra -pedantic $(INC) -Wno-unused-parameter \
			   -g -fdiagnostics-color -O3
			   			   
SOURCES_TARGET := $(shell find $(SRCDIR)  -type f -name *.$(SRCEXT))
SOURCES_COMMON := $(filter-out src/main.cpp,$(SOURCES_TARGET))
SOURCES_TEST   := $(SOURCES_COMMON)
SOURCES_TEST   += $(shell find $(SRCTESTDIR)  -type f -name *.$(SRCEXT))

OBJECTS_TARGET := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES_TARGET:.$(SRCEXT)=.$(OBJEXT)))
OBJECTS_TEST   := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES_TEST:.$(SRCEXT)=.$(OBJEXT)))	

#Defauilt Make
all: directories ${TARGET} $(TEST)
#Make the Directories
directories:
	@mkdir -p $(TARGETDIR)
	@mkdir -p $(BUILDDIR)
	@mkdir -p $(TESTDIR)
	@mkdir -p $(PROFILEDIR)

test: all
	./${TESTDIR}/${TEST}
	 
run: all
	./${TARGETDIR}/${TARGET}

clean:
	@$(RM) -rf $(BUILDDIR)

#Full Clean, Objects and Binaries
cleaner: clean
	@$(RM) -rf $(TARGETDIR)
	@$(RM) -rf $(TESTDIR)
	@$(RM) -rf $(PROFILEDIR)    

profile-generate: cleaner
profile-generate: CXXFLAGS := $(CXXFLAGS) -fprofile-generate=./$(PROFILEDIR)
profile-generate: LIB += -lgcov --coverage 
profile-generate: run

profile-utilize: clean
profile-utilize: CXXFLAGS := $(CXXFLAGS) -fprofile-use=./$(PROFILEDIR)
profile-utilize: LIB += --coverage
profile-utilize: all

$(TARGET): $(OBJECTS_TARGET)
	$(CC) -o $(TARGETDIR)/$(TARGET) $^ $(LIB)
	
$(TEST): $(OBJECTS_TEST)
	$(CC) -o $(TESTDIR)/$(TEST) $^ $(LIB)
	
#Compile target
$(BUILDDIR)/%.$(OBJEXT): $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(dir $@)x
	$(CC) $(CXXFLAGS) -c -o $@ $<

#Non-File Targets
.PHONY: all clean cleaner run test profile
