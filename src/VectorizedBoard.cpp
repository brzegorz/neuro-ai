
#include <Army.h>
#include <Game.h>
#include <Player.h>
#include <Tile.h>
#include <VectorizedBoard.h>

#include <iostream>
#include <iterator>

using namespace std;

VectorizedBoard::VectorizedBoard(Game* game)
    : indexes_map{}
{
    const GameData& game_data = game->get_data();
    add_player_tiles(game_data.get_active_player());
    add_player_tiles(game_data.get_previous_player());
    TILES_COUNT = tile_names.size();
    VECTOR_SIZE = TILES_COUNT * TILE_SIZE + DEPENDENT_VAR_SIZE;
    vectorized_game = vector<int>(VECTOR_SIZE);
}

void
VectorizedBoard::add_player_tiles(Player* player)
{
    int id = player->id;
    Army* army = player->get_resources()->get_army();
    for (string s : army->get_tile_names()) {
        s = to_string(id) + '_' + s;
        tile_names.insert(s);
    }
    unique_ptr<Tile> headquarter = army->get_headquarter();
    tile_names.insert(to_string(id) + '_' + headquarter->get_name());
    army->set_headquarter(move(headquarter));
}

VectorizedBoard::VectorizedBoard(const VectorizedBoard& other)
    : TILES_COUNT{ other.TILES_COUNT }
    , VECTOR_SIZE{ other.VECTOR_SIZE }
    , vectorized_game{ other.vectorized_game }
    , tile_names{ other.tile_names }
    , indexes_map{}
{}

void
VectorizedBoard::add_tile(Coordinates coords, Tile* tile)
{
    int starting_index = get_tile_index(tile);
    vectorized_game.at(starting_index) = coords.x;
    vectorized_game.at(starting_index + 1) = coords.y;
    vectorized_game.at(starting_index + 2) = (int)tile->get_direction();
    vectorized_game.at(starting_index + 3) = tile->get_hp();
}

void
VectorizedBoard::delete_tile(Tile* tile)
{
    int starting_index = get_tile_index(tile);
    int finishing_index = starting_index + TILE_SIZE;
    for (int i = starting_index; i < finishing_index; i++) {
        vectorized_game.at(i) = 0;
    }
}

auto
VectorizedBoard::get_tile_index(Tile* tile) -> int
{
    const auto& cached_index = indexes_map.find(tile->get_name());
    if (cached_index != indexes_map.end())
        return (*cached_index).second;
    int id = tile->get_owner()->id;
    const string tile_name = to_string(id) + "_" + tile->get_name();
    int tile_position =
        distance(tile_names.begin(), tile_names.find(tile_name));
    int index = tile_position * TILE_SIZE;
    indexes_map[tile->get_name()] = index;
    return index;
}

auto
VectorizedBoard::get_csv_header() const -> const string
{
    string result{ "" };
    for (auto const& tile_name : tile_names) {
        result += make_csv_field(tile_name, "x");
        result += make_csv_field(tile_name, "y");
        result += make_csv_field(tile_name, "rotation");
        result += make_csv_field(tile_name, "hp");
    }
    result += "winner";
    return result;
}

auto
VectorizedBoard::get_csv_row() const -> string
{
    string result = "";
    for (int i = 0; i < (int)vectorized_game.size() - 1; i++) {
        result += to_string(vectorized_game.at(i));
        result += ", ";
    }
    result += to_string(vectorized_game.at(vectorized_game.size() - 1));
    return result;
}

auto
VectorizedBoard::make_csv_field(const string& tile_name,
                                const string& field_name) -> const string
{
    string result = "";
    result += tile_name;
    result += "_";
    result += field_name;
    result += ", ";
    return result;
}

void
VectorizedBoard::add_winner(Player* winner)
{
    if (winner)
        vectorized_game.at(TILES_COUNT * TILE_SIZE) = winner->id;
    else
        vectorized_game.at(TILES_COUNT * TILE_SIZE) = -1;
}
