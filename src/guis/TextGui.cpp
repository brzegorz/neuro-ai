#include <BattleAction.h>
#include <Board.h>
#include <Colours.h>
#include <Coordinates.h>
#include <Gui.h>
#include <Player.h>
#include <PlayerResources.h>
#include <TextGui.h>
#include <Tile.h>
#include <Tile_graphic_parameters.h>

#include <algorithm>
#include <array>
#include <cmath>
#include <cstdint>
#include <regex>
#include <set>
#include <string>
#include <vector>
#include <iterator>

using namespace std;

void
TextGui::print_board(const Board& board)
{
    construct_ascii_vector(board);
    // Print the board
    string board_str = "\n";
    for (uint64_t y = 0; y < ascii_vec.size(); y++) {
        for (uint64_t x = 0; x < ascii_vec.at(0).size(); x++) {
            board_str += ascii_vec.at(y).at(x);
        }
        board_str += "\n";
    }
    cout << board_str;
}

void
TextGui::print_player_hand(Player* player)
{
    int player_hand_size = player->get_resources()->get_hand_size();
    vector<unique_ptr<Tile>> player_hand{};
    int i = 0;
    while (i < player_hand_size) {
        player_hand.push_back(player->get_resources()->pick_tile_from_hand(0));
        i++;
    }

    unique_ptr<Board> hand_gui = make_unique<Board>(Board::HAND);
    set<Coordinates> hand_gui_indexes{};
    for (auto coords : hand_gui->get_tile_indexes()) {
        hand_gui_indexes.insert(coords);
    }
    auto indexes_it = begin(hand_gui_indexes);
    auto tiles_it = begin(player_hand);
    auto tiles_it_end = end(player_hand);
    Coordinates current_coords;
    while (tiles_it != tiles_it_end and indexes_it != end(hand_gui_indexes)) {
        current_coords = *indexes_it;
        hand_gui->set_tile(current_coords, make_unique<Tile>(**tiles_it));
        hand_gui->get_tile_ptr(current_coords)
            ->reverse_passive_bonuses(*hand_gui, current_coords);
        ++indexes_it;
        ++tiles_it;
    }
    this->print_board(*hand_gui.get());

    for (int i = 0; i < player_hand_size; i++)
        player->get_resources()->receive_tile(move(player_hand.at(i)));
}

void
TextGui::print_game_result(Player* winner)
{
    if (winner)
        cout << "The winner is " << winner->get_colour() << winner->get_name()
             << COLOURS.NEUTRAL << endl;
    else
        cout << "Everybody lost" << endl;
}

void
TextGui::print_progress_bar(int current, int max) const
{
    const char PBSTR[]{
        "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
    };
    const int PBWIDTH = 20;
    double percentage = (double)current / max;
    int val = (int)(percentage * 100);
    int lpad = (int)(percentage * PBWIDTH);
    int rpad = PBWIDTH - lpad;
    printf("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
    fflush(stdout);
}

void
TextGui::add_text_at_particular_hex(const tile_parameters& p,
                                    int x,
                                    int y,
                                    const string& text)
{
    ascii_vec.at(p.start_y + y).at(p.start_x + x) = p.colour + text;
}

void
TextGui::construct_ascii_vector(const Board& board)
{
    initialize_ascii_vector(board);
    for (auto& t : board.get_tile_indexes()) {
        int start_x = board_x_to_ascii_x(t.x);
        int start_y = board_y_to_ascii_y(t.y);
        tile_parameters p = { TextGui::tile_width,
                              TextGui::tile_height,
                              start_x,
                              start_y,
                              COLOURS.NEUTRAL };
        add_tile_borders(p);
        Tile* tile = board.get_tile_ptr(t);
        if (tile)
            add_tile_ascii(tile, p);
    }
}

void
TextGui::initialize_ascii_vector(const Board& board)
{
    int TextGui_width =
        board.width * (tile_width + tile_height + 1) + tile_height + 1;
    int TextGui_height = board.height * (tile_height + 1) + 5;
    ascii_vec = vector<vector<string>>(TextGui_height);
    for (auto& i : ascii_vec)
        i.resize(TextGui_width);
    for (uint64_t y = 0; y < ascii_vec.size(); y++) {
        for (uint64_t x = 0; x < ascii_vec.at(0).size(); x++)
            ascii_vec.at(y).at(x) = " ";
    }
    add_coords_axes(board, TextGui_width, TextGui_height);
}

void
TextGui::add_coords_axes(const Board& board, int gui_width, int /*gui_height*/)
{
    const unordered_set<Coordinates>& indexes = board.get_tile_indexes();
    set<int> x_indexes{};
    set<int> y_indexes{};
    for (Coordinates index : indexes) {
        x_indexes.insert(index.x);
        y_indexes.insert(index.y);
    }
    for (int x : x_indexes) {
        int x_coord =
            tile_height + tile_width / 2 + x * (tile_height + tile_width + 0.5);
        ascii_vec.at(0).at(x_coord) = COLOURS.NEUTRAL + to_string(x);
        //~ int median_x = x_indexes.size()/2;
        //~ for(int y = 1; y <= abs(median_x - x) * tile_height; y++){
        //~ ascii_vec.at(y).at(x_coord) = "|";
        //~ }
        //~ for(int y = gui_height - 2;
        //~ y >= gui_height - abs(median_x - x) * tile_height;
        //~ y--){
        //~ ascii_vec.at(y).at(x_coord) = "|";
        //~ }
        //~ ascii_vec.at(gui_height - 1).at(x_coord) = to_string(x);
    }
    for (int y : y_indexes) {
        int y_coord = 1.5 * tile_height + y * (tile_height) + y / 2;
        int x_coord = y % 2 * (gui_width - 1);
        ascii_vec.at(y_coord).at(x_coord) = COLOURS.NEUTRAL + to_string(y);
        //~ ascii_vec.at(y_coord).at(gui_width - 1) = to_string(y);
    }
}

void
TextGui::add_tile_borders(const tile_parameters& p)
{
    for (DIRECTION direction : all_directions) {
        add_border(direction, p);
    }
}

void
TextGui::add_tile_ascii(Tile* tile, tile_parameters p)
{
    for (DIRECTION direction : all_directions) {
        DIRECTION armour_direction = direction;
        if (tile->is_armoured(armour_direction)) {
            p.colour = COLOURS.ARMOUR;
            add_border(armour_direction, p);
        }
    }
    p.colour = tile->get_owner()->get_colour();
    if (tile->is_in_net()) {
        p.colour = p.colour + COLOURS.WEB;
        add_tile_borders(p);
    }
    add_tile_hp(p, tile);
    add_tile_initiative(p, tile);
    add_tile_name(p, tile);
    add_tile_passives(p, tile);
    add_tile_innate(p, tile);
    add_tile_medic(p, tile);
    tile->update_ascii_gui(p, this);
}

void
TextGui::add_tile_hp(const tile_parameters& p, Tile* tile)
{
    int unit_hp = tile->get_hp();
    string unit_hp_str = to_string(unit_hp);
    int hp_x = (p.tile_width + 2 * p.tile_height) / 2;
    int hp_y = (2 * p.tile_height) / 2 + 2;
    if (unit_hp > 0) {
        add_text_at_particular_hex(p, hp_x, hp_y, "H");
        add_text_at_particular_hex(p, hp_x + 1, hp_y, "P");
        add_text_at_particular_hex(p, hp_x + 2, hp_y, ":");
        add_text_at_particular_hex(p, hp_x + 3, hp_y, unit_hp_str);
        for (unsigned int i = 1; i < unit_hp_str.length(); i++)
            add_text_at_particular_hex(p, hp_x + 3 + i, hp_y, "");
    }
}

void
TextGui::add_tile_initiative(const tile_parameters& p, Tile* tile)
{
    const set<int>& unit_initiative = tile->get_initiative();
    int init_x = (p.tile_width + 2 * p.tile_height) / 2;
    int init_y = (2 * p.tile_height) / 2 + 1;
    if (unit_initiative.size() > 0) {
        add_text_at_particular_hex(p, init_x, init_y, "I");
        add_text_at_particular_hex(p, init_x + 1, init_y, ":");
        int i = 0;
        for (auto init = unit_initiative.rbegin();
             init != unit_initiative.rend();
             ++init) {
            string initiative_str = to_string(*init);
            add_text_at_particular_hex(
                p, init_x + 2 + i, init_y, initiative_str);
            i++;
        }
    }
}

void
TextGui::add_tile_name(const tile_parameters& p, Tile* tile)
{
    if (not tile->is_name_visible)
        return;
    string tile_name = tile->get_name();
    regex e(".*_[0-9]$");
    if (regex_match(tile_name, e))
        tile_name.resize(tile_name.size() - 2);
    int name_x = (p.tile_width + 2 * p.tile_height) / 2 - tile_name.size() / 3;
    int name_y = (2 * p.tile_height) / 2 + 2;
    int shift = 0;
    string letter;
    for (const char& c : tile_name) {
        letter = string(&c);
        if (letter.size() > 1)
            letter.resize(1);
        add_text_at_particular_hex(p, name_x + shift, name_y, letter);
        ++shift;
    }
}

void
TextGui::add_tile_passives(const tile_parameters& p, Tile* tile)
{
    string passives_string = tile->get_passives_string();
    int passives_x =
        (p.tile_width + 2 * p.tile_height) / 2 - passives_string.size() / 3;
    int passives_y = (2 * p.tile_height) / 2 + 3;
    if (tile->get_initiative().empty())
        passives_y = (2 * p.tile_height) / 2 + 1;
    int shift = 0;
    for (const char& c : passives_string) {
        string letter = string(&c);
        if (letter.size() > 1)
            letter.resize(1);
        add_text_at_particular_hex(p, passives_x + shift, passives_y, letter);
        ++shift;
    }
}

void
TextGui::add_tile_innate(const tile_parameters& p, Tile* tile)
{
    string innate_string = tile->get_innate_function_string();
    int innate_x = p.tile_height * 2;
    int innate_y = (2 * p.tile_height) / 2 + 3;
    int shift = 0;
    for (const char& c : innate_string) {
        string letter = string(&c);
        if (letter.size() > 1)
            letter.resize(1);
        add_text_at_particular_hex(p, innate_x + shift, innate_y, letter);
        ++shift;
    }
}

void
TextGui::add_tile_medic(const tile_parameters& p, Tile* tile)
{
    for (DIRECTION dir : all_directions)
        if (tile->is_medic(dir))
            add_single_medic_direction(p, dir);
}

void
TextGui::add_single_medic_direction(const tile_parameters& p, DIRECTION dir)
{
    int x1;
    int x2;
    int y1;
    int y2;
    string ascii_symbol = "+";
    switch (dir) {
        case N:
            x1 = p.tile_height + p.tile_width / 2 + 2;
            y1 = 2;
            x2 = x1;
            y2 = y1 + 1;
            break;
        case S:
            x1 = p.tile_height + p.tile_width / 2 + 2;
            y1 = 2 * p.tile_height;
            x2 = x1;
            y2 = y1 - 1;
            break;
        case NE:
            x1 = p.tile_width + 2 * p.tile_height - 2;
            y1 = p.tile_height;
            x2 = x1 - 1;
            y2 = y1 + 1;
            break;
        case SW:
            x1 = p.tile_height;
            y1 = 2 * p.tile_height - 1;
            x2 = x1 + 1;
            y2 = y1 - 1;
            break;
        case NW:
            x1 = p.tile_height;
            y1 = p.tile_height;
            x2 = x1 + 1;
            y2 = y1 + 1;
            break;
        case SE:
            x1 = p.tile_width + p.tile_height + 2;
            y1 = 2 * p.tile_height - 1;
            x2 = x1 - 1;
            y2 = y1 - 1;
            break;
        default:
            throw invalid_argument("Invalid direction specified");
    }
    add_text_at_particular_hex(p, x1, y1, ascii_symbol);
    add_text_at_particular_hex(p, x2, y2, ascii_symbol);
}

void
TextGui::add_border(DIRECTION border_direction, const tile_parameters& p)
{
    switch (border_direction) {
        case N:
            add_border_north(p);
            break;
        case NE:
            add_border_north_east(p);
            break;
        case SE:
            add_border_south_east(p);
            break;
        case S:
            add_border_south(p);
            break;
        case SW:
            add_border_south_west(p);
            break;
        case NW:
            add_border_north_west(p);
            break;
        default:
            throw invalid_argument("Invalid direction specified");
    }
}

void
TextGui::add_border_north_east(const tile_parameters& p)
{
    vector<int> backslash_x;
    vector<int> backslash_y;
    for (int x = p.tile_width + p.tile_height + 1;
         x < p.tile_width + 2 * p.tile_height + 1;
         x++)
        backslash_x.push_back(x);

    for (int y = 2; y < p.tile_height + 2; y++)
        backslash_y.push_back(y);
    int board_index_x = ascii_x_to_board_x(p.start_x);
    int board_index_y = ascii_y_to_board_y(p.start_y);
    bool is_right_border = false;
    if ((board_index_x == 2 and board_index_y == 0) or
        (board_index_x == 3 and board_index_y == 1) or board_index_x == 4)
        is_right_border = true;
    for (unsigned int i = 0; i < backslash_x.size(); i++) {
        add_text_at_particular_hex(p, backslash_x[i], backslash_y[i], "\\");
        if (is_right_border)
            add_text_at_particular_hex(
                p, backslash_x[i] + 1, backslash_y[i], COLOURS.NEUTRAL + " ");
    }
}
void
TextGui::add_border_south_west(const tile_parameters& p)
{
    vector<int> backslash_x;
    vector<int> backslash_y;
    for (int x = 1; x < p.tile_height + 1; x++)
        backslash_x.push_back(x);

    for (int y = p.tile_height + 2; y < p.tile_height * 2 + 3; y++)
        backslash_y.push_back(y);

    for (unsigned int i = 0; i < backslash_x.size(); i++) {
        add_text_at_particular_hex(p, backslash_x[i], backslash_y[i], "\\");
    }
}

void
TextGui::add_border_north_west(const tile_parameters& p)
{
    vector<int> slash_x;
    vector<int> slash_y;
    for (int x = p.tile_height; x > 0; x--)
        slash_x.push_back(x);

    for (int y = 2; y < p.tile_height + 2; y++)
        slash_y.push_back(y);

    for (unsigned int i = 0; i < slash_x.size(); i++) {
        add_text_at_particular_hex(p, slash_x[i], slash_y[i], "/");
    }
}

void
TextGui::add_border_south_east(const tile_parameters& p)
{
    vector<int> slash_x;
    vector<int> slash_y;

    for (int x = p.tile_width + 2 * p.tile_height;
         x > p.tile_height + p.tile_width;
         x--)
        slash_x.push_back(x);

    for (int y = p.tile_height + 2; y < p.tile_height * 2 + 3; y++)
        slash_y.push_back(y);
    int board_index_x = ascii_x_to_board_x(p.start_x);
    int board_index_y = ascii_y_to_board_y(p.start_y);
    bool is_right_border = false;
    if ((board_index_x == 2 and board_index_y == 8) or
        (board_index_x == 3 and board_index_y == 7) or board_index_x == 4)
        is_right_border = true;
    for (unsigned int i = 0; i < slash_x.size(); i++) {
        add_text_at_particular_hex(p, slash_x[i], slash_y[i], "/");
        if (is_right_border)
            add_text_at_particular_hex(
                p, slash_x[i] + 1, slash_y[i], COLOURS.NEUTRAL + " ");
    }
}

void
TextGui::add_border_north(const tile_parameters& p)
{
    vector<int> undescore_x;
    vector<int> underscore_y;
    for (int x = p.tile_height + 1; x < p.tile_height + p.tile_width + 1; x++)
        undescore_x.push_back(x);
    underscore_y.reserve(p.tile_width);

    for (int y = 0; y < p.tile_width; y++)
        underscore_y.push_back(1);
    for (unsigned int i = 0; i < undescore_x.size(); i++) {
        add_text_at_particular_hex(p, undescore_x[i], underscore_y[i], "_");
    }
    add_text_at_particular_hex(p,
                               undescore_x[undescore_x.size() - 1] + 1,
                               underscore_y[0],
                               COLOURS.NEUTRAL + " ");
}
void
TextGui::add_border_south(const tile_parameters& p)
{
    vector<int> undescore_x;
    vector<int> underscore_y;
    for (int x = p.tile_height + 1; x < p.tile_height + p.tile_width + 1; x++)
        undescore_x.push_back(x);
    underscore_y.reserve(p.tile_width);

    for (int y = 0; y < p.tile_width; y++)
        underscore_y.push_back(2 * p.tile_height + 1);
    for (unsigned int i = 0; i < undescore_x.size(); i++) {
        add_text_at_particular_hex(p, undescore_x[i], underscore_y[i], "_");
    }
}

auto
TextGui::board_x_to_ascii_x(int board_x) -> int
{
    return board_x * (TextGui::tile_width + TextGui::tile_height + 1);
}

auto
TextGui::board_y_to_ascii_y(int board_y) -> int
{
    return board_y * TextGui::tile_height + board_y / 2;
}

auto
TextGui::ascii_x_to_board_x(int ascii_x) -> int
{
    return ascii_x / (TextGui::tile_width + TextGui::tile_height + 1);
}

auto
TextGui::ascii_y_to_board_y(int ascii_y) -> int
{
    return ascii_y / (TextGui::tile_height + 0.5);
}

auto
TextGui::get_numeric_input(string question, const set<int>& valid_values) const
    -> int
{
    stringstream valid_values_sstream;
    copy(valid_values.begin(),
         valid_values.end(),
         ostream_iterator<int>(valid_values_sstream, " "));
    question = question + " Options: " + valid_values_sstream.str();
    int result;
    bool is_x_valid;
    if (valid_values.size() == 1)
    {
        result = *valid_values.cbegin();
        cout << "Picking the only available option automatically. Picked " << result << endl;
        return result;
    }

    do {
        cout << question << endl;
        cin >> result;
        if (not cin) {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            is_x_valid = false;
        } else {
            is_x_valid =
                find(valid_values.begin(), valid_values.end(), result) !=
                valid_values.end();
        }
    } while (not is_x_valid);

    return result;
}

auto
TextGui::get_direction(string direction_question,
                       const set<DIRECTION>& valid_directions) const
    -> DIRECTION
{
    string chosen_rotation;
    map<string, DIRECTION> directions{
        make_pair("N", N), make_pair("NE", NE), make_pair("SE", SE),
        make_pair("S", S), make_pair("SW", SW), make_pair("NW", NW)
    };
    direction_question += " Options: ";
    auto directions_it = directions.begin();
    const auto& directions_end = directions.end();
    while (directions_it != directions_end) {
        auto current_element = directions_it++;
        if (valid_directions.find(current_element->second) ==
            valid_directions.end())
            directions.erase(current_element->first);
        else
            direction_question += " " + current_element->first;
    }
    direction_question += "\n";
    while (true) {
        cout << direction_question;
        cin >> chosen_rotation;
        transform(chosen_rotation.begin(),
                  chosen_rotation.end(),
                  chosen_rotation.begin(),
                  ::toupper);
        if (directions.find(chosen_rotation) != directions.end()) {
            return directions.at(chosen_rotation);
        }
    }
}

auto
TextGui::get_coords(std::string question,
                    const std::set<Coordinates>& valid_hexes) const
    -> Coordinates
{
    Coordinates result;
    bool is_coord_valid;
    int x;
    int y;
    cout << "Available tiles: ";
    int previous_x = -1;
    for (auto const& coords : valid_hexes) {
        if (coords.x != previous_x)
            cout << endl;
        cout << "(" << coords << ") ";
        previous_x = coords.x;
    }
    cout << endl;
    do {
        cout << question << endl;
        cin >> x >> y;
        result = Coordinates(x, y);
        if (not cin) {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            is_coord_valid = false;
        } else {
            is_coord_valid =
                find(valid_hexes.begin(), valid_hexes.end(), result) !=
                valid_hexes.end();
        }
    } while (not is_coord_valid);
    return result;
}
