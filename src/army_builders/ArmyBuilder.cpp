#include <ArmyBuilder.h>
#include <BattleAction.h>
#include <BattleInstant.h>
#include <BombInstant.h>
#include <Hit.h>
#include <Movement.h>
#include <Player.h>
#include <Shoot.h>
#include <Sniper.h>
#include <Tile.h>
#include <UsageFunction.h>

using namespace std;

auto
ArmyBuilder::make_movement_tile(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<Tile> movement_tile =
        make_unique<Tile>("MOVEMENT", owner, make_unique<Movement>());
    return movement_tile;
}

auto
ArmyBuilder::make_battle_tile(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<Tile> battle_tile =
        make_unique<Tile>("BATTLE", owner, make_unique<BattleInstant>());
    return battle_tile;
}

auto
ArmyBuilder::make_sniper_tile(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<Tile> unit =
        make_unique<Tile>("SNIPER", owner, make_unique<Sniper>());
    return unit;
}

auto
ArmyBuilder::make_bomb_tile(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<Tile> unit =
        make_unique<Tile>("BOMB", owner, make_unique<BombInstant>());
    return unit;
}
