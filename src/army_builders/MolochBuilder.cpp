#include <AllPassiveBonuses.h>
#include <Army.h>
#include <BattleAction.h>
#include <BattleInstant.h>
#include <Hit.h>
#include <MolochBuilder.h>
#include <Movement.h>
#include <Player.h>
#include <PushBack.h>
#include <Shoot.h>
#include <ShootGauss.h>
#include <Tile.h>
#include <UsageFunction.h>

using namespace std;

void
MolochBuilder::assign_army(Player* owner)
{
    vector<unique_ptr<Tile>> army(0);
    int i;
    // UNITS
    for (i = 0; i < 2; i++)
        army.push_back(make_blocker(owner));
    for (i = 0; i < 2; i++)
        army.push_back(make_hybrid(owner));
    army.push_back(make_gauss_cannon(owner));
    army.push_back(make_juggernaut(owner));
    for (i = 0; i < 2; i++)
        army.push_back(make_hunter_killer(owner));
    army.push_back(make_clown(owner));
    for (i = 0; i < 2; i++)
        army.push_back(make_armored_hunter(owner));
    army.push_back(make_armored_guard(owner));
    army.push_back(make_guard(owner));
    army.push_back(make_protector(owner));
    army.push_back(make_hornet(owner));
    army.push_back(make_net_fighter(owner));
    army.push_back(make_stormtrooper(owner));
    // // MODULES
    army.push_back(make_mother_module(owner));
    for (i = 0; i < 2; i++)
        army.push_back(make_medic(owner));
    army.push_back(make_brain(owner));
    army.push_back(make_officer(owner));
    army.push_back(make_scout(owner));
    // // INSTANTS
    army.push_back(make_bomb_tile(owner));
    for (i = 0; i < 4; i++)
        army.push_back(make_battle_tile(owner));
    for (i = 0; i < 1; i++)
        army.push_back(make_movement_tile(owner));
    for (i = 0; i < 5; i++)
        army.push_back(make_push_back(owner));
    unique_ptr<Army> army1 = make_unique<Army>(make_headquarter(owner), army);
    owner->get_resources()->set_army(move(army1));
}

auto
MolochBuilder::make_headquarter(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    unique_ptr<set<unique_ptr<PassiveBonus>>> bonuses =
        make_unique<set<unique_ptr<PassiveBonus>>>();
    for (DIRECTION direction : all_directions) {
        actions->insert(make_unique<Hit>(1, direction));
        bonuses->insert(make_unique<BonusStrengthShoot>(direction));
    }
    unique_ptr<Tile> headquarter = make_unique<Tile>(
        true, "HQ", owner, 20, set<int>{ 0 }, move(actions), move(bonuses));
    return headquarter;
}

auto
MolochBuilder::make_blocker(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<Tile> blocker =
        make_unique<Tile>(false,
                          "blocker",
                          owner,
                          3,
                          set<int>{},
                          make_unique<set<unique_ptr<BattleAction>>>(),
                          unordered_set<DIRECTION>{ N });
    return blocker;
}

auto
MolochBuilder::make_hybrid(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Shoot>(1, N));
    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "hybrid", owner, 1, set<int>{ 3 }, move(actions));
    return unit;
}

auto
MolochBuilder::make_gauss_cannon(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<ShootGauss>(1, N));
    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "gauss_cannon", owner, 2, set<int>{ 1 }, move(actions));
    return unit;
}

auto
MolochBuilder::make_juggernaut(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Shoot>(1, NE));
    actions->insert(make_unique<Hit>(2, N));
    unique_ptr<Tile> unit =
        make_unique<Tile>(false,
                          "juggernaut",
                          owner,
                          2,
                          set<int>{ 1 },
                          move(actions),
                          unordered_set<DIRECTION>{ N, SE, SW });
    return unit;
}

auto
MolochBuilder::make_hunter_killer(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Hit>(1, NW));
    actions->insert(make_unique<Hit>(1, N));
    actions->insert(make_unique<Hit>(1, NE));
    actions->insert(make_unique<Hit>(1, S));

    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "hunter_killer", owner, 1, set<int>{ 3 }, move(actions));
    return unit;
}

auto
MolochBuilder::make_clown(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Hit>(1, NW));
    actions->insert(make_unique<Hit>(1, N));
    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "clown", owner, 2, set<int>{ 2 }, move(actions));
    return unit;
}

auto
MolochBuilder::make_armored_hunter(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Hit>(1, NW));
    actions->insert(make_unique<Hit>(1, N));
    actions->insert(make_unique<Hit>(1, NE));
    actions->insert(make_unique<Hit>(1, SE));
    actions->insert(make_unique<Hit>(1, S));
    actions->insert(make_unique<Hit>(1, SW));
    unique_ptr<Tile> unit =
        make_unique<Tile>(false,
                          "armored_hunter",
                          owner,
                          1,
                          set<int>{ 2 },
                          move(actions),
                          unordered_set<DIRECTION>{ N, NW });
    return unit;
}

auto
MolochBuilder::make_armored_guard(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Shoot>(1, NW));
    actions->insert(make_unique<Shoot>(1, NE));
    unique_ptr<Tile> unit = make_unique<Tile>(false,
                                              "armored_guard",
                                              owner,
                                              1,
                                              set<int>{ 2 },
                                              move(actions),
                                              unordered_set<DIRECTION>{ N });
    return unit;
}

auto
MolochBuilder::make_guard(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Shoot>(1, N));
    actions->insert(make_unique<Shoot>(1, NW));
    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "guard", owner, 1, set<int>{ 2 }, move(actions));
    return unit;
}

auto
MolochBuilder::make_protector(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Shoot>(1, NW));
    actions->insert(make_unique<Shoot>(1, N));
    actions->insert(make_unique<Shoot>(1, NE));

    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "protector", owner, 2, set<int>{ 1 }, move(actions));
    return unit;
}

auto
MolochBuilder::make_hornet(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Hit>(2, N));
    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "hornet", owner, 1, set<int>{ 2 }, move(actions));
    return unit;
}

auto
MolochBuilder::make_net_fighter(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<PassiveBonus>>> bonuses =
        make_unique<set<unique_ptr<PassiveBonus>>>();
    bonuses->insert(make_unique<MalusNet>(N));
    bonuses->insert(make_unique<MalusNet>(NW));
    unique_ptr<Tile> unit =
        make_unique<Tile>(false, "net_fighter", owner, 1, move(bonuses));
    return unit;
}

auto
MolochBuilder::make_stormtrooper(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Shoot>(1, N));
    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "stormtrooper", owner, 2, set<int>{ 2, 1 }, move(actions));
    return unit;
}

auto
MolochBuilder::make_mother_module(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<PassiveBonus>>> bonuses =
        make_unique<set<unique_ptr<PassiveBonus>>>();
    bonuses->insert(make_unique<BonusAttack>(N));
    unique_ptr<Tile> unit =
        make_unique<Tile>(false, "mother_module", owner, 1, move(bonuses));
    return unit;
}

auto
MolochBuilder::make_medic(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<Tile> unit = make_unique<Tile>(false, "medic", owner, 1);
    unit->add_medic_direction(N);
    unit->add_medic_direction(SE);
    unit->add_medic_direction(SW);
    return unit;
}

auto
MolochBuilder::make_brain(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<PassiveBonus>>> bonuses =
        make_unique<set<unique_ptr<PassiveBonus>>>();
    bonuses->insert(make_unique<BonusStrengthShoot>(NW));
    bonuses->insert(make_unique<BonusStrengthShoot>(NE));
    bonuses->insert(make_unique<BonusStrengthShoot>(S));
    bonuses->insert(make_unique<BonusStrengthHit>(NW));
    bonuses->insert(make_unique<BonusStrengthHit>(NE));
    bonuses->insert(make_unique<BonusStrengthHit>(S));
    unique_ptr<Tile> unit =
        make_unique<Tile>(false, "the_brain", owner, 1, move(bonuses));
    return unit;
}

auto
MolochBuilder::make_officer(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<PassiveBonus>>> bonuses =
        make_unique<set<unique_ptr<PassiveBonus>>>();
    bonuses->insert(make_unique<BonusStrengthShoot>(N));
    bonuses->insert(make_unique<BonusStrengthShoot>(SE));
    bonuses->insert(make_unique<BonusStrengthShoot>(SW));
    unique_ptr<Tile> unit =
        make_unique<Tile>(false, "officer", owner, 1, move(bonuses));
    return unit;
}

auto
MolochBuilder::make_scout(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<PassiveBonus>>> bonuses =
        make_unique<set<unique_ptr<PassiveBonus>>>();
    bonuses->insert(make_unique<BonusInitiative>(N));
    bonuses->insert(make_unique<BonusInitiative>(SE));
    bonuses->insert(make_unique<BonusInitiative>(SW));
    unique_ptr<Tile> unit =
        make_unique<Tile>(false, "scout", owner, 1, move(bonuses));
    return unit;
}

auto
MolochBuilder::make_push_back(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<Tile> unit =
        make_unique<Tile>("PUSH_BACK", owner, make_unique<PushBack>());
    return unit;
}
