#include <AllPassiveBonuses.h>
#include <Army.h>
#include <BattleAction.h>
#include <BattleInstant.h>
#include <Hit.h>
#include <InnateMovement.h>
#include <Movement.h>
#include <OutpostBuilder.h>
#include <Player.h>
#include <Shoot.h>
#include <Tile.h>
#include <UsageFunction.h>

#include <iostream>

using namespace std;

void
OutpostBuilder::assign_army(Player* owner)
{
    vector<unique_ptr<Tile>> army(0);
    int i;
    // UNITS
    for (i = 0; i < 2; i++)
        army.push_back(make_runner(owner));
    army.push_back(make_HMG(owner));
    for (i = 0; i < 5; i++)
        army.push_back(make_commando(owner));
    for (i = 0; i < 2; i++)
        army.push_back(make_annihilator(owner));
    army.push_back(make_mobile_armor(owner));
    army.push_back(make_brawler(owner));
    // MODULES
    army.push_back(make_saboteur(owner));
    army.push_back(make_recon_center(owner));
    for (i = 0; i < 2; i++)
        army.push_back(make_medic(owner));
    army.push_back(make_officer(owner));
    army.push_back(make_scoper(owner));
    for (i = 0; i < 2; i++)
        army.push_back(make_scout(owner));
    // INSTANTS
    for (i = 0; i < 6; i++)
        army.push_back(make_battle_tile(owner));
    for (i = 0; i < 7; i++)
        army.push_back(make_movement_tile(owner));
    army.push_back(make_sniper_tile(owner));
    unique_ptr<Army> army1 = make_unique<Army>(make_headquarter(owner), army);
    owner->get_resources()->set_army(move(army1));
}

auto
OutpostBuilder::make_headquarter(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    unique_ptr<set<unique_ptr<PassiveBonus>>> bonuses =
        make_unique<set<unique_ptr<PassiveBonus>>>();
    for (DIRECTION direction : all_directions) {
        actions->insert(make_unique<Hit>(1, direction));
        bonuses->insert(make_unique<BonusAttack>(direction));
    }
    unique_ptr<Tile> headquarter = make_unique<Tile>(
        true, "HQ", owner, 20, set<int>{ 0 }, move(actions), move(bonuses));
    return headquarter;
}

auto
OutpostBuilder::make_runner(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Hit>(1, N));
    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "runner", owner, 1, set<int>{ 2 }, move(actions));
    unit->set_innate_function(make_unique<InnateMovement>());
    return unit;
}

auto
OutpostBuilder::make_HMG(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Shoot>(1, N));
    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "HMG", owner, 1, set<int>{ 2, 1 }, move(actions));
    return unit;
}

auto
OutpostBuilder::make_commando(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Shoot>(1, N));
    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "commando", owner, 1, set<int>{ 3 }, move(actions));
    return unit;
}

auto
OutpostBuilder::make_annihilator(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Shoot>(2, N));
    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "annihilator", owner, 1, set<int>{ 2 }, move(actions));
    return unit;
}

auto
OutpostBuilder::make_mobile_armor(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Shoot>(1, NW));
    actions->insert(make_unique<Hit>(2, N));
    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "mobile_armor", owner, 1, set<int>{ 3, 2 }, move(actions));
    unit->set_innate_function(make_unique<InnateMovement>());
    return unit;
}

auto
OutpostBuilder::make_brawler(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Hit>(2, N));
    unique_ptr<Tile> unit = make_unique<Tile>(
        false, "brawler", owner, 1, set<int>{ 3 }, move(actions));
    return unit;
}

auto
OutpostBuilder::make_saboteur(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<PassiveBonus>>> bonuses =
        make_unique<set<unique_ptr<PassiveBonus>>>();
    for (DIRECTION direction : all_directions) {
        bonuses->insert(make_unique<MalusInitiative>(direction));
    }
    unique_ptr<Tile> unit =
        make_unique<Tile>(false, "saboteur", owner, 1, move(bonuses));
    return unit;
}

auto
OutpostBuilder::make_recon_center(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<PassiveBonus>>> bonuses =
        make_unique<set<unique_ptr<PassiveBonus>>>();
    for (DIRECTION direction : all_directions) {
        bonuses->insert(make_unique<BonusReconCenter>(direction));
    }
    unique_ptr<Tile> unit =
        make_unique<Tile>(false, "recon_center", owner, 1, move(bonuses));

    return unit;
}

auto
OutpostBuilder::make_medic(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<Tile> unit = make_unique<Tile>(false, "medic", owner, 1);
    unit->add_medic_direction(N);
    unit->add_medic_direction(NE);
    unit->add_medic_direction(NW);
    return unit;
}

auto
OutpostBuilder::make_officer(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<PassiveBonus>>> bonuses =
        make_unique<set<unique_ptr<PassiveBonus>>>();
    for (DIRECTION direction : all_directions) {
        bonuses->insert(make_unique<BonusStrengthShoot>(direction));
    }
    unique_ptr<Tile> unit =
        make_unique<Tile>(false, "officer", owner, 1, move(bonuses));
    return unit;
}

auto
OutpostBuilder::make_scoper(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<PassiveBonus>>> bonuses =
        make_unique<set<unique_ptr<PassiveBonus>>>();
    for (DIRECTION direction : all_directions) {
        bonuses->insert(make_unique<MalusScoper>(direction));
    }
    unique_ptr<Tile> unit =
        make_unique<Tile>(false, "scoper", owner, 1, move(bonuses));
    return unit;
}

auto
OutpostBuilder::make_scout(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<PassiveBonus>>> bonuses =
        make_unique<set<unique_ptr<PassiveBonus>>>();
    bonuses->insert(make_unique<BonusInitiative>(N));
    bonuses->insert(make_unique<BonusInitiative>(SE));
    bonuses->insert(make_unique<BonusInitiative>(SW));
    unique_ptr<Tile> unit =
        make_unique<Tile>(false, "scout", owner, 1, move(bonuses));
    return unit;
}
