#include <Army.h>
#include <BattleAction.h>
#include <BattleInstant.h>
#include <Hit.h>
#include <Movement.h>
#include <Player.h>
#include <Shoot.h>
#include <TestArmyBuilder.h>
#include <Tile.h>
#include <UsageFunction.h>

using namespace std;

void
TestArmyBuilder::assign_army(Player* owner)
{
    const int army_size = 34;
    const int movement_tiles_count = 6;
    const int battle_tiles_count = 6;
    vector<unique_ptr<Tile>> army(0);
    for (int i = 0; i < army_size - movement_tiles_count - battle_tiles_count;
         i++) {
        army.push_back(make_shooter(owner));
    }
    for (int i = 0; i < movement_tiles_count; i++) {
        army.push_back(make_movement_tile(owner));
    }
    for (int i = 0; i < battle_tiles_count; i++) {
        army.push_back(make_battle_tile(owner));
    }
    unique_ptr<Army> army1 = make_unique<Army>(make_headquarter(owner), army);
    owner->get_resources()->set_army(move(army1));
}

void
TestArmyBuilder::assign_army_of_size_n(Player* owner, int n)
{
    const int army_size = n;
    vector<unique_ptr<Tile>> army(0);
    for (int i = 0; i < army_size; i++) {
        army.push_back(make_shooter(owner));
    }
    unique_ptr<Army> army1 = make_unique<Army>(make_headquarter(owner), army);
    owner->get_resources()->set_army(move(army1));
}

auto
TestArmyBuilder::make_headquarter(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    for (DIRECTION direction : all_directions)
        actions->insert(make_unique<Hit>(1, direction));
    unique_ptr<Tile> headquarter =
        make_unique<Tile>(true, "HQ", owner, 20, set<int>{ 0 }, move(actions));
    return headquarter;
}

auto
TestArmyBuilder::make_shooter(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<set<unique_ptr<BattleAction>>> actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    actions->insert(make_unique<Shoot>(1, N));
    unique_ptr<Tile> shooter = make_unique<Tile>(
        false, "shooter", owner, 1, set<int>{ 3 }, move(actions));
    return shooter;
}
