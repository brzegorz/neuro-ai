#include <Colours.h>
#include <Test.h>
#include <TestArmyBuilder.h>

#include <iostream>

using namespace std;

static TestArmyBuilder army_builder{};

void
Test::print_ok_message(const string& message)
{
    cout << COLOURS.GREEN << message << COLOURS.NEUTRAL << endl;
}

void
Test::print_error_message(const string& message)
{
    cout << COLOURS.RED << message << COLOURS.NEUTRAL << endl;
}

void
Test::make_tests()
{
    const bool verbose = true;
    const bool silent = false;

    bool test_passed = this->test_function(silent);
    if (not test_passed) {
        this->test_function(verbose);
        Test::print_error_message(this->get_name() + " FAILED");
    } else if (test_passed)
        Test::print_ok_message(this->get_name() + " PASSED");
}

auto
Test::single_test(bool (*test_func)(bool verbose), bool verbose) -> bool
{
    bool passed = test_func(false);
    if (not passed and verbose)
        test_func(true);
    return passed;
}

auto
Test::prepare_basic_game() -> Game
{
    unique_ptr<Player> player1 = make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 = make_unique<Player>("player B", COLOURS.RED);
    army_builder.assign_army_of_size_n(player1.get(), ARMY_SIZE);
    army_builder.assign_army_of_size_n(player2.get(), ARMY_SIZE);
    Game game = Game();
    game.get_data().set_players(move(player1), move(player2));
    return game;
}