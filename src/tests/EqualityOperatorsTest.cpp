#include <Army.h>
#include <BattleAction.h>
#include <BonusAttack.h>
#include <Colours.h>
#include <Directions.h>
#include <EqualityOperatorsTest.h>
#include <GameData.h>
#include <Hit.h>
#include <Movement.h>
#include <OutpostBuilder.h>
#include <PlaceOnBoard.h>
#include <Player.h>
#include <PlayerLogic.h>
#include <PlayerResources.h>
#include <Shoot.h>
#include <Tile.h>

#include <iostream>
#include <memory>

using namespace std;

static unique_ptr<Player> player1 =
    make_unique<Player>("player A", COLOURS.BLUE);
static unique_ptr<Player> player2 =
    make_unique<Player>("player B", COLOURS.RED);
static OutpostBuilder army_builder{};
static string wrongly_unequal = " wrongly deemed unequal";
static string wrongly_equal = " wrongly deemed equal";
static string rightly_equal = " are equal";
static string rightly_unequal = " are unequal";

auto
EqualityOperatorsTest::test_function(bool verbose) -> bool
{
    return single_test(*shooting_eq_op_test, verbose) and
           single_test(*usage_func_eq_op_test, verbose) and
           single_test(*bonus_attack_eq_op_test, verbose) and
           single_test(*tile_eq_op_test, verbose) and
           single_test(*board_eq_op_test, verbose) and
           single_test(*game_data_eq_op_test, verbose);
}

auto
EqualityOperatorsTest::shooting_eq_op_test(bool verbose) -> bool
{
    unique_ptr<BattleAction> Shoot_N_1 = make_unique<Shoot>(1, N);
    unique_ptr<BattleAction> Shoot_N_1_beta = make_unique<Shoot>(1, N);
    unique_ptr<BattleAction> Shoot_N_2 = make_unique<Shoot>(2, N);
    unique_ptr<BattleAction> Shoot_S_1 = make_unique<Shoot>(1, S);

    bool equal_actions = *Shoot_N_1 == *Shoot_N_1_beta;
    bool strength_differs = *Shoot_N_1 != *Shoot_N_2;
    bool direction_differs = *Shoot_N_1 != *Shoot_S_1;

    if (verbose) {
        if (not equal_actions)
            Test::print_error_message("Identical shooting actions " +
                                      wrongly_unequal);
        else
            Test::print_ok_message("Identical shooting actions" +
                                   rightly_equal);
        if (not strength_differs)
            Test::print_error_message(
                "Shooting actions with different strengths" + wrongly_equal);
        else
            Test::print_ok_message("Shooting actions with different strengths" +
                                   rightly_unequal);
        if (not direction_differs)
            Test::print_error_message(
                "Shooting actions with different directions" + wrongly_equal);
        else
            Test::print_ok_message(
                "Shooting actions with different directions" + rightly_equal);
    }
    return equal_actions and strength_differs and direction_differs;
}

auto
EqualityOperatorsTest::usage_func_eq_op_test(bool verbose) -> bool
{
    unique_ptr<UsageFunction> alef = make_unique<PlaceOnBoard>();
    unique_ptr<UsageFunction> alefalef = make_unique<PlaceOnBoard>();
    unique_ptr<UsageFunction> beta = make_unique<Movement>();

    bool equal_funcs = *alef == *alefalef;
    bool funcs_differ = *alef != *beta;
    bool passed = equal_funcs and funcs_differ;
    if (verbose) {
        if (not equal_funcs)
            Test::print_error_message("Identical usage functions" +
                                      wrongly_unequal);
        else
            Test::print_ok_message("Identical usage functions" + rightly_equal);
        if (not funcs_differ)
            Test::print_error_message("Different usage functions" +
                                      wrongly_equal);
        else
            Test::print_ok_message("Different usage functions" +
                                   rightly_unequal);
    }
    return passed;
}

auto
EqualityOperatorsTest::bonus_attack_eq_op_test(bool verbose) -> bool
{
    unique_ptr<PassiveBonus> alef = make_unique<BonusAttack>(N);
    unique_ptr<PassiveBonus> alefalef = make_unique<BonusAttack>(N);
    unique_ptr<PassiveBonus> beta = make_unique<BonusAttack>(S);

    bool equal_bonuses = *alef == *alefalef;
    bool different_bonuses = *alef != *beta;
    if (verbose) {
        if (not equal_bonuses)
            Test::print_error_message("Identical attack bonuses" +
                                      wrongly_unequal);
        else
            Test::print_ok_message("Identical attack bonuses" + rightly_equal);
        if (not different_bonuses)
            Test::print_error_message("Different attack bonuses" +
                                      wrongly_equal);
        else
            Test::print_ok_message("Different attack bonuses" +
                                   rightly_unequal);
    }
    return equal_bonuses and different_bonuses;
}

auto
EqualityOperatorsTest::tile_eq_op_test(bool verbose) -> bool
{
    unique_ptr<set<unique_ptr<BattleAction>>> alef_actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    alef_actions->insert(make_unique<Shoot>(1, N));
    unique_ptr<Tile> alef = make_unique<Tile>(
        false, "shooter", player1.get(), 2, set<int>{ 3 }, move(alef_actions));

    unique_ptr<set<unique_ptr<BattleAction>>> alefalef_actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    alefalef_actions->insert(make_unique<Shoot>(1, N));

    unique_ptr<Tile> alefalef = make_unique<Tile>(false,
                                                  "shooter",
                                                  player1.get(),
                                                  2,
                                                  set<int>{ 3 },
                                                  move(alefalef_actions));

    unique_ptr<Tile> different_owner = make_unique<Tile>(*alef);
    different_owner->set_owner(*player2);

    unique_ptr<set<unique_ptr<BattleAction>>> different_actions_actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    different_actions_actions->insert(make_unique<Shoot>(1, S));
    unique_ptr<Tile> different_actions =
        make_unique<Tile>(false,
                          "shooter",
                          player1.get(),
                          2,
                          set<int>{ 3 },
                          move(different_actions_actions));

    unique_ptr<Tile> different_initiative = make_unique<Tile>(*alef);
    different_initiative->set_initiative(set{ 3, 2 });

    unique_ptr<set<unique_ptr<BattleAction>>> different_hp_actions =
        make_unique<set<unique_ptr<BattleAction>>>();
    different_hp_actions->insert(make_unique<Shoot>(1, N));
    unique_ptr<Tile> different_hp =
        make_unique<Tile>(false,
                          "shooter",
                          player1.get(),
                          22,
                          set<int>{ 3 },
                          move(different_hp_actions));

    unique_ptr<Tile> different_name = make_unique<Tile>(*alef);
    different_name->set_name("Not a shooter");

    unique_ptr<Tile> hq = army_builder.make_headquarter(player1.get());
    unique_ptr<Tile> hq1 = army_builder.make_headquarter(player1.get());
    bool equal_tiles = *alef == *alefalef;
    bool owner_differs = *alef != *different_owner;
    bool actions_differ = *alef != *different_actions;
    bool initiative_differs = *alef != *different_initiative;
    bool hp_differs = *alef != *different_hp;
    bool name_differs = *alef != *different_name;
    bool hqs_equal = *hq == *hq1;
    if (verbose) {
        string equal_error = " wrongly deemed equal";
        if (not equal_tiles)
            Test::print_error_message("Identical tiles wrongly deemed unequal");
        else
            Test::print_ok_message("Identical tiles are equal");
        if (not owner_differs)
            Test::print_error_message("Tiles with different owners" +
                                      equal_error);
        else
            Test::print_ok_message("Tiles with different owners are not equal");
        if (not actions_differ)
            Test::print_error_message("Tiles with different actions" +
                                      equal_error);
        else
            Test::print_ok_message(
                "Tiles with different actions are not equal");
        if (not initiative_differs)
            Test::print_error_message("Tiles with different inititiatives" +
                                      equal_error);
        else
            Test::print_ok_message(
                "Tiles with different initiatives are not equal");
        if (not hp_differs)
            Test::print_error_message("Tiles with different HP" + equal_error);
        else
            Test::print_ok_message("Tiles with different HP are not equal");
        if (not name_differs)
            Test::print_error_message("Tiles with different names" +
                                      equal_error);
        if (not hqs_equal)
            Test::print_error_message("Identical hqs wrongly deemed unequal");
        else
            Test::print_ok_message("Tiles with different names are not equal");
    }
    return equal_tiles and owner_differs and actions_differ and
           initiative_differs and hp_differs and name_differs and hqs_equal;
}

auto
EqualityOperatorsTest::board_eq_op_test(bool verbose) -> bool
{
    Board alef = Board();
    Board alefalef = Board();
    Board small = Board(Board::HAND);
    Board tile_at_some_coords = Board();
    Board tile_at_some_coords_different = Board();
    Board tile_at_other_coords = Board();
    OutpostBuilder army_builder{};
    Coordinates some_coords = Coordinates(2, 0);
    Coordinates other_coords = Coordinates(2, 8);
    unique_ptr<Player> player1;
    player1 = make_unique<Player>("player A", COLOURS.BLUE);
    unique_ptr<Tile> some_tile_one = army_builder.make_commando(player1.get());
    unique_ptr<Tile> some_tile_two = make_unique<Tile>(*some_tile_one);
    unique_ptr<Tile> other_tile = army_builder.make_runner(player1.get());
    tile_at_some_coords.set_tile(some_coords, move(some_tile_one));
    tile_at_some_coords_different.set_tile(some_coords, move(other_tile));
    tile_at_other_coords.set_tile(other_coords, move(some_tile_two));
    bool equal_boards = alef == alefalef;
    bool different_size = alef != small;
    bool different_placement = tile_at_some_coords != tile_at_other_coords;
    bool different_tiles = tile_at_some_coords != tile_at_some_coords_different;

    if (verbose) {
        string equal_error = " wrongly deemed equal";
        if (not equal_boards)
            Test::print_error_message(
                "Identical boards wrongly deemed unequal");
        else
            Test::print_ok_message("Identical boards are equal");
        if (not different_size)
            Test::print_error_message("Boards with different sizes" +
                                      equal_error);
        else
            Test::print_ok_message("Boards with different sizes are not equal");
        if (not different_placement)
            Test::print_error_message("Boards with different tile placement" +
                                      equal_error);
        else
            Test::print_ok_message(
                "Boards with different tile placement are not equal");
        if (not different_tiles)
            Test::print_error_message(
                "Boards with different tiles at same place" + equal_error);
        else
            Test::print_ok_message(
                "Boards with different tiles at same place are not equal");
    }

    return equal_boards and different_size and different_placement and
           different_tiles;
}

auto
EqualityOperatorsTest::game_data_eq_op_test(bool verbose) -> bool
{
    GameData alef = GameData();
    GameData alefalef = GameData(alef);
    GameData beta = GameData();

    beta.next_turn();

    bool equal_game_datas = alef == alefalef;
    bool different_game_datas = alef != beta;
    if (verbose) {
        string equal_error = " wrongly deemed equal";
        if (not equal_game_datas)
            Test::print_error_message(
                "Identical game_datas wrongly deemed unequal");
        else
            Test::print_ok_message("Identical game_datas are equal");
        if (not different_game_datas)
            Test::print_error_message("Different game_datas" + equal_error);
        else
            Test::print_ok_message("Different game_datas are not equal");
    }

    return equal_game_datas and different_game_datas;
}
