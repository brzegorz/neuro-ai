#include <Army.h>
#include <Battle.h>
#include <BattleAction.h>
#include <Board.h>
#include <Colours.h>
#include <Gui.h>
#include <MolochBuilder.h>
#include <OutpostBuilder.h>
#include <PassiveBonusTest.h>
#include <Player.h>
#include <PlayerLogic.h>
#include <PlayerResources.h>
#include <Shoot.h>
#include <Test.h>
#include <TextGui.h>
#include <Tile.h>

#include <memory>

using namespace std;

static OutpostBuilder outpost_builder{};
static MolochBuilder moloch_builder{};
static unique_ptr<Gui> gui = make_unique<TextGui>();
static Coordinates board_center{ 2, 4 };
static vector<Coordinates> inner_ring{ { 2, 2 }, { 3, 3 }, { 3, 5 },
                                       { 2, 6 }, { 1, 5 }, { 1, 3 } };
static vector<Coordinates> outer_ring{ { 0, 2 }, { 0, 4 }, { 0, 6 }, { 1, 1 },
                                       { 1, 7 }, { 2, 0 }, { 2, 8 }, { 3, 1 },
                                       { 3, 7 }, { 4, 2 }, { 4, 4 }, { 4, 6 } };

auto
PassiveBonusTest::test_function(bool verbose) -> bool
{
    return single_test(*bonus_gets_applied, verbose) and
           single_test(*bonus_gets_applied, verbose) and
           single_test(*bonus_gets_removed, verbose) and
           single_test(*bonus_gets_moved, verbose) and
           single_test(*bonus_gets_rotated, verbose) and
           single_test(*tiles_get_attacked, verbose) and
           single_test(*malus_initiative_zero, verbose) and
           single_test(*set_bonus_next_to_scoper, verbose) and
           single_test(*net_unit, verbose) and
           single_test(*net_the_net, verbose) and
           single_test(*net_mutual, verbose) and
           single_test(*net_scoper_in_last_turn, verbose) and
           single_test(*recon_center_defaults_to_false, verbose) and
           single_test(*recon_center_gets_applied, verbose) and
           single_test(*recon_center_gets_removed, verbose);
}

auto
PassiveBonusTest::bonus_gets_applied(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Board board = Board();
    board.set_tile(board_center,
                   outpost_builder.make_headquarter(player1.get()));
    set<int> correct_initiative{ 3, 2 };
    for (auto const& coords : inner_ring) {
        board.set_tile(coords, outpost_builder.make_commando(player1.get()));
        set<int> commando_initiative =
            board.get_tile_ptr(coords)->get_initiative();
        if (verbose) {
            cout << "Bonus applied when tile is placed after the bonus" << endl;
            gui->print_board(board);
        }
        if (commando_initiative != correct_initiative)
            return false;
    }
    return true;
}

auto
PassiveBonusTest::bonus_gets_applied2(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Board board = Board();

    for (auto const& coords : inner_ring) {
        board.set_tile(coords, outpost_builder.make_commando(player1.get()));
    }
    board.set_tile(board_center,
                   outpost_builder.make_headquarter(player1.get()));
    set<int> correct_initiative{ 3, 2 };
    for (auto const& coords : inner_ring) {
        set<int> commando_initiative =
            board.get_tile_ptr(coords)->get_initiative();
        if (verbose) {
            cout << "Bonus applied when bonus is placed after the tile" << endl;
            gui->print_board(board);
        }
        if (commando_initiative != correct_initiative)
            return false;
    }
    return true;
}

auto
PassiveBonusTest::bonus_gets_removed(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Board board = Board();
    board.set_tile(board_center,
                   outpost_builder.make_headquarter(player1.get()));
    for (auto const& coords : inner_ring) {
        board.set_tile(coords, outpost_builder.make_commando(player1.get()));
    }
    board.kill_tile(board_center);
    set<int> correct_initiative{ 3 };
    for (auto const& coords : inner_ring) {
        set<int> commando_initiative =
            board.get_tile_ptr(coords)->get_initiative();
        if (commando_initiative != correct_initiative)
            return false;
    }
    if (verbose) {
        cout << "Board after removing Outpost HQ from the center" << endl;
        gui->print_board(board);
    }
    return true;
}

auto
PassiveBonusTest::bonus_gets_moved(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Board board = Board();
    Coordinates x_improved_coords{ 1, 3 };
    Coordinates y_improved_coords{ 4, 4 };
    set<int> x_commando_initiative;
    set<int> y_commando_initiative;
    board.set_tile(board_center,
                   outpost_builder.make_headquarter(player1.get()));
    board.set_tile(x_improved_coords,
                   outpost_builder.make_commando(player1.get()));
    board.set_tile(y_improved_coords,
                   outpost_builder.make_commando(player1.get()));
    set<int> base_initiative{ 3 };
    set<int> improved_initiative{ 3, 2 };
    bool passed = true;
    if (verbose) {
        cout << "Before HQ movement" << endl;
        gui->print_board(board);
    }

    x_commando_initiative =
        board.get_tile_ptr(x_improved_coords)->get_initiative();
    y_commando_initiative =
        board.get_tile_ptr(y_improved_coords)->get_initiative();
    if (x_commando_initiative != improved_initiative or
        y_commando_initiative != base_initiative)
        passed = false;
    unique_ptr<Tile> HQ = board.pick_up_tile(board_center);
    board.set_tile(Coordinates(3, 3), move(HQ));
    if (verbose) {
        cout << "After HQ movement" << endl;
        gui->print_board(board);
    }

    x_commando_initiative =
        board.get_tile_ptr(x_improved_coords)->get_initiative();
    y_commando_initiative =
        board.get_tile_ptr(y_improved_coords)->get_initiative();
    if (x_commando_initiative != base_initiative or
        y_commando_initiative != improved_initiative)
        passed = false;
    return passed;
}

auto
PassiveBonusTest::bonus_gets_rotated(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Board board = Board();
    Coordinates x_improved_coords{ 1, 3 };
    Coordinates y_improved_coords{ 4, 4 };
    set<int> x_hybrid_initiative;
    set<int> y_hybrid_initiative;
    board.set_tile(board_center,
                   moloch_builder.make_mother_module(player1.get()));
    board.rotate_tile(board_center, NW);
    board.set_tile(x_improved_coords,
                   moloch_builder.make_hybrid(player1.get()));
    board.set_tile(y_improved_coords,
                   moloch_builder.make_hybrid(player1.get()));
    set<int> base_initiative{ 3 };
    set<int> improved_initiative{ 3, 2 };
    bool passed = true;
    if (verbose) {
        cout << "Before mother module movement" << endl;
        gui->print_board(board);
    }

    x_hybrid_initiative =
        board.get_tile_ptr(x_improved_coords)->get_initiative();
    y_hybrid_initiative =
        board.get_tile_ptr(y_improved_coords)->get_initiative();
    if (x_hybrid_initiative != improved_initiative or
        y_hybrid_initiative != base_initiative)
        passed = false;
    unique_ptr<Tile> HQ = board.pick_up_tile(board_center);
    board.set_tile(Coordinates(3, 3), move(HQ));
    board.rotate_tile(Coordinates(3, 3), S);
    if (verbose) {
        cout << "After mother module movement" << endl;
        gui->print_board(board);
    }

    x_hybrid_initiative =
        board.get_tile_ptr(x_improved_coords)->get_initiative();
    y_hybrid_initiative =
        board.get_tile_ptr(y_improved_coords)->get_initiative();
    if (x_hybrid_initiative != base_initiative or
        y_hybrid_initiative != improved_initiative)
        passed = false;
    return passed;
}

auto
PassiveBonusTest::tiles_get_attacked(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    Board* board = game_data.get_board();
    board->set_tile(Coordinates(2, 6),
                    outpost_builder.make_headquarter(player1.get()));
    board->set_tile(Coordinates(3, 7),
                    outpost_builder.make_commando(player1.get()));
    board->rotate_tile(Coordinates(3, 7), NW);
    board->set_tile(Coordinates(2, 8),
                    moloch_builder.make_armored_hunter(player2.get()));
    board->set_tile(Coordinates(0, 4),
                    moloch_builder.make_headquarter(player2.get()));
    if (verbose)
        gui->print_board(*board);
    game.loop_for_single_battle();
    if (verbose)
        gui->print_board(*board);
    return board->get_tile_ptr(Coordinates(0, 4))->get_hp() == 18;
}

auto
PassiveBonusTest::malus_initiative_zero(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Board board = Board();
    board.set_tile(Coordinates(2, 6),
                   moloch_builder.make_headquarter(player2.get()));
    board.set_tile(Coordinates(3, 7),
                   outpost_builder.make_saboteur(player1.get()));
    if (verbose)
        gui->print_board(board);
    int headquarter_initaitive =
        *board.get_tile_ptr(Coordinates(2, 6))->get_initiative().begin();
    return headquarter_initaitive == 0;
}

auto
PassiveBonusTest::set_bonus_next_to_scoper(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Board board = Board();
    board.set_tile(Coordinates(4, 4),
                   outpost_builder.make_scoper(player1.get()));
    board.set_tile(Coordinates(3, 1),
                   moloch_builder.make_juggernaut(player2.get()));
    board.set_tile(Coordinates(3, 3),
                   moloch_builder.make_mother_module(player2.get()));
    int juggernaut_attack_count1 =
        board.get_tile_ptr(Coordinates(3, 1))->get_initiative().size();
    if (verbose) {
        cout << "Mother module shouldn't apply it's bonus" << endl;
        gui->print_board(board);
    }
    board.pick_up_tile(Coordinates(3, 3));
    int juggernaut_attack_count2 =
        board.get_tile_ptr(Coordinates(3, 1))->get_initiative().size();
    if (verbose) {
        cout << "Mother module shouldn't apply it's bonus" << endl;
        gui->print_board(board);
    }
    return juggernaut_attack_count1 == juggernaut_attack_count2 and
           juggernaut_attack_count1 == 1;
}

auto
PassiveBonusTest::net_unit(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    Board* board = game_data.get_board();
    unique_ptr<Tile> shooter1 = outpost_builder.make_commando(player1.get());
    unique_ptr<Tile> shooter2 = moloch_builder.make_guard(player2.get());
    unique_ptr<Tile> net = moloch_builder.make_net_fighter(player2.get());
    Coordinates coords_winner = Coordinates(2, 8);
    Coordinates coords_loser = Coordinates(2, 0);
    Coordinates coords_net = Coordinates(2, 2);
    board->set_tile(coords_loser, move(shooter1));
    board->set_tile(coords_winner, move(shooter2));
    board->set_tile(coords_net, move(net));
    board->rotate_tile(coords_loser, S);
    if (verbose)
        gui->print_board(*board);
    game.loop_for_single_battle();
    if (verbose)
        gui->print_board(*board);
    // Check if tests passed
    Tile* destroyed = board->get_tile_ptr(coords_loser);
    Tile* winner = board->get_tile_ptr(coords_winner);
    if (destroyed or !winner)
        return false;
    return true;
}

auto
PassiveBonusTest::net_the_net(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    Board* board = game_data.get_board();
    unique_ptr<Tile> shooter1 = outpost_builder.make_commando(player1.get());
    unique_ptr<Tile> shooter2 = moloch_builder.make_guard(player2.get());
    unique_ptr<Tile> net = moloch_builder.make_net_fighter(player2.get());
    unique_ptr<Tile> outpost_net =
        moloch_builder.make_net_fighter(player1.get());
    Coordinates coords_winner = Coordinates(2, 0);
    Coordinates coords_loser = Coordinates(2, 8);
    Coordinates coords_net = Coordinates(2, 2);
    Coordinates coords_net_outpost = Coordinates(2, 4);
    board->set_tile(coords_winner, move(shooter1));
    board->set_tile(coords_loser, move(shooter2));
    board->set_tile(coords_net, move(net));
    board->set_tile(coords_net_outpost, move(outpost_net));
    board->rotate_tile(coords_winner, S);
    if (verbose)
        gui->print_board(*board);
    game.loop_for_single_battle();
    if (verbose)
        gui->print_board(*board);
    // Check if tests passed
    Tile* moloch_tile = board->get_tile_ptr(coords_loser);
    Tile* outpost_tile = board->get_tile_ptr(coords_winner);
    return outpost_tile and moloch_tile;
}

auto
PassiveBonusTest::net_mutual(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Board board = Board();
    unique_ptr<Tile> shooter_outpost =
        outpost_builder.make_commando(player1.get());
    unique_ptr<Tile> shooter_moloch = moloch_builder.make_hybrid(player2.get());
    unique_ptr<Tile> net = moloch_builder.make_net_fighter(player2.get());
    unique_ptr<Tile> net_outpost =
        moloch_builder.make_net_fighter(player1.get());
    net_outpost->set_name("outpost_net_fighter");
    Coordinates coords_outpost = Coordinates(1, 3);
    Coordinates coords_moloch = Coordinates(3, 3);
    Coordinates coords_net_moloch = Coordinates(2, 4);
    Coordinates coords_net_outpost = Coordinates(2, 2);
    board.set_tile(coords_outpost, move(shooter_outpost));
    board.set_tile(coords_moloch, move(shooter_moloch));
    board.set_tile(coords_net_outpost, move(net_outpost));
    board.rotate_tile(coords_net_outpost, S);
    board.set_tile(coords_net_moloch, move(net));
    if (verbose)
        gui->print_board(board);
    return not board.get_tile_ptr(coords_net_moloch)->is_in_net() and
           not board.get_tile_ptr(coords_net_outpost)->is_in_net() and
           board.get_tile_ptr(coords_outpost)->is_in_net() and
           board.get_tile_ptr(coords_moloch)->is_in_net();
}

auto
PassiveBonusTest::net_scoper_in_last_turn(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    Board* board = game_data.get_board();
    if (verbose) {
        cout << "Test of killing a scoper" << endl;
    }
    if (verbose) {
        cout << "Placing HQ with shooting bonus" << endl;
    }
    board->set_tile(Coordinates(2, 8),
                    moloch_builder.make_headquarter(player1.get()));
    if (verbose) {
        cout << "Placing the scoper" << endl;
    }
    board->set_tile(Coordinates(2, 6),
                    outpost_builder.make_scoper(player2.get()));
    if (verbose) {
        cout << "Placing HQ" << endl;
    }
    board->set_tile(Coordinates(1, 5),
                    outpost_builder.make_headquarter(player1.get()));
    if (verbose) {
        cout << "Placing the net fighter" << endl;
    }
    board->set_tile(Coordinates(1, 7),
                    moloch_builder.make_net_fighter(player2.get()));
    if (verbose) {
        cout << "Rotating the net fighter" << endl;
    }
    board->rotate_tile(Coordinates(1, 7), SE);
    if (verbose) {
        gui->print_board(*board);
    }
    game.loop_for_single_battle();
    if (verbose) {
        gui->print_board(*board);
    }
    // didn't crash
    return true;
}

auto
PassiveBonusTest::recon_center_defaults_to_false(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Board board = Board();
    unique_ptr<Player> test_player = make_unique<Player>("player A", COLOURS.GREEN);
    board.set_tile(board_center,
                   outpost_builder.make_headquarter(test_player.get()));
    if (verbose) {
        cout << "Player has double movement by default" << endl;
    }
    return test_player->has_double_movement() == false;
}

auto
PassiveBonusTest::recon_center_gets_applied(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Board board = Board();
    unique_ptr<Player> test_player = make_unique<Player>("player A", COLOURS.GREEN);
    board.set_tile(board_center,
                   outpost_builder.make_headquarter(player1.get()));
    board.set_tile(outer_ring[0], outpost_builder.make_recon_center(player1.get()));
    if (verbose) {
        cout << "Player has not acquired double movement from the recon center" << endl;
    }
    return player1->has_double_movement() == true;
}

auto
PassiveBonusTest::recon_center_gets_removed(bool verbose) -> bool
{
    unique_ptr<Player> player1 =
        make_unique<Player>("player A", COLOURS.GREEN);
    unique_ptr<Player> player2 =
        make_unique<Player>("player B", COLOURS.RED);
    Board board = Board();
    board.set_tile(board_center,
                   outpost_builder.make_headquarter(player1.get()));
    board.set_tile(inner_ring[0], outpost_builder.make_recon_center(player1.get()));
    board.kill_tile(inner_ring[0]);
    if (verbose) {
        cout << "Player has double movement after removal of the recon center" << endl;
    }
    return player1->has_double_movement() == false;
}