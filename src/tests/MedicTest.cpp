#include <Battle.h>
#include <Board.h>
#include <Game.h>
#include <MedicTest.h>
#include <MolochBuilder.h>
#include <OutpostBuilder.h>
#include <TextGui.h>
#include <HumanPlayerLogic.h>

#include <memory>

using namespace std;

static MolochBuilder moloch_builder{};
static OutpostBuilder outpost_builder{};
static unique_ptr<Gui> gui = make_unique<TextGui>();

auto
MedicTest::test_function(bool verbose) -> bool
{
    return single_test(*saves_one_unit, verbose) and
           single_test(*player_can_choose_an_attack_to_heal, verbose) and
           single_test(*medic_released_from_web, verbose) and
           single_test(*one_medic_many_units, verbose) and
           single_test(*many_medics_one_unit, verbose) and
           single_test(*mutualy_healing_medics, verbose) and
           single_test(*does_not_save_unit_when_scoper_used, verbose) and
           single_test(*saves_enemy_unit_when_scoper_used, verbose);
}

auto
MedicTest::saves_one_unit(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    Board* board = game.get_data().get_board();
    const Coordinates attacker_coords{ 2, 8 };
    const Coordinates attacked_coords{ 2, 6 };
    const Coordinates medic_coords{ 1, 7 };
    Player* attacking_player = game.get_data().get_active_player();
    Player* medic_player = game.get_data().get_previous_player();
    attacking_player->set_logic(nullptr);
    medic_player->set_logic(nullptr);
    board->set_tile(attacker_coords,
                    outpost_builder.make_brawler(attacking_player));
    board->set_tile(attacked_coords,
                    moloch_builder.make_juggernaut(medic_player));
    board->rotate_tile(attacked_coords, S);
    board->set_tile(medic_coords, moloch_builder.make_medic(medic_player));
    board->rotate_tile(medic_coords, NE);
    PlayerDecision healing_decision{};
    healing_decision.set_tile_target_location(
        attacked_coords.x, attacked_coords.y, N);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(healing_decision));
    PlayerDecision attack_to_heal_decision{};
    attack_to_heal_decision.set_attack_to_heal_index(0);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(attack_to_heal_decision));
    if (verbose) {
        cout << "Juggernaut should survive and kill the brawler" << endl;
        gui->print_board(*board);
    }
    game.loop_for_single_battle();
    if (verbose) {
        gui->print_board(*board);
    }
    return board->get_tile_ptr(attacked_coords) and
           not board->get_tile_ptr(attacker_coords) and
           not board->get_tile_ptr(medic_coords);
}

auto
MedicTest::one_medic_many_units(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    Board* board = game.get_data().get_board();
    const Coordinates attacker_coords{ 2, 4 };
    const Coordinates survivor_coords{ 2, 6 };
    const Coordinates destroyed_coords{ 1, 3 };
    const Coordinates medic_coords{ 1, 5 };
    Player* attacking_player = game.get_data().get_active_player();
    Player* medic_player = game.get_data().get_previous_player();
    attacking_player->set_logic(nullptr);
    medic_player->set_logic(nullptr);
    board->set_tile(attacker_coords,
                    moloch_builder.make_hunter_killer(attacking_player));
    board->set_tile(survivor_coords, moloch_builder.make_hybrid(medic_player));
    board->set_tile(destroyed_coords,
                    moloch_builder.make_officer(medic_player));
    board->set_tile(medic_coords, moloch_builder.make_medic(medic_player));
    PlayerDecision healing_decision{};
    healing_decision.set_tile_target_location(
        survivor_coords.x, survivor_coords.y, N);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(healing_decision));
    PlayerDecision attack_to_heal_decision{};
    attack_to_heal_decision.set_attack_to_heal_index(0);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(attack_to_heal_decision));
    if (verbose) {
        cout << "Only hybrid should survive" << endl;
        gui->print_board(*board);
    }
    game.loop_for_single_battle();
    if (verbose) {
        gui->print_board(*board);
    }
    return board->get_tile_ptr(survivor_coords) and
           not board->get_tile_ptr(destroyed_coords) and
           not board->get_tile_ptr(medic_coords) and
           not board->get_tile_ptr(attacker_coords);
}

auto
MedicTest::many_medics_one_unit(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    Board* board = game.get_data().get_board();
    const Coordinates attacker_coords{ 2, 4 };
    const Coordinates medic1_coords{ 2, 6 };
    const Coordinates medic2_coords{ 1, 7 };
    const Coordinates defender_coords{ 1, 5 };
    Player* attacking_player = game.get_data().get_active_player();
    Player* medic_player = game.get_data().get_previous_player();
    attacking_player->set_logic(nullptr);
    medic_player->set_logic(nullptr);
    board->set_tile(attacker_coords,
                    moloch_builder.make_hybrid(attacking_player));
    board->set_tile(medic1_coords, outpost_builder.make_medic(medic_player));
    board->set_tile(medic2_coords, outpost_builder.make_medic(medic_player));
    board->set_tile(defender_coords, moloch_builder.make_hybrid(medic_player));
    board->rotate_tile(medic1_coords, NW);
    board->rotate_tile(medic2_coords, NE);
    board->rotate_tile(attacker_coords, SW);
    PlayerDecision medic_to_choose_decision{};
    medic_to_choose_decision.set_tile_target_location(
        medic2_coords.x, medic2_coords.y, N);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(medic_to_choose_decision));
    PlayerDecision healing_decision{};
    healing_decision.set_tile_target_location(
        defender_coords.x, defender_coords.y, N);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(healing_decision));
    PlayerDecision attack_to_heal_decision{};
    attack_to_heal_decision.set_attack_to_heal_index(0);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(attack_to_heal_decision));
    PlayerDecision medic_to_lose_decision{};
    medic_to_lose_decision.set_tile_target_location(
        medic2_coords.x, medic2_coords.y, N);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(medic_to_lose_decision));
    if (verbose) {
        cout << "Only upper medic should disappear" << endl;
        gui->print_board(*board);
    }
    game.loop_for_single_battle();
    if (verbose) {
        gui->print_board(*board);
    }
    return board->get_tile_ptr(attacker_coords) and
           board->get_tile_ptr(defender_coords) and
           board->get_tile_ptr(medic1_coords) and
           not board->get_tile_ptr(medic2_coords);
}

auto
MedicTest::medic_released_from_web(bool verbose) -> bool
{
    return true;
}

auto
MedicTest::player_can_choose_an_attack_to_heal(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    Board* board = game.get_data().get_board();
    const Coordinates strong_attacker_coords{ 2, 8 };
    const Coordinates weak_attacker_coords{ 3, 7 };
    const Coordinates attacked_coords{ 2, 6 };
    const Coordinates medic_coords{ 1, 7 };
    Player* attacking_player = game.get_data().get_active_player();
    Player* medic_player = game.get_data().get_previous_player();
    attacking_player->set_logic(nullptr);
    medic_player->set_logic(nullptr);
    board->set_tile(strong_attacker_coords,
                    outpost_builder.make_brawler(attacking_player));
    board->set_tile(weak_attacker_coords,
                    outpost_builder.make_commando(attacking_player));
    board->set_tile(attacked_coords,
                    moloch_builder.make_headquarter(medic_player));
    board->rotate_tile(attacked_coords, S);
    board->rotate_tile(weak_attacker_coords, NW);
    board->set_tile(medic_coords, moloch_builder.make_medic(medic_player));
    board->rotate_tile(medic_coords, NE);
    PlayerDecision tile_to_heal_decision{};
    tile_to_heal_decision.set_tile_target_location(
        attacked_coords.x, attacked_coords.y, N);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(tile_to_heal_decision));
    PlayerDecision attack_to_heal_decision{};
    attack_to_heal_decision.set_attack_to_heal_index(1);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(attack_to_heal_decision));
    if (verbose) {
        cout << "Headquarter should have 19 hit points" << endl;
        gui->print_board(*board);
    }
    game.loop_for_single_battle();
    if (verbose) {
        gui->print_board(*board);
    }
    return board->get_tile_ptr(attacked_coords)->get_hp() == 19;
}

auto
MedicTest::mutualy_healing_medics(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    Board* board = game.get_data().get_board();
    const Coordinates attacker_coords{ 2, 4 };
    const Coordinates medic1_coords{ 2, 6 };
    const Coordinates medic2_coords{ 1, 5 };
    Player* attacking_player = game.get_data().get_active_player();
    Player* medic_player = game.get_data().get_previous_player();
    attacking_player->set_logic(nullptr);
    medic_player->set_logic(nullptr);
    board->set_tile(attacker_coords,
                    moloch_builder.make_hybrid(attacking_player));
    board->set_tile(medic1_coords, moloch_builder.make_medic(medic_player));
    board->set_tile(medic2_coords, moloch_builder.make_medic(medic_player));
    board->rotate_tile(medic1_coords, NW);
    board->rotate_tile(attacker_coords, SW);
    PlayerDecision healing_decision{};
    healing_decision.set_tile_target_location(
        medic2_coords.x, medic2_coords.y, N);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(healing_decision));
    PlayerDecision attack_to_heal_decision{};
    attack_to_heal_decision.set_attack_to_heal_index(0);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(attack_to_heal_decision));
    if (verbose) {
        cout << "Only upper medic should disappear" << endl;
        gui->print_board(*board);
    }
    game.loop_for_single_battle();
    if (verbose) {
        gui->print_board(*board);
    }
    return board->get_tile_ptr(attacker_coords) and
           board->get_tile_ptr(medic1_coords) and
           not board->get_tile_ptr(medic2_coords);
}

auto
MedicTest::does_not_save_unit_when_scoper_used(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    Board* board = game.get_data().get_board();
    const Coordinates attacker_coords{ 2, 8 };
    const Coordinates attacked_coords{ 2, 6 };
    const Coordinates medic_coords{ 1, 7 };
    const Coordinates scoper_coords{ 1, 5 };
    Player* attacking_player = game.get_data().get_active_player();
    Player* medic_player = game.get_data().get_previous_player();
    attacking_player->set_logic(nullptr);
    medic_player->set_logic(nullptr);
    board->set_tile(attacker_coords,
                    outpost_builder.make_brawler(attacking_player));
    board->set_tile(attacked_coords,
                    moloch_builder.make_juggernaut(medic_player));
    board->rotate_tile(attacked_coords, S);
    board->set_tile(medic_coords, moloch_builder.make_medic(medic_player));
    board->rotate_tile(medic_coords, NE);
    board->set_tile(scoper_coords, outpost_builder.make_scoper(attacking_player));
    if (verbose) {
        cout << "Juggernaut should die" << endl;
        gui->print_board(*board);
    }
    game.loop_for_single_battle();
    if (verbose) {
        gui->print_board(*board);
    }
    return not board->get_tile_ptr(attacked_coords) and
           board->get_tile_ptr(attacker_coords) and
           board->get_tile_ptr(medic_coords) and
           board->get_tile_ptr(scoper_coords);
}

auto 
MedicTest::saves_enemy_unit_when_scoper_used(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    Board* board = game.get_data().get_board();
    game.set_gui(make_unique<TextGui>());
    const Coordinates attacked_coords{ 2, 8 };
    const Coordinates attacker_coords{ 2, 6 };
    const Coordinates medic_coords{ 1, 7 };
    const Coordinates scoper_coords{ 1, 5 };
    Player* attacked_player = game.get_data().get_active_player();
    Player* medic_player = game.get_data().get_previous_player();
    attacked_player->set_logic(nullptr);
    medic_player->set_logic(nullptr);
    board->set_tile(attacked_coords,
                    outpost_builder.make_brawler(attacked_player));
    board->set_tile(attacker_coords,
                    moloch_builder.make_juggernaut(medic_player));
    board->rotate_tile(attacked_coords, S);
    board->rotate_tile(attacker_coords, S);
    board->set_tile(medic_coords, moloch_builder.make_medic(medic_player));
    board->rotate_tile(medic_coords, SE);
    board->set_tile(scoper_coords, outpost_builder.make_scoper(attacked_player));

    PlayerDecision healing_decision{};
    healing_decision.set_tile_target_location(
        attacked_coords.x, attacked_coords.y, N);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(healing_decision));

    PlayerDecision healing_decision2{};
    healing_decision2.set_attack_to_heal_index(0);
    game.get_data().get_decision_manager()->add_test_decision(
        make_unique<PlayerDecision>(healing_decision2));

    if (verbose) {
        cout << "Nobody except medic should die" << endl;
        gui->print_board(*board);
    }
    game.loop_for_single_battle();
    if (verbose) {
        gui->print_board(*board);
    }
    return board->get_tile_ptr(attacker_coords) and
           board->get_tile_ptr(attacked_coords) and
           not board->get_tile_ptr(medic_coords) and
           board->get_tile_ptr(scoper_coords);
}