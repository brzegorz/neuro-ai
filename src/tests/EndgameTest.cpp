#include <Army.h>
#include <BattleAction.h>
#include <Board.h>
#include <Colours.h>
#include <EndgameTest.h>
#include <Game.h>
#include <Gui.h>
#include <MolochBuilder.h>
#include <Player.h>
#include <RandomPlayerLogic.h>
#include <Test.h>
#include <TestArmyBuilder.h>
#include <TextGui.h>
#include <Tile.h>

#include <memory>

using namespace std;

static const Coordinates winner_hq_coords{ 2, 2 };
static const Coordinates loser_hq_coords{ 2, 6 };

auto
EndgameTest::test_function(bool verbose) -> bool
{
    return single_test(*out_of_tiles_with_winner, verbose) and
           single_test(*out_of_tiles_tie, verbose) and
           single_test(*HQ_destroyed_with_winner, verbose) and
           single_test(*HQ_destroyed_both, verbose) and
           single_test(*out_of_tiles_board_full, verbose);
}

auto
EndgameTest::out_of_tiles_with_winner(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    out_of_tiles_with_winner_prepare_moves(&game);
    if (verbose) {
        cout << "One player should win after somebody runs out of tiles"
             << endl;
        game.set_gui(make_unique<TextGui>());
    }
    game.execute_whole_game();
    Player* alleged_winner =
        game_data.get_board()->get_tile_ptr(winner_hq_coords)->get_owner();
    Player* defacto_winner = game.get_data().get_winner();
    return defacto_winner == alleged_winner and game_data.is_game_finished and
           game_data.is_one_player_out_of_tiles and
           game_data.is_final_battle_over;
}

void
EndgameTest::out_of_tiles_with_winner_prepare_moves(Game* game)
{
    PlayerDecisionManager* manager = game->get_data().get_decision_manager();
    unique_ptr<PlayerDecision> set_headquarter1 = make_unique<PlayerDecision>();
    set_headquarter1->set_tile_target_location(
        winner_hq_coords.x, winner_hq_coords.y, 0);
    manager->add_test_decision(move(set_headquarter1));
    unique_ptr<PlayerDecision> set_headquarter2 = make_unique<PlayerDecision>();
    set_headquarter2->set_tile_target_location(
        loser_hq_coords.x, loser_hq_coords.y, 0);
    manager->add_test_decision(move(set_headquarter2));

    array<PlayerDecision, 3> loser_move{};
    array<PlayerDecision, 4> winner_move{};
    loser_move.at(0).set_whether_to_use_innates(false);
    loser_move.at(1).set_tile_to_use_index(0);
    loser_move.at(2).set_chosen_way_of_tile_usage(DISCARD);
    winner_move.at(0).set_whether_to_use_innates(false);
    winner_move.at(1).set_tile_to_use_index(0);
    winner_move.at(2).set_chosen_way_of_tile_usage(USE);
    winner_move.at(3).set_tile_target_location(2, 8, 0);
    for (int i = 0; i < 20; i++) {
        for (int j = 0; j < 3; j++) {
            manager->add_test_decision(
                make_unique<PlayerDecision>(loser_move.at(j)));
        }
        for (int j = 0; j < 4; j++) {
            manager->add_test_decision(
                make_unique<PlayerDecision>(winner_move.at(j)));
        }
    }
}

auto
EndgameTest::out_of_tiles_tie(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    out_of_tiles_tie_prepare_moves(&game);
    if (verbose) {
        cout << "Game should end with a tie after somebody runs out of tiles"
             << endl;
        game.set_gui(make_unique<TextGui>());
    }
    game.execute_whole_game();
    Player* winner = game.get_data().get_winner();
    return not winner and game_data.is_game_finished and
           game_data.is_one_player_out_of_tiles and
           game_data.is_final_battle_over;
}

void
EndgameTest::out_of_tiles_tie_prepare_moves(Game* game)
{
    PlayerDecisionManager* manager = game->get_data().get_decision_manager();
    unique_ptr<PlayerDecision> set_headquarter1 = make_unique<PlayerDecision>();
    set_headquarter1->set_tile_target_location(2, 8, 0);
    manager->add_test_decision(move(set_headquarter1));
    unique_ptr<PlayerDecision> set_headquarter2 = make_unique<PlayerDecision>();
    set_headquarter2->set_tile_target_location(2, 0, 0);
    manager->add_test_decision(move(set_headquarter2));

    array<PlayerDecision, 3> universal_tie_move{};
    universal_tie_move.at(0).set_whether_to_use_innates(false);
    universal_tie_move.at(1).set_tile_to_use_index(0);
    universal_tie_move.at(2).set_chosen_way_of_tile_usage(DISCARD);
    for (int i = 0; i < 40; i++) {
        for (int j = 0; j < 3; j++) {
            manager->add_test_decision(
                make_unique<PlayerDecision>(universal_tie_move.at(j)));
        }
    }
}

auto
EndgameTest::HQ_destroyed_both(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    if (verbose) {
        cout << "Both HQs should be destroyed without a winner" << endl;
        game.set_gui(make_unique<TextGui>());
    }
    HQ_destroyed_both_prepare_moves(&game);
    game_data.get_active_player()->get_resources()->get_army()->set_headquarter(
        make_a_weak_HQ(game_data.get_active_player()));
    game_data.get_previous_player()
        ->get_resources()
        ->get_army()
        ->set_headquarter(make_a_weak_HQ(game_data.get_previous_player()));
    game.execute_whole_game();
    Player* winner = game.get_data().get_winner();
    return not winner and game_data.is_game_finished and
           not game_data.get_board()->get_tile_ptr(loser_hq_coords) and
           not game_data.get_board()->get_tile_ptr(winner_hq_coords);
}

void
EndgameTest::HQ_destroyed_both_prepare_moves(Game* game)
{
    PlayerDecisionManager* manager = game->get_data().get_decision_manager();
    unique_ptr<PlayerDecision> set_headquarter1 = make_unique<PlayerDecision>();
    set_headquarter1->set_tile_target_location(
        winner_hq_coords.x, winner_hq_coords.y, 0);
    manager->add_test_decision(move(set_headquarter1));
    unique_ptr<PlayerDecision> set_headquarter2 = make_unique<PlayerDecision>();
    set_headquarter2->set_tile_target_location(
        loser_hq_coords.x, loser_hq_coords.y, 0);
    manager->add_test_decision(move(set_headquarter2));

    array<PlayerDecision, 4> loser_move{};
    array<PlayerDecision, 4> winner_move{};
    loser_move.at(0).set_whether_to_use_innates(false);
    loser_move.at(1).set_tile_to_use_index(0);
    loser_move.at(2).set_chosen_way_of_tile_usage(USE);
    loser_move.at(3).set_tile_target_location(2, 0, 3);
    winner_move.at(0).set_whether_to_use_innates(false);
    winner_move.at(1).set_tile_to_use_index(0);
    winner_move.at(2).set_chosen_way_of_tile_usage(USE);
    winner_move.at(3).set_tile_target_location(2, 8, 0);
    for (int i = 0; i < 20; i++) {
        for (int j = 0; j < 4; j++) {
            manager->add_test_decision(
                make_unique<PlayerDecision>(loser_move.at(j)));
        }
        for (int j = 0; j < 4; j++) {
            manager->add_test_decision(
                make_unique<PlayerDecision>(winner_move.at(j)));
        }
    }
}

auto
EndgameTest::HQ_destroyed_with_winner(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    out_of_tiles_with_winner_prepare_moves(&game);
    if (verbose) {
        cout << "Loser HQ should be destroyed and he should lose" << endl;
        game.set_gui(make_unique<TextGui>());
    }
    game_data.get_active_player()->get_resources()->get_army()->set_headquarter(
        make_a_weak_HQ(game_data.get_active_player()));
    game_data.get_previous_player()
        ->get_resources()
        ->get_army()
        ->set_headquarter(make_a_weak_HQ(game_data.get_previous_player()));
    game.execute_whole_game();
    Player* alleged_winner =
        game_data.get_board()->get_tile_ptr(winner_hq_coords)->get_owner();
    Player* defacto_winner = game.get_data().get_winner();
    return defacto_winner == alleged_winner and game_data.is_game_finished and
           not game_data.get_board()->get_tile_ptr(loser_hq_coords);
}

auto
EndgameTest::out_of_tiles_board_full(bool verbose) -> bool
{
    Game game{};
    GameData& game_data = game.get_data();
    MolochBuilder moloch_builder{};
    if (verbose) {
        cout << "Game should have two battles resulting in first player winning"
             << endl;
        game.set_gui(make_unique<TextGui>());
    }
    auto active_player = game_data.get_active_player();
    auto headquarter1 = moloch_builder.make_headquarter(active_player);
    unique_ptr<Tile> stormtrooper = moloch_builder.make_stormtrooper(active_player);
    vector<unique_ptr<Tile>> active_player_tiles(0);
    active_player_tiles.push_back(move(stormtrooper));
    unique_ptr<Army> active_player_army = make_unique<Army>(move(headquarter1), active_player_tiles);
    active_player->get_resources()->set_army(move(active_player_army));

    auto inactive_player = game_data.get_previous_player();
    auto headquarter2 = moloch_builder.make_headquarter(inactive_player);
    vector<unique_ptr<Tile>> inactive_player_tiles(0);
    unique_ptr<Army> inactive_player_army = make_unique<Army>(move(headquarter2), inactive_player_tiles);
    inactive_player->get_resources()->set_army(move(inactive_player_army));
    auto empty_indexes = game_data.get_board()->get_empty_indexes();
    for (const auto& coords : empty_indexes)
    {
        game_data.get_board()->set_tile(Coordinates(coords.x, coords.y), moloch_builder.make_blocker(inactive_player));
    }
    game_data.get_board()->pick_up_tile(Coordinates{2, 2});
    game_data.get_board()->set_tile(Coordinates(2, 2), moloch_builder.make_blocker(inactive_player));
    game_data.get_board()->pick_up_tile(Coordinates{2, 0});
    game_data.get_board()->pick_up_tile(Coordinates{2, 4});
    game_data.get_board()->pick_up_tile(Coordinates{2, 8});

    out_of_tiles_board_full_prepare_moves(&game);
    game.loop_game();
    Player* winner = game.get_data().get_winner();

    return winner == active_player and game_data.is_game_finished and
           game_data.is_one_player_out_of_tiles and
           game_data.is_final_battle_over and
           game_data.get_board()->get_tile_ptr(Coordinates{2, 0})->get_hp()==19;
}

void 
EndgameTest::out_of_tiles_board_full_prepare_moves(Game* game)
{
    PlayerDecisionManager* manager = game->get_data().get_decision_manager();
    unique_ptr<PlayerDecision> set_headquarter1 = make_unique<PlayerDecision>();
    set_headquarter1->set_tile_target_location(2, 8, 0);
    manager->add_test_decision(move(set_headquarter1));
    unique_ptr<PlayerDecision> set_headquarter2 = make_unique<PlayerDecision>();
    set_headquarter2->set_tile_target_location(2, 0, 0);
    manager->add_test_decision(move(set_headquarter2));

    array<PlayerDecision, 4> universal_tie_move{};
    universal_tie_move.at(0).set_whether_to_use_innates(false);
    universal_tie_move.at(1).set_tile_to_use_index(0);
    universal_tie_move.at(2).set_chosen_way_of_tile_usage(USE);
    universal_tie_move.at(3).set_tile_target_location(2, 4, N);
    for (int j = 0; j < 4; j++) {
        manager->add_test_decision(
            make_unique<PlayerDecision>(universal_tie_move.at(j)));
    }
}

auto
EndgameTest::make_a_weak_HQ(Player* owner) -> unique_ptr<Tile>
{
    unique_ptr<Tile> headquarter =
        make_unique<Tile>(true, "HQ", owner, 1, set<int>{ 0 });
    return headquarter;
}
