#include <Army.h>
#include <Battle.h>
#include <BattleAction.h>
#include <Board.h>
#include <Colours.h>
#include <DamageDealingTest.h>
#include <Gui.h>
#include <MolochBuilder.h>
#include <OutpostBuilder.h>
#include <Player.h>
#include <PlayerLogic.h>
#include <PlayerResources.h>
#include <Shoot.h>
#include <Sniper.h>
#include <Test.h>
#include <TextGui.h>
#include <Tile.h>

#include <memory>

using namespace std;

static OutpostBuilder outpost_builder{};
static MolochBuilder moloch_builder{};
static unique_ptr<Gui> gui = make_unique<TextGui>();
static unique_ptr<Player> player1 =
    make_unique<Player>("player A", COLOURS.GREEN);
static unique_ptr<Player> player2 =
    make_unique<Player>("player B", COLOURS.RED);

auto
DamageDealingTest::test_function(bool verbose) -> bool
{
    return single_test(*damage_is_applied_to_enemy, verbose) and
           single_test(*armour_blocks_weak_attacks, verbose) and
           single_test(*armour_weakens_strong_attacks, verbose) and
           single_test(*hqs_dont_damage_hqs, verbose);
}

auto
DamageDealingTest::damage_is_applied_to_enemy(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    Board* board = game_data.get_board();
    unique_ptr<Tile> shooter = outpost_builder.make_HMG(player1.get());
    unique_ptr<Tile> enemy = moloch_builder.make_headquarter(player2.get());
    Coordinates coords_shooter = Coordinates(2, 0);
    Coordinates coords_enemy = Coordinates(2, 8);
    board->set_tile(coords_shooter, move(shooter));
    board->set_tile(coords_enemy, move(enemy));
    board->rotate_tile(coords_shooter, S);
    if (verbose)
        gui->print_board(*board);
    game.loop_for_single_battle();
    if (verbose)
        gui->print_board(*board);
    // Check if tests passed
    return board->get_tile_ptr(coords_enemy)->get_hp() == 18;
}

auto
DamageDealingTest::armour_blocks_weak_attacks(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    Board* board = game_data.get_board();
    unique_ptr<Tile> shooter = outpost_builder.make_HMG(player1.get());
    unique_ptr<Tile> armoured_enemy =
        moloch_builder.make_juggernaut(player2.get());
    Coordinates coords_shooter = Coordinates(2, 0);
    Coordinates coords_armoured = Coordinates(2, 8);
    board->set_tile(coords_shooter, move(shooter));
    board->set_tile(coords_armoured, move(armoured_enemy));
    board->rotate_tile(coords_shooter, S);
    if (verbose)
        gui->print_board(*board);
    game.loop_for_single_battle();
    if (verbose)
        gui->print_board(*board);
    // Check if tests passed
    return board->get_tile_ptr(coords_armoured)->get_hp() == 2;
}

auto
DamageDealingTest::armour_weakens_strong_attacks(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    Board* board = game_data.get_board();
    unique_ptr<Tile> shooter = outpost_builder.make_annihilator(player1.get());
    unique_ptr<Tile> armoured_enemy =
        moloch_builder.make_juggernaut(player2.get());
    Coordinates coords_shooter = Coordinates(2, 0);
    Coordinates coords_armoured = Coordinates(2, 8);
    board->set_tile(coords_shooter, move(shooter));
    board->set_tile(coords_armoured, move(armoured_enemy));
    board->rotate_tile(coords_shooter, S);
    if (verbose)
        gui->print_board(*board);
    game.loop_for_single_battle();
    if (verbose)
        gui->print_board(*board);
    // Check if tests passed
    return board->get_tile_ptr(coords_armoured)->get_hp() == 1;
}

auto
DamageDealingTest::hqs_dont_damage_hqs(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    Board* board = game_data.get_board();
    unique_ptr<Tile> hq1 = outpost_builder.make_headquarter(player1.get());
    unique_ptr<Tile> hq2 = moloch_builder.make_headquarter(player2.get());
    Coordinates coords_1 = Coordinates(2, 0);
    Coordinates coords_2 = Coordinates(2, 2);
    board->set_tile(coords_1, move(hq1));
    board->set_tile(coords_2, move(hq2));
    board->rotate_tile(coords_1, S);
    if (verbose)
        gui->print_board(*board);
    game.loop_for_single_battle();
    if (verbose)
        gui->print_board(*board);
    // Check if tests passed
    return board->get_tile_ptr(coords_2)->get_hp() == 20 and
           board->get_tile_ptr(coords_1)->get_hp() == 20;
}
