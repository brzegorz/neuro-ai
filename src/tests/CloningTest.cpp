#include <Army.h>
#include <BattleAction.h>
#include <CloningTest.h>
#include <Colours.h>
#include <Directions.h>
#include <Game.h>
#include <GameData.h>
#include <Hit.h>
#include <Node.h>
#include <OutpostBuilder.h>
#include <PlaceHeadquarters.h>
#include <PlaceOnBoard.h>
#include <Player.h>
#include <PlayerLogic.h>
#include <PlayerResources.h>
#include <Shoot.h>
#include <TextGui.h>
#include <Tile.h>

#include <iostream>
#include <memory>

using namespace std;

static unique_ptr<Player> player1 =
    make_unique<Player>("player A", COLOURS.BLUE);
static unique_ptr<Player> player2 =
    make_unique<Player>("player B", COLOURS.RED);
static OutpostBuilder army_builder{};

auto
CloningTest::test_function(bool verbose) -> bool
{
    return single_test(*usage_func_clone_test, verbose) and
           single_test(*shooting_action_clone_test, verbose) and
           single_test(*tile_clone_test, verbose) and
           single_test(*board_clone_test, verbose) and
           single_test(*game_data_clone_test, verbose) and
           single_test(*game_clone_test, verbose);
}

auto
CloningTest::usage_func_clone_test(bool verbose) -> bool
{
    unique_ptr<UsageFunction> func1 = make_unique<PlaceOnBoard>();
    unique_ptr<UsageFunction> func2 = func1->clone();
    bool passed = *func1 == *func2;
    if (verbose) {
        if (not passed)
            Test::print_error_message("Cloning of usage functions not working");
        else
            Test::print_ok_message("Cloning of usage functions working");
    }
    return passed;
}

auto
CloningTest::shooting_action_clone_test(bool verbose) -> bool
{
    unique_ptr<BattleAction> shoot1 = make_unique<Shoot>(1, N);
    unique_ptr<BattleAction> shoot2 = shoot1->clone();
    bool passed = *shoot1 == *shoot2;
    if (verbose) {
        if (not passed)
            Test::print_error_message(
                "Cloning of shooting actions not working");
        else
            Test::print_ok_message("Cloning of shooting actions working");
    }
    return passed;
}

auto
CloningTest::tile_clone_test(bool verbose) -> bool
{
    unique_ptr<Tile> alef = army_builder.make_commando(player1.get());
    unique_ptr<Tile> alefalef = make_unique<Tile>(*alef);
    unique_ptr<Tile> hq = army_builder.make_headquarter(player1.get());
    unique_ptr<Tile> hq1 = make_unique<Tile>(*hq);
    bool equal_tiles = *hq == *hq1;
    if (verbose) {
        if (not equal_tiles)
            Test::print_error_message("Copy constructor of tile not working");
        else
            Test::print_ok_message("Copy constructor of tile working");
    }
    return equal_tiles;
}

auto
CloningTest::board_clone_test(bool verbose) -> bool
{
    Board alef = Board();
    unique_ptr<Tile> shooter1 = army_builder.make_commando(player1.get());
    alef.set_tile(Coordinates(2, 0), move(shooter1));
    unique_ptr<Tile> hq = army_builder.make_headquarter(player1.get());
    alef.set_tile(Coordinates(1, 1), move(hq));
    Board alefalef{ alef };
    bool passed = alef == alefalef;
    if (verbose) {
        unique_ptr<Gui> gui = make_unique<TextGui>();
        gui->print_board(alef);
        gui->print_board(alefalef);
        if (not passed)
            Test::print_error_message("Board cloning not working");
        else
            Test::print_ok_message("Board cloning is working");
    }
    return passed;
}

auto
CloningTest::game_data_clone_test(bool verbose) -> bool
{
    GameData alef = GameData();
    GameData alefalef = GameData(alef);
    bool passed = alef == alefalef;
    if (verbose) {
        if (not passed)
            Test::print_error_message(
                "GameData copying constructor not working");
        else
            Test::print_ok_message("GameData copying constructor working");
    }
    return passed;
}

auto
CloningTest::game_clone_test(bool verbose) -> bool
{
    Game alef = Game();
    alef.set_next_state(make_unique<PlaceHeadquarters>());
    Game alefalef = Game(alef);
    bool passed = alef.get_data() == alefalef.get_data();
    if (verbose) {
        if (not passed)
            Test::print_error_message("Game copying constructor not working");
        else
            Test::print_ok_message("Game copying constructor working");
    }
    return passed;
}
