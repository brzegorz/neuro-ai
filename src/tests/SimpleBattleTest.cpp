#include <Army.h>
#include <Battle.h>
#include <BattleAction.h>
#include <Board.h>
#include <Colours.h>
#include <Game.h>
#include <Gui.h>
#include <MolochBuilder.h>
#include <OutpostBuilder.h>
#include <Player.h>
#include <PlayerLogic.h>
#include <PlayerResources.h>
#include <Shoot.h>
#include <SimpleBattleTest.h>
#include <Test.h>
#include <TextGui.h>
#include <Tile.h>

#include <memory>

using namespace std;

auto
SimpleBattleTest::test_function(bool verbose) -> bool
{
    Game game = prepare_basic_game();
    GameData& game_data = game.get_data();
    Board* board = game_data.get_board();
    OutpostBuilder outpost_builder{};
    MolochBuilder moloch_builder{};
    unique_ptr<Gui> gui = make_unique<TextGui>();
    Player* player1 = game_data.get_active_player();
    Player* player2 = game_data.get_previous_player();
    unique_ptr<Tile> shooter1 = outpost_builder.make_HMG(player1);
    unique_ptr<Tile> shooter2 = moloch_builder.make_clown(player2);
    Coordinates coords_winner = Coordinates(2, 0);
    Coordinates coords_loser = Coordinates(2, 8);
    board->set_tile(coords_winner, move(shooter1));
    board->set_tile(coords_loser, move(shooter2));
    board->rotate_tile(coords_winner, S);
    if (verbose)
        gui->print_board(*board);
    game.loop_for_single_battle();
    if (verbose)
        gui->print_board(*board);
    // Check if tests passed
    Tile* destroyed = board->get_tile_ptr(coords_loser);
    Tile* winner = board->get_tile_ptr(coords_winner);
    if (destroyed or !winner)
        return false;
    return true;
}
