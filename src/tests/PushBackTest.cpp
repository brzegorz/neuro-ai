#include <Army.h>
#include <BattleAction.h>
#include <Board.h>
#include <Colours.h>
#include <Game.h>
#include <Gui.h>
#include <MolochBuilder.h>
#include <OutpostBuilder.h>
#include <Player.h>
#include <PushBackTest.h>
#include <Test.h>
#include <TestArmyBuilder.h>
#include <TextGui.h>
#include <Tile.h>

#include <memory>

using namespace std;

static MolochBuilder moloch_builder{};
static OutpostBuilder outpost_builder{};
static unique_ptr<Gui> gui = make_unique<TextGui>();

auto
PushBackTest::test_function(bool verbose) -> bool
{
    return single_test(*lonely_tile_unusubale, verbose) and
           single_test(*invalid_enemy_untargetable, verbose) and
           single_test(*valid_enemy_targetable, verbose) and
           single_test(*proper_valid_decisions, verbose);
}

auto
PushBackTest::lonely_tile_unusubale(bool verbose) -> bool
{
    Game game{};
    Board* board = game.get_data().get_board();

    const Coordinates pusher_coords{ 2, 8 };
    const Coordinates far_enemy_coords{ 2, 4 };
    Player* pushing_player = game.get_data().get_active_player();
    Player* pushed_player = game.get_data().get_previous_player();
    board->set_tile(pusher_coords, moloch_builder.make_hybrid(pushing_player));
    board->set_tile(far_enemy_coords,
                    outpost_builder.make_headquarter(pushed_player));
    unique_ptr<Tile> push_back = moloch_builder.make_push_back(pushing_player);
    bool is_push_possible = push_back->is_usable(&game);
    if (verbose) {
        cout << "It should be impossible to use a push_back tile" << endl;
        gui->print_board(*board);
    }
    return is_push_possible == false;
}

auto
PushBackTest::invalid_enemy_untargetable(bool verbose) -> bool
{
    Game game{};
    Board* board = game.get_data().get_board();

    const Coordinates pusher_coords{ 2, 8 };
    const Coordinates enemy_coords{ 2, 6 };
    Player* pushing_player = game.get_data().get_active_player();
    Player* pushed_player = game.get_data().get_previous_player();
    board->set_tile(pusher_coords, moloch_builder.make_hybrid(pushing_player));
    board->set_tile(enemy_coords,
                    outpost_builder.make_headquarter(pushed_player));
    board->set_tile(Coordinates{ 1, 5 },
                    outpost_builder.make_commando(pushed_player));
    board->set_tile(Coordinates{ 2, 4 },
                    outpost_builder.make_commando(pushed_player));
    board->set_tile(Coordinates{ 3, 5 },
                    outpost_builder.make_commando(pushed_player));
    unique_ptr<Tile> push_back = moloch_builder.make_push_back(pushing_player);
    bool is_push_possible = push_back->is_usable(&game);
    if (verbose) {
        cout << "It should be impossible to use a push_back tile" << endl;
        gui->print_board(*board);
    }
    return is_push_possible == false;
}

auto
PushBackTest::valid_enemy_targetable(bool verbose) -> bool
{
    Game game{};
    Board* board = game.get_data().get_board();

    const Coordinates pusher_coords{ 2, 8 };
    set<Coordinates> enemy_coords_set{ Coordinates{ 2, 6 },
                                       Coordinates{ 1, 5 },
                                       Coordinates{ 2, 4 } };
    Player* pushing_player = game.get_data().get_active_player();
    Player* pushed_player = game.get_data().get_previous_player();
    board->set_tile(pusher_coords, moloch_builder.make_hybrid(pushing_player));
    for (const Coordinates& enemy_coords : enemy_coords_set)
        board->set_tile(enemy_coords,
                        outpost_builder.make_commando(pushed_player));
    unique_ptr<Tile> push_back = moloch_builder.make_push_back(pushing_player);
    bool is_push_possible = push_back->is_usable(&game);
    if (verbose) {
        cout << "It should be possible to use a push_back tile" << endl;
        gui->print_board(*board);
    }
    return is_push_possible == true;
}

auto
PushBackTest::proper_valid_decisions(bool verbose) -> bool
{
    Game game{};
    Board* board = game.get_data().get_board();

    const Coordinates pusher_coords{ 2, 6 };
    set<Coordinates> enemy_coords_set{ Coordinates{ 2, 8 },
                                       Coordinates{ 1, 7 },
                                       Coordinates{ 0, 6 },
                                       Coordinates{ 3, 7 } };
    Player* pushing_player = game.get_data().get_active_player();
    Player* pushed_player = game.get_data().get_previous_player();
    board->set_tile(pusher_coords, moloch_builder.make_hybrid(pushing_player));
    for (const Coordinates& enemy_coords : enemy_coords_set)
        board->set_tile(enemy_coords,
                        outpost_builder.make_commando(pushed_player));
    unique_ptr<Tile> push_back = moloch_builder.make_push_back(pushing_player);
    vector<PlayerDecision> valid_decisions = push_back->get_valid_moves(&game);
    const DIRECTION true_direction = SE;
    PlayerDecision one_true_decision{};
    one_true_decision.set_push_source_and_direction(pusher_coords,
                                                    true_direction);
    if (verbose) {
        cout << "Actual valid target is ";
        one_true_decision.print_decision();
        cout << endl;
        cout << "Code returned following decisions as valid: " << endl;
        for (const PlayerDecision& decision : valid_decisions) {
            decision.print_decision();
            cout << endl;
        }
        gui->print_board(*board);
    }
    if (valid_decisions.size() != 1)
        return false;
    PlayerDecision alleged_true_decision = valid_decisions.at(0);
    pair<Coordinates, DIRECTION> alleged_true_params;
    alleged_true_params = alleged_true_decision.get_push_source_and_direction();
    return alleged_true_params == make_pair(pusher_coords, true_direction);
}
