#include <Directions.h>
#include <Game.h>
#include <Gui.h>
#include <InnateMovement.h>
#include <PlayerLogic.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <memory>
#include <stdexcept>

using namespace std;

void
InnateMovement::change_board(Game* game)
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    const Coordinates& source = game_data.tile_using_innate_coordinates;
    pair<Coordinates, DIRECTION> target;
    target = get_target_location(game);
    unique_ptr<Tile> tile = board->pick_up_tile(source);
    tile->rotate(target.second);
    board->set_tile(target.first, move(tile));
    already_used = true;
}

auto
InnateMovement::get_target_location(Game* game) -> pair<Coordinates, DIRECTION>
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        unique_ptr<PlayerDecision> movement_decision;
        movement_decision = game_data.get_active_player()
                                ->get_logic()
                                ->pick_innate_movement_target(game);
        game_data.get_decision_manager()->add_player_decision(
            move(movement_decision));
    }
    unique_ptr<PlayerDecision> result =
        game_data.get_decision_manager()->get_player_decision();
    return result->get_tile_target_location();
}

auto
InnateMovement::is_usable(Game* game) const -> bool
{
    if (already_used)
        return false;
    GameData& game_data = game->get_data();
    const Board* board = game_data.get_board();
    const Coordinates& source = game_data.tile_using_innate_coordinates;
    //~ cout << "Checking valid innate moves for " << source << endl;
    const Tile* moving_tile = board->get_tile_ptr(source);
    if (moving_tile->is_in_net())
        return false;
    return true;
}

auto
InnateMovement::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    GameData& game_data = game->get_data();
    const Board* board = game_data.get_board();
    const Coordinates& source = game_data.tile_using_innate_coordinates;
    set<pair<Coordinates, Coordinates>> valid_source_target{};

    valid_source_target.insert(make_pair(source, source));
    for (Coordinates target : board->get_empty_neighbours(source)) {
        valid_source_target.insert(make_pair(source, target));
        const bool has_double_movement =
            board->get_tile_ptr(source)->get_owner()->has_double_movement();
        if (has_double_movement) {
            for (Coordinates far_target : board->get_empty_neighbours(target))
                valid_source_target.insert(make_pair(source, far_target));
        }
    }

    vector<PlayerDecision> valid_moves{};
    for (auto [source, target] : valid_source_target) {
        for (DIRECTION rotation : all_directions) {
            PlayerDecision valid_decision{};
            valid_decision.set_tile_target_location(
                target.x, target.y, rotation);
            valid_moves.push_back(valid_decision);
        }
    }
    return valid_moves;
}

auto
InnateMovement::get_ascii_description() const -> string
{
    return "MOVE";
}

auto
InnateMovement::clone() const -> unique_ptr<InnateFunction>
{
    return make_unique<InnateMovement>(*this);
}

auto
InnateMovement::operator==(const InnateFunction& other) const -> bool
{
    if (!InnateFunction::operator==(other))
        return false;
    return true;
}
