#include <InnateFunction.h>

using namespace std;

auto
InnateFunction::operator==(const InnateFunction& other) const -> bool
{
    if (typeid(*this) != typeid(other))
        return false;
    return true;
}

auto
InnateFunction::operator!=(const InnateFunction& other) const -> bool
{
    return !(*this == other);
}
