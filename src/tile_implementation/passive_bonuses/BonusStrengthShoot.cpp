#include <Board.h>
#include <BonusStrengthShoot.h>
#include <Player.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <limits>

using namespace std;

void
BonusStrengthShoot::apply_bonus(Board& board, Coordinates target_coords)
{
    if (not is_target_valid(board, target_coords))
        return;
    Tile* tile_to_change = board.get_tile_ptr(target_coords);
    tile_to_change->modify_shoot_strength(1);
    was_target_changed = true;
}

void
BonusStrengthShoot::reverse_bonus(Board& board, Coordinates target_coords)
{
    if (not was_target_changed)
        return;
    Tile* tile_to_unchange = board.get_tile_ptr(target_coords);
    tile_to_unchange->modify_shoot_strength(-1);
    was_target_changed = false;
}

auto
BonusStrengthShoot::get_string() const -> string
{
    return "S+";
}

auto
BonusStrengthShoot::clone() const -> unique_ptr<PassiveBonus>
{
    return make_unique<BonusStrengthShoot>(*this);
}

auto
BonusStrengthShoot::operator==(const PassiveBonus& other) const -> bool
{
    if (!PassiveBonus::operator==(other))
        return false;
    const auto& other_casted = dynamic_cast<const BonusStrengthShoot&>(other);
    return (this->direction == other_casted.direction);
}

auto
BonusStrengthShoot::operator<(const PassiveBonus& other) const -> bool
{
    // check if actions are of the same class, e.g. BonusStrengthShoot and
    // BonusStrengthShoot:
    if (typeid(*this) != typeid(other))
        return PassiveBonus::operator<(other);
    const auto& other_casted = dynamic_cast<const BonusStrengthShoot&>(other);
    return this->direction < other_casted.direction;
}
