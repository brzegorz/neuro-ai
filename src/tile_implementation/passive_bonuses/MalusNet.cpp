#include <Board.h>
#include <MalusNet.h>
#include <Player.h>
#include <TextGui.h>
#include <Tile.h>
#include <Tile_graphic_parameters.h>

#include <algorithm>
#include <iostream>
#include <limits>

using namespace std;

bool MalusNet::is_mutual_nets_being_resolved_now = false;

void
MalusNet::apply_bonus(Board& board, Coordinates target_coords)
{
    if (not is_target_valid(board, target_coords))
        return;
    Tile* tile_to_change = board.get_tile_ptr(target_coords);
    tile_to_change->net_unit(board, target_coords);
    was_target_changed = true;
    resolve_mutual_nets(board, target_coords);
}

void
MalusNet::reverse_bonus(Board& board, Coordinates target_coords)
{
    if (not was_target_changed)
        return;
    Tile* tile_to_unchange = board.get_tile_ptr(target_coords);
    if (not tile_to_unchange->is_in_net())
        return;
    tile_to_unchange->unnet_unit(board, target_coords);
    was_target_changed = false;
}

void
MalusNet::switch_allegiance(Board& board, Coordinates bonus_coords)
{}

void
MalusNet::update_ascii_gui(const tile_parameters& p, TextGui* gui) const
{
    DIRECTION dir = this->direction;
    vector<int> xs{};
    vector<int> ys{};
    string ascii_symbol = "#";
    switch (dir) {
        case N: {
            int start_x = p.tile_height + 1;
            int end_x = p.tile_height + p.tile_width;
            int start_y = 2;
            for (int i = 0; i < p.tile_height / 2 + 1; i++) {
                xs.push_back(start_x + i);
                ys.push_back(start_y + i);
                xs.push_back(end_x - i);
                ys.push_back(start_y + i);
            }
            int horizontal_y = start_y + p.tile_height / 2;
            int horizontal_x_start = start_x + p.tile_height / 2;
            for (int i = 0; i < p.tile_width - p.tile_height; i++) {
                xs.push_back(horizontal_x_start + i);
                ys.push_back(horizontal_y);
            }
        } break;
        case S: {
            int start_x = p.tile_height + 1;
            int end_x = p.tile_height + p.tile_width;
            int start_y = 2 * p.tile_height + 1;
            for (int i = 0; i < p.tile_height / 2 + 1; i++) {
                xs.push_back(start_x + i);
                ys.push_back(start_y - i);
                xs.push_back(end_x - i);
                ys.push_back(start_y - i);
            }
            int horizontal_y = start_y - p.tile_height / 2;
            int horizontal_x_start = start_x + p.tile_height / 2;
            for (int i = 0; i < p.tile_width - p.tile_height; i++) {
                xs.push_back(horizontal_x_start + i);
                ys.push_back(horizontal_y);
            }
        } break;
        case NE: {
            int end_x = p.tile_height + p.tile_width;
            int start_y = 2;
            for (int i = 0; i < p.tile_height / 2 + 1; i++) {
                xs.push_back(end_x - i);
                ys.push_back(start_y + i);
            }
            int horizontal_x = p.tile_height / 2 + p.tile_width - 1;
            int horizontal_y = p.tile_height + p.tile_height / 4;
            for (int i = 1; i < p.tile_height + 3; i++) {
                xs.push_back(horizontal_x + i);
                ys.push_back(horizontal_y);
            }
        } break;
        case SW: {
            int start_x = p.tile_height + 1;
            int start_y = 2 * p.tile_height + 1;
            for (int i = 0; i < p.tile_height / 2 + 1; i++) {
                xs.push_back(start_x + i);
                ys.push_back(start_y - i);
            }
            int horizontal_x = p.tile_height + 2;
            int horizontal_y = 2 * p.tile_height - p.tile_height / 2;
            for (int i = 0; i < p.tile_height + 1; i++) {
                xs.push_back(horizontal_x - i);
                ys.push_back(horizontal_y);
            }
        } break;
        case NW: {
            int start_x = p.tile_height + 1;
            int start_y = 2;
            for (int i = 0; i < p.tile_height / 2 + 1; i++) {
                xs.push_back(start_x + i);
                ys.push_back(start_y + i);
            }
            int horizontal_x = p.tile_height / 2;
            int horizontal_y = p.tile_height + p.tile_height / 4;
            for (int i = 0; i < p.tile_height + 2; i++) {
                xs.push_back(horizontal_x + i);
                ys.push_back(horizontal_y);
            }
        } break;
        case SE: {
            int end_x = p.tile_height + p.tile_width;
            int start_y = 2 * p.tile_height + 1;
            for (int i = 0; i < p.tile_height / 2 + 1; i++) {
                xs.push_back(end_x - i);
                ys.push_back(start_y - i);
            }
            int horizontal_x = p.tile_width + p.tile_height + 3;
            int horizontal_y = 2 * p.tile_height - p.tile_height / 2;
            for (int i = 0; i < p.tile_height + 1; i++) {
                xs.push_back(horizontal_x - i);
                ys.push_back(horizontal_y);
            }
        } break;
        default:
            throw invalid_argument("Invalid direction specified");
    }
    for (int i = 0; i < (int)xs.size(); i++)
        gui->add_text_at_particular_hex(p, xs.at(i), ys.at(i), ascii_symbol);
}

auto
MalusNet::get_string() const -> string
{
    return "NET";
}

void
MalusNet::resolve_mutual_nets(Board& board, Coordinates victim_coords)
{
    if (is_mutual_nets_being_resolved_now)
        return;
    is_mutual_nets_being_resolved_now = true;
    DIRECTION netter_direction = opposite_direction(direction);
    Tile* netter = board.get_tile_ptr(
        board.get_neighbour(victim_coords, netter_direction));
    Tile* victim = board.get_tile_ptr(victim_coords);
    victim->unnet_unit(board, victim_coords);
    if (not netter->is_in_net()) {
        victim->net_unit(board, victim_coords);
    } else {
        netter->unnet_when_mutual_netting();
        was_target_changed = false;
    }
    is_mutual_nets_being_resolved_now = false;
}

auto
MalusNet::clone() const -> unique_ptr<PassiveBonus>
{
    return make_unique<MalusNet>(*this);
}

auto
MalusNet::operator==(const PassiveBonus& other) const -> bool
{
    if (!PassiveBonus::operator==(other))
        return false;
    const auto& other_casted = dynamic_cast<const MalusNet&>(other);
    return (this->direction == other_casted.direction);
}

auto
MalusNet::operator<(const PassiveBonus& other) const -> bool
{
    // check if actions are of the same class, e.g. MalusNet and MalusNet:
    if (typeid(*this) != typeid(other))
        return PassiveBonus::operator<(other);
    const auto& other_casted = dynamic_cast<const MalusNet&>(other);
    return this->direction < other_casted.direction;
}
