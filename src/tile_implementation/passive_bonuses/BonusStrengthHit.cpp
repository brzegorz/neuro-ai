#include <Board.h>
#include <BonusStrengthHit.h>
#include <Player.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <limits>

using namespace std;

void
BonusStrengthHit::apply_bonus(Board& board, Coordinates target_coords)
{
    if (not is_target_valid(board, target_coords))
        return;
    Tile* tile_to_change = board.get_tile_ptr(target_coords);
    tile_to_change->modify_hit_strength(1);
    was_target_changed = true;
}

void
BonusStrengthHit::reverse_bonus(Board& board, Coordinates target_coords)
{
    if (not was_target_changed)
        return;
    Tile* tile_to_unchange = board.get_tile_ptr(target_coords);
    tile_to_unchange->modify_hit_strength(-1);
    was_target_changed = false;
}

auto
BonusStrengthHit::get_string() const -> string
{
    return "H+";
}

auto
BonusStrengthHit::clone() const -> unique_ptr<PassiveBonus>
{
    return make_unique<BonusStrengthHit>(*this);
}

auto
BonusStrengthHit::operator==(const PassiveBonus& other) const -> bool
{
    if (!PassiveBonus::operator==(other))
        return false;
    const auto& other_casted = dynamic_cast<const BonusStrengthHit&>(other);
    return (this->direction == other_casted.direction);
}

auto
BonusStrengthHit::operator<(const PassiveBonus& other) const -> bool
{
    // check if actions are of the same class, e.g. BonusStrengthHit and
    // BonusStrengthHit:
    if (typeid(*this) != typeid(other))
        return PassiveBonus::operator<(other);
    const auto& other_casted = dynamic_cast<const BonusStrengthHit&>(other);
    return this->direction < other_casted.direction;
}
