#include <Board.h>
#include <BonusReconCenter.h>
#include <Player.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <limits>

using namespace std;

void
BonusReconCenter::apply_bonus(Board& board, Coordinates target_coords)
{
    if (was_target_changed)
        return;
    DIRECTION recon_direction = opposite_direction(direction);
    const Tile* recon =
        board.get_tile_ptr(board.get_neighbour(target_coords, recon_direction));
    recon->get_owner()->give_double_movement();
    was_target_changed = true;
}

void
BonusReconCenter::reverse_bonus(Board& board, Coordinates target_coords)
{
    if (not was_target_changed)
        return;
    DIRECTION recon_direction = opposite_direction(direction);
    const Tile* recon =
        board.get_tile_ptr(board.get_neighbour(target_coords, recon_direction));
    recon->get_owner()->remove_double_movement();
    was_target_changed = false;
}

void
BonusReconCenter::update_ascii_gui(const tile_parameters& p, TextGui* gui) const
{}

auto
BonusReconCenter::get_string() const -> string
{
    return "MOVEx2";
}

auto
BonusReconCenter::clone() const -> unique_ptr<PassiveBonus>
{
    return make_unique<BonusReconCenter>(*this);
}

auto
BonusReconCenter::operator==(const PassiveBonus& other) const -> bool
{
    if (!PassiveBonus::operator==(other))
        return false;
    const auto& other_casted = dynamic_cast<const BonusReconCenter&>(other);
    return (this->direction == other_casted.direction);
}

auto
BonusReconCenter::operator<(const PassiveBonus& other) const -> bool
{
    // check if actions are of the same class, e.g. BonusReconCenter and
    // BonusReconCenter:
    if (typeid(*this) != typeid(other))
        return PassiveBonus::operator<(other);
    const auto& other_casted = dynamic_cast<const BonusReconCenter&>(other);
    return this->direction < other_casted.direction;
}
