#include <Board.h>
#include <BonusInitiative.h>
#include <MalusInitiative.h>
#include <Player.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <limits>

using namespace std;

void
MalusInitiative::apply_bonus(Board& board, Coordinates target_coords)
{
    if (not is_target_valid(board, target_coords))
        return;
    Tile* tile_to_change = board.get_tile_ptr(target_coords);
    const set<int>& tile_initiative = tile_to_change->get_initiative();
    if (tile_initiative.size() == 0)
        return;
    if (tile_initiative.size() == 1 and *tile_initiative.begin() == 0)
        return;
    set<int> new_initiative{};
    old_target_initiative = set<int>(tile_initiative);
    for (int initiative : tile_initiative) {
        if (initiative != 0)
            new_initiative.insert(initiative - 1);
        else
            new_initiative.insert(0);
    }

    tile_to_change->set_initiative(new_initiative);
    was_target_changed = true;
}

void
MalusInitiative::reverse_bonus(Board& board, Coordinates target_coords)
{
    if (not was_target_changed)
        return;
    Tile* tile_to_unchange = board.get_tile_ptr(target_coords);
    tile_to_unchange->set_initiative(old_target_initiative);
    was_target_changed = false;
}

auto
MalusInitiative::get_string() const -> string
{
    return "I-";
}

auto
MalusInitiative::clone() const -> unique_ptr<PassiveBonus>
{
    return make_unique<MalusInitiative>(*this);
}

auto
MalusInitiative::operator==(const PassiveBonus& other) const -> bool
{
    if (!PassiveBonus::operator==(other))
        return false;
    const auto& other_casted = dynamic_cast<const MalusInitiative&>(other);
    return (this->direction == other_casted.direction);
}

auto
MalusInitiative::operator<(const PassiveBonus& other) const -> bool
{
    // check if actions are of the same class, e.g. MalusInitiative and
    // MalusInitiative:
    if (typeid(*this) != typeid(other))
        return PassiveBonus::operator<(other);
    const auto& other_casted = dynamic_cast<const MalusInitiative&>(other);
    return this->direction < other_casted.direction;
}
