#include <Board.h>
#include <BonusInitiative.h>
#include <Player.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <limits>

using namespace std;

void
BonusInitiative::apply_bonus(Board& board, Coordinates target_coords)
{
    if (not is_target_valid(board, target_coords))
        return;
    Tile* tile_to_change = board.get_tile_ptr(target_coords);
    const set<int>& tile_initiative = tile_to_change->get_initiative();
    set<int> new_initiative{};
    for (int initiative : tile_initiative) {
        new_initiative.insert(initiative + 1);
    }
    tile_to_change->set_initiative(new_initiative);
    was_target_changed = true;
}

void
BonusInitiative::reverse_bonus(Board& board, Coordinates target_coords)
{
    if (not was_target_changed)
        return;
    Tile* tile_to_unchange = board.get_tile_ptr(target_coords);
    const set<int>& tile_initiative = tile_to_unchange->get_initiative();
    set<int> new_initiative{};
    for (int initiative : tile_initiative) {
        new_initiative.insert(initiative - 1);
    }
    tile_to_unchange->set_initiative(new_initiative);
    was_target_changed = false;
}

auto
BonusInitiative::get_string() const -> string
{
    return "I+";
}

auto
BonusInitiative::clone() const -> unique_ptr<PassiveBonus>
{
    return make_unique<BonusInitiative>(*this);
}

auto
BonusInitiative::operator==(const PassiveBonus& other) const -> bool
{
    if (!PassiveBonus::operator==(other))
        return false;
    const auto& other_casted = dynamic_cast<const BonusInitiative&>(other);
    return (this->direction == other_casted.direction);
}

auto
BonusInitiative::operator<(const PassiveBonus& other) const -> bool
{
    // check if actions are of the same class, e.g. BonusInitiative and
    // BonusInitiative:
    if (typeid(*this) != typeid(other))
        return PassiveBonus::operator<(other);
    const auto& other_casted = dynamic_cast<const BonusInitiative&>(other);
    return this->direction < other_casted.direction;
}
