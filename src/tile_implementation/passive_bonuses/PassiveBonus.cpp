#include <Board.h>
#include <PassiveBonus.h>
#include <TextGui.h>
#include <Tile.h>
#include <Tile_graphic_parameters.h>

#include <iostream>

using namespace std;

PassiveBonus::PassiveBonus(DIRECTION dir, bool affects_enemies)
    : direction{ dir }
    , affects_enemies{ affects_enemies }
    , was_target_changed{ false }
{}

void
PassiveBonus::switch_allegiance(Board& board, Coordinates bonus_coords)
{
    if (was_target_changed)
        this->reverse_bonus(board,
                            board.get_neighbour(bonus_coords, direction));
    affects_enemies = not affects_enemies;
    this->apply_bonus(board, board.get_neighbour(bonus_coords, direction));
}

void
PassiveBonus::set_direction(DIRECTION new_dir)
{
    direction = rotated_direction(direction, new_dir);
}

auto
PassiveBonus::is_target_valid(Board& board, Coordinates target_coords) -> bool
{
    if (not board.is_valid_position(target_coords))
        return false;
    if (not(bool) board.get_tile_ptr(target_coords))
        return false;
    bool is_enemy = is_target_enemy(board, target_coords);
    return is_enemy == this->affects_enemies;
}

auto
PassiveBonus::is_target_enemy(Board& board, Coordinates target_coords) -> bool
{
    Tile* tile_to_change = board.get_tile_ptr(target_coords);
    Player* target_owner = tile_to_change->get_owner();
    Coordinates bonus_owner_coords;
    bonus_owner_coords =
        board.get_neighbour(target_coords, opposite_direction(direction));
    Player* bonus_owner = board.get_tile_ptr(bonus_owner_coords)->get_owner();
    return target_owner != bonus_owner;
}

void
PassiveBonus::update_ascii_gui(const tile_parameters& p, TextGui* gui) const
{
    DIRECTION dir = this->direction;
    int x1;
    int y1;
    string ascii_symbol;
    switch (dir) {
        case N:
            x1 = p.tile_height + p.tile_width / 2;
            y1 = 2;
            ascii_symbol = "|";
            break;
        case S:
            x1 = p.tile_height + p.tile_width / 2;
            y1 = 2 * p.tile_height;
            ascii_symbol = "|";
            break;
        case NE:
            x1 = p.tile_width + 2 * p.tile_height - 3;
            y1 = p.tile_height - 1;
            ascii_symbol = "/";
            break;
        case SW:
            x1 = p.tile_height + 1;
            y1 = 2 * p.tile_height;
            ascii_symbol = "/";
            break;
        case NW:
            x1 = p.tile_height + 1;
            y1 = p.tile_height - 1;
            ascii_symbol = "\\";
            break;
        case SE:
            x1 = p.tile_width + p.tile_height + 1;
            y1 = 2 * p.tile_height;
            ascii_symbol = "\\";
            break;
        default:
            throw invalid_argument("Invalid direction specified");
    }
    gui->add_text_at_particular_hex(p, x1, y1, ascii_symbol);
}

auto
PassiveBonus::operator==(const PassiveBonus& other) const -> bool
{
    // check if actions are of the same class, e.g. Shoot and Shoot:
    if (typeid(*this) != typeid(other))
        return false;
    return true;
}

auto
PassiveBonus::operator<(const PassiveBonus& other) const -> bool
{
    return (typeid(*this).hash_code() < typeid(other).hash_code());
}

auto
PassiveBonus::operator!=(const PassiveBonus& other) const -> bool
{
    return !(*this == other);
}
