#include <Board.h>
#include <BonusAttack.h>
#include <Player.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <limits>

using namespace std;

void
BonusAttack::apply_bonus(Board& board, Coordinates target_coords)
{
    if (not is_target_valid(board, target_coords))
        return;
    Tile* tile_to_change = board.get_tile_ptr(target_coords);
    int min_initiative = numeric_limits<int>::max();
    const set<int>& tile_initiative = tile_to_change->get_initiative();
    min_initiative =
        *min_element(tile_initiative.begin(), tile_initiative.end());
    if (min_initiative == 0)
        return;
    set<int> new_initiative{ tile_initiative };
    new_initiative.insert(min_initiative - 1);
    tile_to_change->set_initiative(new_initiative);
    was_target_changed = true;
}

void
BonusAttack::reverse_bonus(Board& board, Coordinates target_coords)
{
    if (not was_target_changed)
        return;
    Tile* tile_to_unchange = board.get_tile_ptr(target_coords);
    const set<int>& tile_initiative = tile_to_unchange->get_initiative();
    int min_initiative =
        *min_element(tile_initiative.begin(), tile_initiative.end());
    set<int> new_initiative{ tile_initiative };
    new_initiative.erase(min_initiative);
    tile_to_unchange->set_initiative(new_initiative);
    was_target_changed = false;
}

auto
BonusAttack::get_string() const -> string
{
    return "**";
}

auto
BonusAttack::clone() const -> unique_ptr<PassiveBonus>
{
    return make_unique<BonusAttack>(*this);
}

auto
BonusAttack::operator==(const PassiveBonus& other) const -> bool
{
    if (!PassiveBonus::operator==(other))
        return false;
    const auto& other_casted = dynamic_cast<const BonusAttack&>(other);
    return (this->direction == other_casted.direction);
}

auto
BonusAttack::operator<(const PassiveBonus& other) const -> bool
{
    // check if actions are of the same class, e.g. BonusAttack and BonusAttack:
    if (typeid(*this) != typeid(other))
        return PassiveBonus::operator<(other);
    const auto& other_casted = dynamic_cast<const BonusAttack&>(other);
    return this->direction < other_casted.direction;
}
