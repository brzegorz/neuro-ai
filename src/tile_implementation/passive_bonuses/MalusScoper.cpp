#include <Board.h>
#include <MalusScoper.h>
#include <Player.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <limits>

using namespace std;

void
MalusScoper::apply_bonus(Board& board, Coordinates target_coords)
{
    if (not is_target_valid(board, target_coords))
        return;
    Tile* tile_to_change = board.get_tile_ptr(target_coords);
    tile_to_change->switch_passives_allegiance(board, target_coords);
    was_target_changed = true;
}

void
MalusScoper::reverse_bonus(Board& board, Coordinates target_coords)
{
    if (not was_target_changed or not is_target_valid(board, target_coords))
        return;
    Tile* tile_to_unchange = board.get_tile_ptr(target_coords);
    tile_to_unchange->switch_passives_allegiance(board, target_coords);
    was_target_changed = false;
}

auto
MalusScoper::get_string() const -> string
{
    return "SCOPER";
}

auto
MalusScoper::clone() const -> unique_ptr<PassiveBonus>
{
    return make_unique<MalusScoper>(*this);
}

auto
MalusScoper::operator==(const PassiveBonus& other) const -> bool
{
    if (!PassiveBonus::operator==(other))
        return false;
    const auto& other_casted = dynamic_cast<const MalusScoper&>(other);
    return (this->direction == other_casted.direction);
}

auto
MalusScoper::operator<(const PassiveBonus& other) const -> bool
{
    // check if actions are of the same class, e.g. MalusScoper and MalusScoper:
    if (typeid(*this) != typeid(other))
        return PassiveBonus::operator<(other);
    const auto& other_casted = dynamic_cast<const MalusScoper&>(other);
    return this->direction < other_casted.direction;
}
