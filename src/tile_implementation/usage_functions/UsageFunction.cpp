#include <UsageFunction.h>

using namespace std;

auto
UsageFunction::operator==(const UsageFunction& other) const -> bool
{
    if (typeid(*this) != typeid(other))
        return false;
    return true;
}

auto
UsageFunction::operator!=(const UsageFunction& other) const -> bool
{
    return !(*this == other);
}
