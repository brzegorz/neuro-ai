#include <Battle.h>
#include <BattleInstant.h>
#include <Directions.h>
#include <Game.h>
#include <Gui.h>
#include <PlayerLogic.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <memory>
#include <stdexcept>

using namespace std;

auto
BattleInstant::operator==(const UsageFunction& other) const -> bool
{
    if (!UsageFunction::operator==(other))
        return false;
    return true;
}

void
BattleInstant::change_board(Game* game)
{
    game->set_next_state(make_unique<Battle>());
}

auto
BattleInstant::is_usable(Game* game) const -> bool
{
    return not game->get_data().get_active_player()->get_resources()->get_army()->is_empty()
        and not game->get_data().get_previous_player()->get_resources()->get_army()->is_empty();
}

auto BattleInstant::get_valid_moves(Game * /*game*/) -> vector<PlayerDecision>
{
    return vector<PlayerDecision>{};
}

auto
BattleInstant::clone() const -> unique_ptr<UsageFunction>
{
    return make_unique<BattleInstant>(*this);
}
