#include <Army.h>
#include <Game.h>
#include <GameData.h>
#include <PlayerLogic.h>
#include <Sniper.h>
#include <Tile.h>

#include <algorithm>

using namespace std;

auto
Sniper::operator==(const UsageFunction& other) const -> bool
{
    if (!UsageFunction::operator==(other))
        return false;
    return true;
}

void
Sniper::change_board(Game* game)
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    Coordinates target_location;
    target_location = get_sniper_target(game);
    board->damage_tile_outside_battle(target_location, 1);
}

auto
Sniper::is_usable(Game* game) const -> bool
{
    auto data = game->get_data();
    return data.get_board()
               ->get_players_indexes(data.get_previous_player())
               .size() > 1;
}

auto
Sniper::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    unordered_set<Coordinates> valid_hexes;
    const Player* enemy = game->get_data().get_previous_player();
    for (Coordinates index :
         game->get_data().get_board()->get_players_indexes(enemy)) {
        if (not game->get_data()
                    .get_board()
                    ->get_tile_ptr(index)
                    ->is_headquarter) {
            valid_hexes.insert(index);
        }
    }
    vector<PlayerDecision> valid_decisions{};
    for (auto const& index : valid_hexes) {
        PlayerDecision valid_position{};
        valid_position.set_tile_target_location(index.x,
                                                index.y,
                                                N); // rotation irrelevant
        valid_decisions.push_back(valid_position);
    }
    return valid_decisions;
}

auto
Sniper::clone() const -> unique_ptr<UsageFunction>
{
    return make_unique<Sniper>(*this);
}

auto
Sniper::get_sniper_target(Game* game) -> Coordinates
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        unique_ptr<PlayerDecision> placement_decision;
        placement_decision =
            game_data.get_active_player()->get_logic()->pick_sniper_target(
                game);
        game_data.get_decision_manager()->add_player_decision(
            move(placement_decision));
    }
    unique_ptr<PlayerDecision> result =
        game_data.get_decision_manager()->get_player_decision();
    pair<Coordinates, DIRECTION> sniping_target;
    sniping_target = result->get_tile_target_location();
    return Coordinates(sniping_target.first.x, sniping_target.first.y);
}
