#include <BombInstant.h>
#include <Game.h>
#include <GameData.h>
#include <PlayerLogic.h>

using namespace std;

auto
BombInstant::operator==(const UsageFunction& other) const -> bool
{
    if (!UsageFunction::operator==(other))
        return false;
    return true;
}

void
BombInstant::change_board(Game* game)
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    Coordinates target_location;
    target_location = get_bomb_target(game);
    set<Coordinates> targets_coords = board->get_neighbours(target_location);
    if (game->get_data().gui)
        targets_coords.insert(target_location);
    for (const Coordinates& target_coords : targets_coords) {
        board->damage_tile_outside_battle(target_coords, 1);
    }
}

auto
BombInstant::is_usable(Game* /*game*/) const -> bool
{
    return true;
}

auto BombInstant::get_valid_moves(Game * /*game*/) -> vector<PlayerDecision>
{
    vector<PlayerDecision> valid_positions{};
    const unordered_set<Coordinates> valid_hexes{
        Coordinates(1, 3), Coordinates(1, 5), Coordinates(2, 2),
        Coordinates(2, 4), Coordinates(2, 6), Coordinates(3, 3),
        Coordinates(3, 5)
    };
    for (auto const& index : valid_hexes) {
        PlayerDecision valid_position{};
        valid_position.set_tile_target_location(index.x,
                                                index.y,
                                                N); // rotation irrelevant
        valid_positions.push_back(valid_position);
    }
    return valid_positions;
}

auto
BombInstant::clone() const -> unique_ptr<UsageFunction>
{
    return make_unique<BombInstant>(*this);
}

auto
BombInstant::get_bomb_target(Game* game) -> Coordinates
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        unique_ptr<PlayerDecision> placement_decision;
        placement_decision =
            game_data.get_active_player()->get_logic()->pick_bomb_target(game);
        game_data.get_decision_manager()->add_player_decision(
            move(placement_decision));
    }
    unique_ptr<PlayerDecision> result =
        game_data.get_decision_manager()->get_player_decision();
    pair<Coordinates, DIRECTION> bombing_target;
    bombing_target = result->get_tile_target_location();
    return Coordinates(bombing_target.first.x, bombing_target.first.y);
}
