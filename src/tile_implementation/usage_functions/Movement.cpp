#include <Directions.h>
#include <Game.h>
#include <Gui.h>
#include <Movement.h>
#include <PlayerLogic.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <memory>
#include <stdexcept>

using namespace std;

void
Movement::change_board(Game* game)
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    tuple<Coordinates, Coordinates, DIRECTION> source_and_target;
    source_and_target = get_source_and_target(game);
    unique_ptr<Tile> tile = board->pick_up_tile(get<0>(source_and_target));
    tile->rotate(get<2>(source_and_target));
    board->set_tile(get<1>(source_and_target), move(tile));
}

auto
Movement::get_source_and_target(Game* game) -> tuple<Coordinates,

                                                     Coordinates,

                                                     DIRECTION>
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        unique_ptr<PlayerDecision> movement_decision;
        movement_decision = game_data.get_active_player()
                                ->get_logic()
                                ->pick_movement_source_and_target(game);
        game_data.get_decision_manager()->add_player_decision(
            move(movement_decision));
    }
    unique_ptr<PlayerDecision> result =
        game_data.get_decision_manager()->get_player_decision();
    return result->get_movement_source_and_target();
}

auto
Movement::is_usable(Game* game) const -> bool
{
    GameData& game_data = game->get_data();
    const Player* active_player = game_data.get_active_player();
    const Board* board = game_data.get_board();
    for (Coordinates coords : board->get_players_indexes(active_player)) {
        const Tile* tile = board->get_tile_ptr(coords);
        if (not tile->is_in_net())
            return true;
    }
    return false;
}

auto
Movement::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    GameData& game_data = game->get_data();
    Player* active_player = game_data.get_active_player();
    Board* board = game_data.get_board();
    set<Coordinates> valid_sources{};
    for (const Coordinates& coords : board->get_players_indexes(active_player)) {
        if (not board->get_tile_ptr(coords)->is_in_net())
            valid_sources.insert(coords);
    }
    set<pair<Coordinates, Coordinates>> valid_source_target{};

    for (Coordinates source : valid_sources) {
        valid_source_target.insert(make_pair(source, source));
        for (Coordinates target : board->get_empty_neighbours(source)) {
            valid_source_target.insert(make_pair(source, target));
            const bool has_double_movement =
                board->get_tile_ptr(source)->get_owner()->has_double_movement();
            if (has_double_movement) {
                for (Coordinates far_target :
                     board->get_empty_neighbours(target))
                    valid_source_target.insert(make_pair(source, far_target));
            }
        }
    }

    vector<PlayerDecision> valid_moves{};
    for (auto [source, target] : valid_source_target) {
        for (DIRECTION rotation : all_directions) {
            PlayerDecision valid_decision{};
            valid_decision.set_movement_source_and_target(
                source, target, rotation);
            valid_moves.push_back(valid_decision);
        }
    }
    return valid_moves;
}

auto
Movement::clone() const -> unique_ptr<UsageFunction>
{
    return make_unique<Movement>(*this);
}

auto
Movement::operator==(const UsageFunction& other) const -> bool
{
    if (!UsageFunction::operator==(other))
        return false;
    return true;
}
