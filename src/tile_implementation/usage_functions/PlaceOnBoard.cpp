#include <Directions.h>
#include <Game.h>
#include <Gui.h>
#include <PlaceOnBoard.h>
#include <PlayerLogic.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <memory>
#include <stdexcept>

using namespace std;

void
PlaceOnBoard::change_board(Game* game)
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    pair<Coordinates, DIRECTION> target_location;
    target_location = get_target_location(game);
    unique_ptr<Tile> tile = move(game_data.tile_being_used);
    tile->rotate(target_location.second);
    board->set_tile(target_location.first, move(tile));
}

auto
PlaceOnBoard::get_target_location(Game* game) -> pair<Coordinates, DIRECTION>
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        unique_ptr<PlayerDecision> placement_decision;
        placement_decision = game_data.get_active_player()
                                 ->get_logic()
                                 ->pick_tile_target_location(game);
        game_data.get_decision_manager()->add_player_decision(
            move(placement_decision));
    }
    unique_ptr<PlayerDecision> result =
        game_data.get_decision_manager()->get_player_decision();
    return result->get_tile_target_location();
}

auto
PlaceOnBoard::is_usable(Game* /*game*/) const -> bool
{
    return true;
}

auto
PlaceOnBoard::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    vector<PlayerDecision> valid_positions{};
    const unordered_set<Coordinates>& empty_hexes =
        game->get_data().get_board()->get_empty_indexes();
    for (auto const& index : empty_hexes) {
        for (DIRECTION rotation : all_directions) {
            PlayerDecision valid_position{};
            valid_position.set_tile_target_location(index.x, index.y, rotation);
            valid_positions.push_back(valid_position);
        }
    }
    return valid_positions;
}

auto
PlaceOnBoard::clone() const -> unique_ptr<UsageFunction>
{
    return make_unique<PlaceOnBoard>(*this);
}

auto
PlaceOnBoard::operator==(const UsageFunction& other) const -> bool
{
    if (!UsageFunction::operator==(other))
        return false;
    return true;
}
