#include <Directions.h>
#include <ExecutePushBack.h>
#include <Game.h>
#include <Gui.h>
#include <PlayerLogic.h>
#include <PushBack.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <memory>
#include <stdexcept>

using namespace std;

void
PushBack::change_board(Game* game)
{
    pair<Coordinates, DIRECTION> push_params = get_push_parameters(game);
    game->set_next_state(make_unique<ExecutePushBack>(push_params));
}

auto
PushBack::is_usable(Game* game) const -> bool
{
    GameData& game_data = game->get_data();
    const Board* board = game_data.get_board();
    const Player* active_player = game_data.get_active_player();
    const Player* enemy = game_data.get_previous_player();
    for (const Coordinates& pusher :
         board->get_players_indexes(active_player)) {
        if (can_tile_push(board, active_player, enemy, pusher)) {
            return true;
        }
    }
    return false;
}

auto
PushBack::can_tile_push(const Board* board,
                        const Player* active_player,
                        const Player* enemy,
                        Coordinates pusher_coords) -> bool
{
    for (DIRECTION push_dir : all_directions) {
        const bool push_possible = can_tile_be_pushed(
            board, active_player, enemy, pusher_coords, push_dir);
        if (push_possible) {
            return true;
        }
    }
    return false;
}

auto
PushBack::can_tile_be_pushed(const Board* board,
                             const Player* /*active_player*/,
                             const Player* enemy,
                             Coordinates pusher_coords,
                             DIRECTION push_dir) -> bool
{
    Coordinates pushed_coords = board->get_neighbour(pusher_coords, push_dir);
    if (not board->is_valid_position(pushed_coords))
        return false;
    const Tile* pushed = board->get_tile_ptr(pushed_coords);
    if (not(pushed and *(pushed->get_owner()) == *enemy))
        return false;
    for (DIRECTION rotation : set<DIRECTION>{ N, NE, NW }) {
        DIRECTION pushed_enemy_dir = rotated_direction(push_dir, rotation);
        Coordinates target_coords =
            board->get_neighbour(pushed_coords, pushed_enemy_dir);
        if (board->is_valid_position(target_coords) and
            not board->get_tile_ptr(target_coords)) {
            return true;
        }
    }
    return false;
}

auto
PushBack::operator==(const UsageFunction& other) const -> bool
{
    if (!UsageFunction::operator==(other))
        return false;
    return true;
}

auto
PushBack::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    GameData& game_data = game->get_data();
    const Board* board = game_data.get_board();
    const Player* active_player = game_data.get_active_player();
    const Player* enemy = game_data.get_previous_player();
    vector<PlayerDecision> valid_moves{};
    for (const Coordinates& pusher :
         board->get_players_indexes(active_player)) {
        if (can_tile_push(board, active_player, enemy, pusher)) {
            for (DIRECTION push_dir : all_directions) {
                const bool push_possible = can_tile_be_pushed(
                    board, active_player, enemy, pusher, push_dir);
                if (push_possible) {
                    PlayerDecision valid_move{};
                    valid_move.set_push_source_and_direction(pusher, push_dir);
                    valid_moves.push_back(valid_move);
                }
            }
        }
    }
    return valid_moves;
}

auto
PushBack::get_push_parameters(Game* game) -> pair<Coordinates, DIRECTION>
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        unique_ptr<PlayerDecision> placement_decision;
        placement_decision = game_data.get_active_player()
                                 ->get_logic()
                                 ->pick_push_source_and_direction(game);
        game_data.get_decision_manager()->add_player_decision(
            move(placement_decision));
    }
    unique_ptr<PlayerDecision> result =
        game_data.get_decision_manager()->get_player_decision();
    pair<Coordinates, DIRECTION> push_target;
    push_target = result->get_push_source_and_direction();
    return push_target;
}

auto
PushBack::clone() const -> unique_ptr<UsageFunction>
{
    return make_unique<PushBack>(*this);
}
