#include <Board.h>
#include <Player.h>
#include <Shoot.h>
#include <TextGui.h>
#include <Tile.h>
#include <Tile_graphic_parameters.h>

#include <iostream>

using namespace std;

void
Shoot::change_board(Board& board, Coordinates tile_coords)
{
    Tile* tile_to_hit = get_target(board, tile_coords);
    if (not tile_to_hit)
        return;
    Tile* shooter = board.get_tile_ptr(tile_coords);
    int damage = this->strength;
    if (tile_to_hit->is_armoured(opposite_direction(direction)))
        damage = damage - 1;
    if (damage != 0)
        tile_to_hit->receive_damage_in_battle(damage, shooter);
}

auto
Shoot::get_target(Board& board, Coordinates tile_coords) -> Tile*
{
    bool is_target_valid;
    bool is_target_occupied;
    bool is_target_enemy = false;
    Coordinates hex_to_hit = tile_coords;
    Tile* tile_to_hit = nullptr;
    string shooter_owner =
        board.get_tile_ptr(tile_coords)->get_owner()->get_name();
    string target_owner;
    do { // search through shooting line for a valid target
        hex_to_hit = board.get_neighbour(hex_to_hit, this->direction);
        is_target_valid = board.is_valid_position(hex_to_hit);
        if (is_target_valid) {
            tile_to_hit = board.get_tile_ptr(hex_to_hit);
            is_target_occupied = (bool)tile_to_hit;
            if (is_target_occupied) {
                target_owner = tile_to_hit->get_owner()->get_name();
                is_target_enemy = target_owner != shooter_owner;
            }
        }
    } while (is_target_valid and not(is_target_occupied and is_target_enemy));
    if (not is_target_valid or not is_target_enemy)
        return nullptr;
    return tile_to_hit;
}

void
Shoot::modify_shoot_strength(int change)
{
    strength = strength + change;
}

void
Shoot::update_ascii_gui(const tile_parameters& p, TextGui* gui) const
{
    DIRECTION dir = this->direction;
    int x1;
    int x2;
    int y1;
    int y2;
    string ascii_symbol;
    switch (dir) {
        case N:
            x1 = p.tile_height + p.tile_width / 2 + 2;
            y1 = 2;
            x2 = x1;
            y2 = y1 + 1;
            ascii_symbol = "|";
            break;
        case S:
            x1 = p.tile_height + p.tile_width / 2 + 2;
            y1 = 2 * p.tile_height;
            x2 = x1;
            y2 = y1 - 1;
            ascii_symbol = "|";
            break;
        case NE:
            x1 = p.tile_width + 2 * p.tile_height - 2;
            y1 = p.tile_height;
            x2 = x1 - 1;
            y2 = y1 + 1;
            ascii_symbol = "/";
            break;
        case SW:
            x1 = p.tile_height;
            y1 = 2 * p.tile_height - 1;
            x2 = x1 + 1;
            y2 = y1 - 1;
            ascii_symbol = "/";
            break;
        case NW:
            x1 = p.tile_height;
            y1 = p.tile_height;
            x2 = x1 + 1;
            y2 = y1 + 1;
            ascii_symbol = "\\";
            break;
        case SE:
            x1 = p.tile_width + p.tile_height + 2;
            y1 = 2 * p.tile_height - 1;
            x2 = x1 - 1;
            y2 = y1 - 1;
            ascii_symbol = "\\";
            break;
        default:
            throw invalid_argument("Invalid direction specified");
    }
    if (strength != 1)
        gui->add_text_at_particular_hex(p, x1 - 1, y1, to_string(strength));
    gui->add_text_at_particular_hex(p, x1, y1, ascii_symbol);
    gui->add_text_at_particular_hex(p, x2, y2, ascii_symbol);
}

auto
Shoot::clone() const -> unique_ptr<BattleAction>
{
    return make_unique<Shoot>(*this);
}

auto
Shoot::operator==(const BattleAction& other) const -> bool
{
    if (!BattleAction::operator==(other))
        return false;
    const auto& other_casted = dynamic_cast<const Shoot&>(other);
    return (this->get_direction() == other_casted.get_direction() and
            this->get_strength() == other_casted.get_strength());
}

auto
Shoot::operator<(const BattleAction& other) const -> bool
{
    // check if actions are of the same class, e.g. Shoot and Shoot:
    if (typeid(*this) != typeid(other))
        return BattleAction::operator<(other);
    const auto& other_casted = dynamic_cast<const Shoot&>(other);
    if (this->direction != other_casted.direction)
        return this->direction < other_casted.direction;
    return this->strength < other_casted.strength;
}

void
Shoot::print_action() const
{
    cout << "Shoot Direction " << direction << " Strength " << strength << endl;
}
