#include <BattleAction.h>

#include <iostream>

using namespace std;

void
BattleAction::set_direction(DIRECTION new_dir)
{
    direction = rotated_direction(direction, new_dir);
}

void
BattleAction::modify_shoot_strength(int change)
{}

void
BattleAction::modify_hit_strength(int change)
{}

auto
BattleAction::operator==(const BattleAction& other) const -> bool
{
    // check if actions are of the same class, e.g. Shoot and Shoot:
    if (typeid(*this) != typeid(other))
        return false;
    return true;
}

auto
BattleAction::operator<(const BattleAction& other) const -> bool
{
    return (typeid(*this).hash_code() < typeid(other).hash_code());
}

auto
BattleAction::operator!=(const BattleAction& other) const -> bool
{
    return !(*this == other);
}
