#include <Board.h>
#include <Hit.h>
#include <Player.h>
#include <TextGui.h>
#include <Tile.h>
#include <Tile_graphic_parameters.h>

#include <iostream>

using namespace std;

void
Hit::change_board(Board& board, Coordinates tile_coords)
{
    if (not is_target_valid(board, tile_coords))
        return;
    Coordinates hex_to_hit;
    hex_to_hit = board.get_neighbour(tile_coords, this->direction);
    Tile* tile_to_hit = board.get_tile_ptr(hex_to_hit);
    Tile* hitter = board.get_tile_ptr(tile_coords);
    tile_to_hit->receive_damage_in_battle(this->strength, hitter);
}

auto
Hit::is_target_valid(Board& board, Coordinates tile_coords) -> bool
{
    bool is_target_valid;
    bool is_target_occupied;
    bool is_target_enemy = false;
    Tile* tile_to_hit;
    Player* hitter_owner = board.get_tile_ptr(tile_coords)->get_owner();
    Player* target_owner;
    Coordinates hex_to_hit = board.get_neighbour(tile_coords, this->direction);
    is_target_valid = board.is_valid_position(hex_to_hit);
    if (is_target_valid) {
        tile_to_hit = board.get_tile_ptr(hex_to_hit);
        is_target_occupied = (bool)tile_to_hit;
        if (is_target_occupied) {
            target_owner = tile_to_hit->get_owner();
            is_target_enemy = target_owner != hitter_owner;
        }
    }
    if (not is_target_valid or not is_target_enemy)
        return false;
    return true;
}

void
Hit::modify_hit_strength(int change)
{
    strength = strength + change;
}

void
Hit::update_ascii_gui(const tile_parameters& p, TextGui* gui) const
{
    DIRECTION dir = this->direction;
    int x1;
    int y1;
    string ascii_symbol;
    switch (dir) {
        case N:
            x1 = p.tile_height + p.tile_width / 2;
            y1 = 2;
            ascii_symbol = "|";
            break;
        case S:
            x1 = p.tile_height + p.tile_width / 2;
            y1 = 2 * p.tile_height;
            ascii_symbol = "|";
            break;
        case NE:
            x1 = p.tile_width + 2 * p.tile_height - 3;
            y1 = p.tile_height - 1;
            ascii_symbol = "/";
            break;
        case SW:
            x1 = p.tile_height + 1;
            y1 = 2 * p.tile_height;
            ascii_symbol = "/";
            break;
        case NW:
            x1 = p.tile_height + 1;
            y1 = p.tile_height - 1;
            ascii_symbol = "\\";
            break;
        case SE:
            x1 = p.tile_width + p.tile_height + 1;
            y1 = 2 * p.tile_height;
            ascii_symbol = "\\";
            break;
        default:
            throw invalid_argument("Invalid direction specified");
    }
    if (strength != 1)
        gui->add_text_at_particular_hex(p, x1 - 1, y1, to_string(strength));
    gui->add_text_at_particular_hex(p, x1, y1, ascii_symbol);
}

auto
Hit::clone() const -> unique_ptr<BattleAction>
{
    return make_unique<Hit>(*this);
}

auto
Hit::operator==(const BattleAction& other) const -> bool
{
    if (!BattleAction::operator==(other))
        return false;
    const Hit& other_casted = dynamic_cast<const Hit&>(other);
    return (this->get_direction() == other_casted.get_direction() and
            this->get_strength() == other_casted.get_strength());
}

auto
Hit::operator<(const BattleAction& other) const -> bool
{
    // check if actions are of the same class, e.g. Hit and Hit:
    if (typeid(*this) != typeid(other))
        return BattleAction::operator<(other);
    const Hit& other_casted = dynamic_cast<const Hit&>(other);
    if (this->direction != other_casted.direction)
        return this->direction < other_casted.direction;
    return this->strength < other_casted.strength;
}

void
Hit::print_action() const
{
    cout << "Hit Direction " << direction << " Strength " << strength << endl;
}
