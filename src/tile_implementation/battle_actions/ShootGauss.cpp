#include <Board.h>
#include <Player.h>
#include <ShootGauss.h>
#include <TextGui.h>
#include <Tile.h>
#include <Tile_graphic_parameters.h>

#include <iostream>

using namespace std;

void
ShootGauss::change_board(Board& board, Coordinates tile_coords)
{
    bool is_target_occupied;
    bool is_target_enemy;
    Coordinates hex_to_hit = tile_coords;
    Tile* tile_to_hit;
    Tile* gauss_tile = board.get_tile_ptr(tile_coords);
    string ShootGausser_owner =
        board.get_tile_ptr(tile_coords)->get_owner()->get_name();
    string target_owner;
    do { // damage each target in a line
        hex_to_hit = board.get_neighbour(hex_to_hit, this->direction);
        if (not board.is_valid_position(hex_to_hit))
            return;
        tile_to_hit = board.get_tile_ptr(hex_to_hit);
        is_target_occupied = (bool)tile_to_hit;
        if (is_target_occupied) {
            target_owner = tile_to_hit->get_owner()->get_name();
            is_target_enemy = target_owner != ShootGausser_owner;
            if (is_target_enemy)
                tile_to_hit->receive_damage_in_battle(strength, gauss_tile);
        }
    } while (true);
}

void
ShootGauss::update_ascii_gui(const tile_parameters& p, TextGui* gui) const
{
    DIRECTION dir = this->direction;
    std::array<int, 4> x{};
    std::array<int, 4> y{};
    string ascii_symbol;
    switch (dir) {
        case N:
            x[0] = p.tile_height + p.tile_width / 2 + 2;
            y[0] = 2;
            x[1] = x[0];
            y[1] = y[0] + 1;
            x[2] = x[0] + 1;
            y[2] = y[0];
            x[3] = x[0] + 1;
            y[3] = y[0] + 1;
            ascii_symbol = "|";
            break;
        case S:
            x[0] = p.tile_height + p.tile_width / 2 + 2;
            y[0] = 2 * p.tile_height;
            x[1] = x[0];
            y[1] = y[0] - 1;
            x[2] = x[0] + 1;
            y[2] = y[0];
            x[3] = x[0] + 1;
            y[3] = y[0] - 1;
            ascii_symbol = "|";
            break;
        case NE:
            x[0] = p.tile_width + 2 * p.tile_height - 2;
            y[0] = p.tile_height;
            x[1] = x[0] - 1;
            y[1] = y[0] + 1;
            x[2] = x[0] - 1;
            y[2] = y[0] - 1;
            x[3] = x[1] - 1;
            y[3] = y[1] - 1;
            ascii_symbol = "/";
            break;
        case SW:
            x[0] = p.tile_height;
            y[0] = 2 * p.tile_height - 1;
            x[1] = x[0] + 1;
            y[1] = y[0] - 1;
            x[2] = x[0] + 1;
            y[2] = y[0] + 1;
            x[3] = x[1] + 1;
            y[3] = y[1] + 1;
            ascii_symbol = "/";
            break;
        case NW:
            x[0] = p.tile_height;
            y[0] = p.tile_height;
            x[1] = x[0] + 1;
            y[1] = y[0] + 1;
            x[2] = x[0] + 1;
            y[2] = y[0] - 1;
            x[3] = x[1] + 1;
            y[3] = y[1] - 1;
            ascii_symbol = "\\";
            break;
        case SE:
            x[0] = p.tile_width + p.tile_height + 2;
            y[0] = 2 * p.tile_height - 1;
            x[1] = x[0] - 1;
            y[1] = y[0] - 1;
            x[2] = x[0] - 1;
            y[2] = y[0] + 1;
            x[3] = x[1] - 1;
            y[3] = y[1] + 1;
            ascii_symbol = "\\";
            break;
        default:
            throw invalid_argument("Invalid direction specified");
    }
    if (strength != 1)
        gui->add_text_at_particular_hex(p, x[0] - 1, y[0], to_string(strength));
    for (int i = 0; i < 4; i++)
        gui->add_text_at_particular_hex(p, x[i], y[i], ascii_symbol);
}

auto
ShootGauss::clone() const -> unique_ptr<BattleAction>
{
    return make_unique<ShootGauss>(*this);
}

auto
ShootGauss::operator==(const BattleAction& other) const -> bool
{
    if (!BattleAction::operator==(other))
        return false;
    const auto& other_casted = dynamic_cast<const ShootGauss&>(other);
    return (this->get_direction() == other_casted.get_direction() and
            this->get_strength() == other_casted.get_strength());
}

auto
ShootGauss::operator<(const BattleAction& other) const -> bool
{
    // check if actions are of the same class, e.g. ShootGauss and ShootGauss:
    if (typeid(*this) != typeid(other))
        return BattleAction::operator<(other);
    const auto& other_casted = dynamic_cast<const ShootGauss&>(other);
    if (this->direction != other_casted.direction)
        return this->direction < other_casted.direction;
    return this->strength < other_casted.strength;
}

void
ShootGauss::print_action() const
{
    cout << "ShootGauss Direction " << direction << " Strength " << strength
         << endl;
}
