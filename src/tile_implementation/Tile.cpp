#include <BattleAction.h>
#include <Directions.h>
#include <Game.h>
#include <GameData.h>
#include <InnateFunction.h>
#include <PlaceOnBoard.h>
#include <Player.h>
#include <Tile.h>

#include <algorithm>
#include <memory>
#include <set>
#include <utility>

using namespace std;

// Base constructor
Tile::Tile(bool is_headquarter,
           string name,
           Player* owner,
           int hp,
           set<int> initiative,
           unique_ptr<set<unique_ptr<BattleAction>>> battle_actions,
           unordered_set<DIRECTION> armour_directions)
    : is_headquarter{ is_headquarter }
    , is_name_visible{ false }
    , name{ move(name) }
    , owner{ owner }
    , hp{ hp }
    , damage_received{}
    , direction{ N }
    , initiative{ move(initiative) }
    , battle_actions{}
    , passive_bonuses{}
    , usage{ make_unique<PlaceOnBoard>() }
    , innate{ nullptr }
    , directions_of_armour{ move(armour_directions) }
    , directions_of_medic{}
    , heals_enemy { false }
    , count_of_nets_on_unit{ 0 }
{
    while (not battle_actions->empty()) {
        auto action_node = battle_actions->extract(begin(*battle_actions));
        this->battle_actions.insert(move(action_node.value()));
    }
}

// Module constructor
Tile::Tile(bool is_headquarter,
           string name,
           Player* owner,
           int hp,
           unique_ptr<set<unique_ptr<PassiveBonus>>> passive_bonuses)
    : Tile(is_headquarter, name, owner, hp)
{
    while (not passive_bonuses->empty()) {
        auto bonus_node = passive_bonuses->extract(begin(*passive_bonuses));
        this->passive_bonuses.insert(move(bonus_node.value()));
    }
}

// Mainly for HQ's
Tile::Tile(bool is_headquarter,
           string name,
           Player* owner,
           int hp,
           set<int> initiative,
           unique_ptr<set<unique_ptr<BattleAction>>> battle_actions,
           unique_ptr<set<unique_ptr<PassiveBonus>>> passive_bonuses)
    : Tile(is_headquarter,
           move(name),
           owner,
           hp,
           move(initiative),
           move(battle_actions))
{
    while (not passive_bonuses->empty()) {
        auto bonus_node = passive_bonuses->extract(begin(*passive_bonuses));
        this->passive_bonuses.insert(move(bonus_node.value()));
    }
}
// Instant tile constructor
Tile::Tile(string name, Player* owner, unique_ptr<UsageFunction> usage_func)
    : is_headquarter{ false }
    , is_name_visible{ true }
    , name{ move(name) }
    , owner{ owner }
    , hp{ -1 }
    , direction{ N }
    , initiative{}
    , battle_actions{}
    , passive_bonuses{}
    , usage{ usage_func->clone() }
    , innate{ nullptr }
    , directions_of_armour{}
    , count_of_nets_on_unit{ 0 }
{}

void
Tile::set_innate_function(unique_ptr<InnateFunction> innate_func)
{
    this->innate = move(innate_func);
}

Tile::Tile(const Tile& other)
    : is_headquarter{ other.is_headquarter }
    , is_name_visible{ other.is_name_visible }
    , name{ other.name }
    , owner{ other.owner }
    , hp{ other.hp }
    , damage_received{ other.damage_received }
    , direction{ other.direction }
    , initiative{ other.get_initiative() }
    , battle_actions{}
    , passive_bonuses{}
    , usage{ other.usage->clone() }
    , directions_of_armour{ other.directions_of_armour }
    , directions_of_medic{ other.directions_of_medic }
    , heals_enemy { other.heals_enemy }
    , count_of_nets_on_unit{ other.count_of_nets_on_unit }
{
    for (auto const& battle_action : other.battle_actions) {
        battle_actions.insert(battle_action->clone());
    }
    for (auto const& passive_bonus : other.passive_bonuses) {
        passive_bonuses.insert(passive_bonus->clone());
    }
    if (other.innate)
        innate = other.innate->clone();
}

void
Tile::use(Game* game) const
{
    usage->change_board(game);
}

void
Tile::use_innate(Game* game)
{
    innate->change_board(game);
}

void
Tile::do_battle_actions(Board& board, Coordinates tile_coords) const
{
    if (is_in_net())
        return;
    for (auto const& act_ptr : battle_actions) {
        act_ptr->change_board(board, tile_coords);
    }
}

void
Tile::apply_passive_bonuses(Board& board, Coordinates source_coords) const
{
    if (is_in_net())
        return;
    Coordinates target_coords;
    DIRECTION bonus_direction;
    for (auto const& bonus_ptr : passive_bonuses) {
        bonus_direction = bonus_ptr->get_direction();
        target_coords = board.get_neighbour(source_coords, bonus_direction);
        if (board.is_valid_position(target_coords)) {
            bonus_ptr->apply_bonus(board, target_coords);
            board.add_passive_bonus_source(target_coords,
                                           opposite_direction(bonus_direction));
        }
    }
    for (auto const& medic_direction : directions_of_medic) {
        target_coords = board.get_neighbour(source_coords, medic_direction);
        if (board.is_valid_position(target_coords))
            board.add_medic_source(target_coords,
                                   opposite_direction(medic_direction));
    }
}

void
Tile::reverse_passive_bonuses(Board& board, Coordinates source_coords) const
{
    Coordinates target_coords;
    DIRECTION bonus_direction;
    for (auto const& bonus_ptr : passive_bonuses) {
        bonus_direction = bonus_ptr->get_direction();
        target_coords = board.get_neighbour(source_coords, bonus_direction);
        bonus_ptr->reverse_bonus(board, target_coords);
        if (board.is_valid_position(target_coords)) {
            board.remove_passive_bonus_source(
                target_coords, opposite_direction(bonus_direction));
        }
    }
    for (DIRECTION medic_direction : directions_of_medic) {
        target_coords = board.get_neighbour(source_coords, medic_direction);
        if (board.is_valid_position(target_coords))
            board.remove_medic_source(target_coords,
                                      opposite_direction(medic_direction));
    }
}

void
Tile::apply_passive_bonuses(Board& board,
                            Coordinates source_coords,
                            DIRECTION target_direction) const
{
    if (is_in_net())
        return;
    auto target_coords = board.get_neighbour(source_coords, target_direction);
    DIRECTION bonus_direction;
    for (auto const& bonus_ptr : passive_bonuses) {
        bonus_direction = bonus_ptr->get_direction();
        if (bonus_direction == target_direction)
            if (board.is_valid_position(target_coords))
                bonus_ptr->apply_bonus(board, target_coords);
    }
}

void
Tile::add_medic_bonus(Board& board,
                      Coordinates source_coords,
                      DIRECTION target_direction) const
{
    if (is_in_net())
        return;
    auto target_coords = board.get_neighbour(source_coords, target_direction);
    if (directions_of_medic.find(target_direction) != directions_of_medic.end())
        if (board.is_valid_position(target_coords))
            board.add_medic_source(target_coords,
                                   opposite_direction(target_direction));
}

void
Tile::reverse_passive_bonuses(Board& board,
                              Coordinates source_coords,
                              DIRECTION target_direction) const
{
    Coordinates target_coords;
    DIRECTION bonus_direction;
    for (auto const& bonus_ptr : passive_bonuses) {
        bonus_direction = bonus_ptr->get_direction();
        if (bonus_direction == target_direction) {
            target_coords = board.get_neighbour(source_coords, bonus_direction);
            if (board.is_valid_position(target_coords))
                bonus_ptr->reverse_bonus(board, target_coords);
        }
    }
}

void
Tile::remove_medic_bonus(Board& board,
                         Coordinates source_coords,
                         DIRECTION target_direction) const
{
    auto target_coords = board.get_neighbour(source_coords, target_direction);
    if (directions_of_medic.find(target_direction) != directions_of_medic.end())
        if (board.is_valid_position(target_coords))
            board.add_medic_source(target_coords,
                                   opposite_direction(target_direction));
}

auto
Tile::is_armoured(DIRECTION where) const -> bool
{
    auto const tile_is_armoured = directions_of_armour.find(where);
    return tile_is_armoured != directions_of_armour.end();
}

auto
Tile::is_medic(DIRECTION where) const -> bool
{
    auto const tile_is_medic = directions_of_medic.find(where);
    return tile_is_medic != directions_of_medic.end();
}

auto
Tile::is_in_net() const -> bool
{
    return count_of_nets_on_unit > 0;
}

auto
Tile::is_usable(Game* game) const -> bool
{
    return usage->is_usable(game);
}

auto
Tile::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    return usage->get_valid_moves(game);
}

void
Tile::rotate(const DIRECTION& new_direction)
{
    this->direction = new_direction;

    auto actions_it = battle_actions.begin();
    auto actions_it_end = battle_actions.end();
    set<unique_ptr<BattleAction>> tmp{};
    while (*actions_it != *actions_it_end) {
        (*actions_it)->set_direction(new_direction);
        tmp.insert(move(battle_actions.extract(actions_it).value()));
        actions_it = battle_actions.begin();
    }
    auto tmp_it = tmp.begin();
    auto tmp_end = tmp.end();
    while (tmp_it != tmp_end) {
        battle_actions.insert(move(tmp.extract(tmp_it).value()));
        tmp_it = tmp.begin();
    }

    auto bonuses_it = passive_bonuses.begin();
    auto bonuses_it_end = passive_bonuses.end();
    set<unique_ptr<PassiveBonus>> tmp2{};
    while (*bonuses_it != *bonuses_it_end) {
        (*bonuses_it)->set_direction(new_direction);
        tmp2.insert(move(passive_bonuses.extract(bonuses_it).value()));
        bonuses_it = passive_bonuses.begin();
    }
    auto tmp2_it = tmp2.begin();
    auto tmp2_end = tmp2.end();
    while (tmp2_it != tmp2_end) {
        passive_bonuses.insert(move(tmp2.extract(tmp2_it).value()));
        tmp2_it = tmp2.begin();
    }

    unordered_set<DIRECTION> new_armour_directions{};
    for (DIRECTION armour_dir : directions_of_armour) {
        new_armour_directions.insert(
            rotated_direction(armour_dir, new_direction));
    }
    directions_of_armour = new_armour_directions;

    unordered_set<DIRECTION> new_medic_directions{};
    for (DIRECTION medic_dir : directions_of_medic) {
        new_medic_directions.insert(
            rotated_direction(medic_dir, new_direction));
    }
    directions_of_medic = new_medic_directions;
}

void
Tile::modify_shoot_strength(int change)
{
    for (auto const& act_ptr : battle_actions) {
        act_ptr->modify_shoot_strength(change);
    }
}

void
Tile::modify_hit_strength(int change)
{
    for (auto const& act_ptr : battle_actions) {
        act_ptr->modify_hit_strength(change);
    }
}

void
Tile::receive_damage_in_battle(int damage, Tile* damage_dealer)
{
    if (is_headquarter and not damage_dealer)
        return;
    if (is_headquarter and damage_dealer->is_headquarter)
        return;
    damage_received.push_back(damage);
}

void
Tile::resolve_damage()
{
    for (int dmg : damage_received)
        hp = hp - dmg;
    damage_received = vector<int>();
}

void
Tile::add_medic_direction(DIRECTION dir)
{
    directions_of_medic.insert(dir);
}

void
Tile::switch_passives_allegiance(Board& board, Coordinates target_coords)
{
    for (auto const& passive_bonus : passive_bonuses) {
        passive_bonus->switch_allegiance(board, target_coords);
    }
    bool heals_enemy = board.get_tile_ptr(target_coords) -> heals_enemy;
    board.get_tile_ptr(target_coords) -> heals_enemy = not heals_enemy;
}

void
Tile::net_unit(Board& board, Coordinates unit_coords)
{
    ++count_of_nets_on_unit;
    if (count_of_nets_on_unit > 1)
        return;
    reverse_passive_bonuses(board, unit_coords);
}

void
Tile::unnet_unit(Board& board, Coordinates unit_coords)
{
    unnet_when_mutual_netting();
    if (count_of_nets_on_unit > 0)
        return;
    apply_passive_bonuses(board, unit_coords);
}

void
Tile::unnet_when_mutual_netting()
{
    --count_of_nets_on_unit;
    if (count_of_nets_on_unit < 0)
        throw logic_error(get_name() + " unnetted too many times");
}

void
Tile::get_healed(int index_of_attack_to_heal)
{
    damage_received.erase(damage_received.begin() + index_of_attack_to_heal);
}

void
Tile::update_ascii_gui(const tile_parameters& p, TextGui* gui) const
{
    for (auto const& act_ptr : battle_actions) {
        act_ptr->update_ascii_gui(p, gui);
    }

    for (auto const& bonus_ptr : passive_bonuses) {
        bonus_ptr->update_ascii_gui(p, gui);
    }
}

auto
Tile::get_passives_string() const -> string
{
    set<string> passive_strings{};
    for (auto const& passive : passive_bonuses)
        passive_strings.insert(passive->get_string());
    string result = "";
    for (const string& passive_string : passive_strings)
        result += passive_string;
    return result;
}

auto
Tile::get_innate_function_string() const -> string
{
    if (innate)
        return innate->get_ascii_description();
    else
        return "";
}

auto
Tile::operator==(const Tile& other) const -> bool
{
    if (this->is_headquarter != other.is_headquarter)
        return false;
    if (this->hp != other.hp)
        return false;
    if (this->direction != other.direction)
        return false;
    if (this->name != other.name)
        return false;
    if (*(this->usage) != *other.usage)
        return false;
    if (this->initiative != other.initiative)
        return false;
    if (*this->owner != *other.owner)
        return false;
    if (not compare_battle_actions(other))
        return false;
    return compare_passive_bonuses(other);
}

auto
Tile::operator!=(const Tile& other) const -> bool
{
    return !(*this == other);
}

auto
Tile::compare_battle_actions(const Tile& other) const -> bool
{
    if (this->battle_actions.size() != other.battle_actions.size())
        return false;

    auto this_actions_it = this->battle_actions.begin();
    auto this_actions_it_end = this->battle_actions.end();
    auto other_actions_it = other.battle_actions.begin();
    while (this_actions_it != this_actions_it_end) {
        if (**this_actions_it != **other_actions_it) {
            return false;
        }
        ++this_actions_it;
        ++other_actions_it;
    }
    return true;
}

auto
Tile::compare_passive_bonuses(const Tile& other) const -> bool
{
    if (this->passive_bonuses.size() != other.passive_bonuses.size())
        return false;
    auto this_bonuses_it = this->passive_bonuses.begin();
    auto this_bonuses_it_end = this->passive_bonuses.end();
    auto other_bonuses_it = other.passive_bonuses.begin();
    while (this_bonuses_it != this_bonuses_it_end) {
        if (**this_bonuses_it != **other_bonuses_it) {
            return false;
        }
        ++this_bonuses_it;
        ++other_bonuses_it;
    }
    return true;
}
