#include <Army.h>
#include <Board.h>
#include <Colours.h>
#include <GameData.h>
#include <Gui.h>
#include <MolochBuilder.h>
#include <OutpostBuilder.h>
#include <Player.h>
#include <PlayerLogic.h>
#include <PlayerResources.h>
#include <Rng.h>
#include <TestArmyBuilder.h>
#include <TextGui.h>
#include <Tile.h>
#include <Tile_usage_way.h>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <memory>
#include <random>
#include <stdexcept>
#include <vector>

using namespace std;

int GameData::games_created_count = 0;

GameData::GameData()
    : is_one_player_out_of_tiles{ false }
    , is_final_turn_in_place{ false }
    , is_final_battle_over{ false }
    , is_game_finished{ false }
    , current_battle_turn{ -1 }
    , tile_being_used{ nullptr }
    , tile_using_innate_coordinates{ -1, -1 }
    , current_medic_coordinates{ -1, -1 }
    , current_target_valid_medics{}
    , current_medic_valid_targets{}
    , tile_to_heal_coordinates{ -1, -1 }
    , board{}
    , players{ make_unique<Player>("player A", COLOURS.RED),
               make_unique<Player>("player B", COLOURS.GREEN) }
    , active_player_index{ 0 }
    , way_of_using_the_tile{}
    , decisions_left_count{ 0 }
    , turn_count{ 0 }
    , hp_of_headquarters{}
    , winner{ nullptr }
{
    ++games_created_count;
}

GameData::GameData(const GameData& other)
    // : gui{make_unique<TextGui>()}
    : is_one_player_out_of_tiles{ other.is_one_player_out_of_tiles }
    , is_final_turn_in_place{ other.is_final_turn_in_place }
    , is_final_battle_over{ other.is_final_battle_over }
    , is_game_finished{ other.is_game_finished }
    , current_battle_turn{ other.current_battle_turn }
    , tile_using_innate_coordinates{ other.tile_using_innate_coordinates }
    , current_medic_coordinates{ other.current_medic_coordinates }
    , current_target_valid_medics{ other.current_target_valid_medics }
    , current_medic_valid_targets{ other.current_medic_valid_targets }
    , tile_to_heal_coordinates{ other.tile_to_heal_coordinates }
    , board{ other.board }
    , active_player_index{ other.active_player_index }
    , way_of_using_the_tile(other.way_of_using_the_tile)
    , decisions_left_count{ other.decisions_left_count }
    , turn_count{ other.turn_count }
    , winner(other.winner)
{
    for (int i = 0; i < players_count; i++)
        players.at(i) = make_unique<Player>(*other.players.at(i));
    if (other.tile_being_used)
        tile_being_used = make_unique<Tile>(*other.tile_being_used);
    ++games_created_count;
}

void
GameData::next_turn()
{
    switch_active_player();
    turn_count = turn_count + 1;
}

void
GameData::switch_active_player()
{
    active_player_index = (active_player_index + 1) % players_count;
}

auto
GameData::get_active_player() const -> Player*
{
    return players.at(active_player_index).get();
}

auto
GameData::get_previous_player() const -> Player*
{
    int previous_player_index = abs((active_player_index - 1) % players_count);
    return players.at(previous_player_index).get();
}

auto
GameData::is_endgame_criteria_fulfilled() -> bool
{
    if (turn_count < players_count)
        return false;
    update_winner();
    is_final_turn_in_place =
        get_previous_player()->get_resources()->get_army()->is_empty();
    return is_final_turn_in_place or is_final_battle_over or winner;
}

auto
GameData::get_board() -> Board*
{
    return &board;
}

auto
GameData::get_winner() const -> Player*
{
    return winner;
}

auto
GameData::get_decision_manager() -> PlayerDecisionManager*
{
    return &decision_manager;
}

auto
GameData::get_way_of_using_current_tile() const -> const TILE_USAGE_WAY&
{
    return way_of_using_the_tile;
}

auto
GameData::get_turn_counter() const -> const int&
{
    return turn_count;
}

auto
GameData::get_decisions_left_count() const -> const int&
{
    return decisions_left_count;
}

void
GameData::set_way_of_using_current_tile(TILE_USAGE_WAY usage)
{
    way_of_using_the_tile = usage;
}

void
GameData::set_decisions_left_count(int new_count)
{
    decisions_left_count = new_count;
}

void
GameData::decrement_decisions_left_count()
{
    decisions_left_count--;
}

void
GameData::set_players(unique_ptr<Player> player1, unique_ptr<Player> player2)
{
    players[0] = move(player1);
    players[1] = move(player2);
}

void
GameData::assign_default_armies()
{
    unique_ptr<ArmyBuilder> moloch_builder = make_unique<MolochBuilder>();
    moloch_builder->assign_army(players.at(0).get());
    unique_ptr<ArmyBuilder> outpost_builder = make_unique<OutpostBuilder>();
    outpost_builder->assign_army(players.at(1).get());
}

auto
GameData::operator==(const GameData& other) const -> bool
{
    if (this->board != other.board)
        return false;
    if (this->is_final_turn_in_place != other.is_final_turn_in_place)
        return false;
    if (this->players_count != other.players_count)
        return false;
    if (this->turn_count != other.turn_count)
        return false;
    if (this->decisions_left_count != other.decisions_left_count)
        return false;
    if (*this->get_active_player() != *other.get_active_player())
        return false;
    if (*this->get_previous_player() != *other.get_previous_player())
        return false;
    if (this->winner != other.winner)
        return false;
    return true;
}

auto
GameData::operator!=(const GameData& other) const -> bool
{
    return !(*this == other);
}

void
GameData::shuffle_players()
{
    shuffle(begin(players), end(players), Rng::rng);
}

void
GameData::shuffle_armies()
{
    for (auto& player : players) {
        player->get_resources()->get_army()->shuffle();
    }
}

void
GameData::update_winner()
{
    update_hp_of_headquarters();
    int headquarters_left_count = hp_of_headquarters.size();
    if (not is_final_turn_in_place and headquarters_left_count == 2)
        return;
    switch (headquarters_left_count) {
        case 0:
            winner = nullptr;
            return; // A tie
        case 1:
            winner = hp_of_headquarters[0].first;
            return;
    }
    // Get player with most headquarter HP
    int maximal_hp = -1;
    for (auto hq_to_hp : hp_of_headquarters) {
        if (hq_to_hp.second > maximal_hp) {
            winner = hq_to_hp.first;
            maximal_hp = hq_to_hp.second;
        }
    }
    // check if it's a tie.
    if (winner == hp_of_headquarters[0].first and
        hp_of_headquarters[0].second == hp_of_headquarters[1].second) {
        winner = nullptr;
        return;
    }
}

void
GameData::update_hp_of_headquarters()
{
    hp_of_headquarters.erase(hp_of_headquarters.begin(),
                             hp_of_headquarters.end());
    for (auto const coords : get_board()->get_hq_coords()) {
        Tile* hq = get_board()->get_tile_ptr(coords);
        hp_of_headquarters.emplace_back(hq->get_owner(), hq->get_hp());
    }
}
