#include <Army.h>
#include <Game.h>
#include <GameRecorder.h>
#include <GameState.h>
#include <HumanPlayerLogic.h>
#include <InjectablePlayerLogic.h>
#include <MonteCarloTreeSearch.h>
#include <MolochBuilder.h>
#include <OutpostBuilder.h>
#include <RandomPlayerLogic.h>
#include <Rng.h>
#include <TextGui.h>
#include <Tile.h>

#include <MctsPlayerLogic.h>
#include <UCT.h>
#include <EnsembleUct.h>
#include <RandomizedUct.h>
#include <Colours.h>

#include <ctime>
#include <iostream>
#include <chrono>

using namespace std;

auto
main() -> int
{
    const int number_of_iterations = 1;
    vector<Game> games(number_of_iterations);

    unique_ptr<ArmyBuilder> first_builder = make_unique<OutpostBuilder>();
    const string first_player_colour = COLOURS.GREEN;
    const string first_player_name = "Human player";
    shared_ptr<PlayerLogic> first_logic = make_shared<HumanPlayerLogic>();

    unique_ptr<ArmyBuilder> second_builder = make_unique<MolochBuilder>();
    const string second_player_colour = COLOURS.RED;
    const string second_player_name = "AI player";
    shared_ptr<PlayerLogic> second_logic = make_shared<InjectablePlayerLogic>(make_unique<RandomizedUct>());

    cout << "Started duel between " << first_player_name << " and " << second_player_name << endl;
    map<string, int> win_count{};
    win_count[first_player_name] = 0;
    win_count[second_player_name] = 0;
    //Ensure both players get the same number of plays as first player
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    for (int i = 0; i < number_of_iterations; i++) {
        Player* active_player = games.at(i).get_data().get_active_player();
        Player* inactive_player = games.at(i).get_data().get_previous_player();

        if (rand()%2 == 0) {
            active_player->set_logic(first_logic->clone());
            active_player->set_name(first_player_name);
            active_player->set_colour(first_player_colour);
            first_builder->assign_army(active_player);
            inactive_player->set_logic(second_logic->clone());
            inactive_player->set_name(second_player_name);
            inactive_player->set_colour(second_player_colour);
            second_builder->assign_army(inactive_player);
        }
        else {
            active_player->set_logic(second_logic->clone());
            active_player->set_name(second_player_name);
            active_player->set_colour(second_player_colour);
            first_builder->assign_army(inactive_player);
            inactive_player->set_logic(first_logic->clone());
            inactive_player->set_name(first_player_name);
            inactive_player->set_colour(first_player_colour);
            second_builder->assign_army(active_player);
        }

        games.at(i).set_gui(make_unique<TextGui>());
        games.at(i).execute_whole_game();
        Player* winner = games.at(i).get_data().get_winner();
        if (winner)
            win_count.at(winner->get_name())++;
    }
    cout << endl;
    cout << first_player_name << " won " << win_count.at(first_player_name) << " times " << endl;
    cout << second_player_name << " won " << win_count.at(second_player_name) << " times " << endl;
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::seconds> (end - begin).count() << "[s] elapsed" << std::endl;
    return 0;
}
