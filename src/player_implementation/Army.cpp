#include <Army.h>
#include <Rng.h>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <random>
#include <stdexcept>

using namespace std;

Army::Army()
    : tiles{ vector<unique_ptr<Tile>>() }
{}

Army::Army(unique_ptr<Tile> headquarter, vector<unique_ptr<Tile>>& tiles)
    : headquarter{ move(headquarter) }
{
    while (not tiles.empty()) {
        unique_ptr<Tile> tile = move(tiles.back());
        tiles.pop_back();
        assure_unique_tile_name(tile.get());
        tile_names.insert(tile->get_name());
        this->tiles.push_back(move(tile));
    }
}

void
Army::assure_unique_tile_name(Tile* tile)
{
    string tile_name = tile->get_name();
    int count_of_this_tile = 1;
    if (tile_names.find(tile_name) != tile_names.end()) {
        tile_name = tile_name + "_" + to_string(count_of_this_tile);
    }
    while (tile_names.find(tile_name) != tile_names.end()) {
        tile_name.resize(tile_name.size() - 2);
        tile_name = tile_name + "_" + to_string(count_of_this_tile);
        ++count_of_this_tile;
    }
    tile->set_name(tile_name);
}

Army::Army(const Army& other)
{
    Tile* other_hq = other.headquarter.get();
    if (other_hq)
        this->headquarter = make_unique<Tile>(*other_hq);
    else
        this->headquarter = nullptr;

    auto& other_tiles{ other.tiles };
    for (auto const& tile_ptr : other_tiles) {
        this->tiles.push_back(make_unique<Tile>(*tile_ptr));
    }
}

void
Army::shuffle()
{
    std::shuffle(tiles.begin(), tiles.end(), Rng::rng);
}

auto
Army::is_empty() const -> bool
{
    return tiles.empty();
}

auto
Army::draw_tile() -> unique_ptr<Tile>
{
    if (tiles.empty())
        throw logic_error("Drawing from an empty army");
    unique_ptr<Tile> result{ move(tiles.back()) };
    tiles.pop_back();
    return result;
}

auto
Army::get_headquarter() -> unique_ptr<Tile>
{
    if (not headquarter)
        throw logic_error("Trying to get absent headquarter");
    return move(headquarter);
}

void
Army::set_headquarter(unique_ptr<Tile> headquarter)
{
    this->headquarter = move(headquarter);
}

auto
Army::get_tile_names() const -> const unordered_set<string>&
{
    return tile_names;
}

auto
Army::operator==(const Army& other) const -> bool
{
    const Tile* this_hq = this->headquarter.get();
    const Tile* other_hq = other.headquarter.get();
    // Ensuring both hq's exist in army, and not on the board
    if (not(this_hq and other_hq))
        return false;
    if (*this_hq != *other_hq)
        return false;
    const vector<unique_ptr<Tile>>& this_tiles{ this->tiles };
    const vector<unique_ptr<Tile>>& other_tiles{ other.tiles };
    if (this_tiles.size() != other_tiles.size())
        return false;
    for (long unsigned int i = 0; i < this_tiles.size(); i++)
        if (*this_tiles.at(i) != *other_tiles.at(i))
            return false;
    return true;
}

auto
Army::operator!=(const Army& other) const -> bool
{
    return !(*this == other);
}
