#include <Army.h>
#include <HumanPlayerLogic.h>
#include <MctsPlayerLogic.h>
#include <Player.h>
#include <PlayerLogic.h>
#include <PlayerResources.h>
#include <RandomPlayerLogic.h>

#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>
#include <stdexcept>
#include <utility>
#include <vector>

using namespace std;

int Player::count = 0;

Player::Player()
    : id{ count }
    , name{ "empty" }
    , colour{ "\033[1;31m" }
    , resources{}
    , double_movement_bonus_count{ 0 }
{
    logic = make_unique<HumanPlayerLogic>();
    ++count;
}

Player::Player(string name, string colour)
    : id{ count }
    , name{ std::move(name) }
    , colour{ std::move(colour) }
    , resources{}
    , double_movement_bonus_count{ 0 }
{
    logic = make_unique<MctsPlayerLogic>();
    ++count;
}

Player::Player(const Player& other)
    : id{ other.id }
    , name{ other.get_name() }
    , colour{ other.get_colour() }
    , resources{ other.resources }
    , logic{ other.logic }
    , double_movement_bonus_count{ other.double_movement_bonus_count }
{}

void
Player::set_logic(shared_ptr<PlayerLogic> new_logic)
{
    logic = std::move(new_logic);
}

auto
Player::operator==(const Player& other) const -> bool
{
    if (this->name != other.name)
        return false;
    return true;
}

auto
Player::operator!=(const Player& other) const -> bool
{
    return !(*this == other);
}
