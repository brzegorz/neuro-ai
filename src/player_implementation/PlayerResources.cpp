#include <Army.h>
#include <PlayerResources.h>
#include <TextGui.h>

#include <iostream>

using namespace std;

PlayerResources::PlayerResources()
    : army{ nullptr }
    , hand{}
    , next_turn_tiles{}
{}

PlayerResources::PlayerResources(const PlayerResources& other)
{
    if (other.army)
        army = make_unique<Army>(*other.army);
    for (auto const& tile_ptr : other.hand)
        this->hand.push_back(make_unique<Tile>(*tile_ptr));
    for (auto const& tile_ptr : other.next_turn_tiles)
        this->next_turn_tiles.push_back(make_unique<Tile>(*tile_ptr));
}

void
PlayerResources::receive_tile(unique_ptr<Tile> drawn_tile)
{
    hand.push_back(move(drawn_tile));
}

auto
PlayerResources::pick_tile_from_hand(int index) -> unique_ptr<Tile>
{
    unique_ptr<Tile> picked_tile = move(hand.at(index));
    hand.erase(hand.begin() + index);
    return picked_tile;
}

void
PlayerResources::keep_tile_for_next_turn(unique_ptr<Tile> tile_to_keep)
{
    next_turn_tiles.push_back(move(tile_to_keep));
}

void
PlayerResources::receive_previous_turn_tiles()
{
    for (auto& next_turn_tile : next_turn_tiles) {
        hand.push_back(move(next_turn_tile));
    }
    next_turn_tiles.erase(next_turn_tiles.begin(), next_turn_tiles.end());
}

void
PlayerResources::erase_hand()
{
    hand.erase(hand.begin(), hand.end());
}

auto
PlayerResources::get_hand_size() const -> int
{
    return hand.size();
}

auto
PlayerResources::get_army() const -> Army*
{
    return army.get();
}

void
PlayerResources::set_army(unique_ptr<Army> a)
{
    this->army = move(a);
}

auto
PlayerResources::operator==(const PlayerResources& other) const -> bool
{
    if (this->hand != other.hand)
        return false;
    if (this->next_turn_tiles != other.next_turn_tiles)
        return false;
    if (this->army and other.army)
        if (*(this->army) != *(other.army))
            return false;
    if (not this->army xor not other.army)
        return false;
    return true;
}

auto
PlayerResources::operator!=(const PlayerResources& other) const -> bool
{
    return !(*this == other);
}

auto
PlayerResources::compare_hands(const PlayerResources& other) const -> bool
{
    if (this->get_hand_size() != other.get_hand_size())
        return false;
    const vector<unique_ptr<Tile>>& other_hand = other.hand;
    auto other_hand_it = other_hand.begin();
    auto this_hand_it = (this->hand).begin();
    auto this_hand_it_end = (this->hand).end();
    while (this_hand_it != this_hand_it_end) {
        if (*(*this_hand_it) != *(*other_hand_it))
            return false;
        ++this_hand_it;
        ++other_hand_it;
    }
    return true;
}

auto
PlayerResources::compare_next_turn_tiles(const PlayerResources& other) const
    -> bool
{
    auto other_next_turn_tiles_it = other.next_turn_tiles.begin();
    auto this_next_turn_tiles_it = (this->next_turn_tiles).begin();
    auto this_next_turn_tiles_it_end = (this->next_turn_tiles).end();
    while (this_next_turn_tiles_it != this_next_turn_tiles_it_end) {
        if (*(*this_next_turn_tiles_it) != *(*other_next_turn_tiles_it))
            return false;
        ++this_next_turn_tiles_it;
        ++other_next_turn_tiles_it;
    }
    return true;
}
