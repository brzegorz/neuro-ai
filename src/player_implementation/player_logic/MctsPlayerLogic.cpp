#include <GameData.h>
#include <MctsPlayerLogic.h>
#include <PlayerResources.h>

#include <algorithm>
#include <iostream>
#include <random>
#include <vector>

using namespace std;

MctsPlayerLogic::MctsPlayerLogic()
    : mcts{}
{}

MctsPlayerLogic::MctsPlayerLogic(const MctsPlayerLogic& other)
    : mcts{ other.mcts }
{}

auto
MctsPlayerLogic::pick_tile_to_use(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked which tile to use");
}

auto
MctsPlayerLogic::pick_how_to_use_tile(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked how to use the tile");
}

auto
MctsPlayerLogic::pick_tile_target_location(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked where to place the tile");
}

auto
MctsPlayerLogic::pick_movement_source_and_target(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked movement parameters");
}

auto
MctsPlayerLogic::pick_bomb_target(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked bomb target");
}

auto
MctsPlayerLogic::pick_sniper_target(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked sniper target");
}

auto
MctsPlayerLogic::pick_push_source_and_direction(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked tile to push");
}

auto
MctsPlayerLogic::pick_pushed_tile_location(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked where to have tile pushed");
}

auto
MctsPlayerLogic::pick_innate_movement_target(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked where to move the tile");
}

auto
MctsPlayerLogic::pick_whether_to_use_innates(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked whether to use innate ability");
}

auto
MctsPlayerLogic::pick_tile_with_innate(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked which tile's innate ability to use");
}

auto
MctsPlayerLogic::pick_tile_to_heal(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked which tile's to heal");
}

auto
MctsPlayerLogic::pick_attack_to_heal(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked which attack to heal");
}

auto
MctsPlayerLogic::pick_medic_to_use(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked which medic to use");
}

auto
MctsPlayerLogic::pick_medic_to_lose(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked which medic to save");
}

auto
MctsPlayerLogic::make_any_decision(Game* game, const string& information)
    -> unique_ptr<PlayerDecision>
{
    unique_ptr<PlayerDecision> result = mcts.get_decision(game);
    if (game->get_data().gui) {
        cout << information << ": ";
        result->print_decision();
        cout << endl;
    }
    return result;
}

auto
MctsPlayerLogic::clone() const -> unique_ptr<PlayerLogic>
{
    return make_unique<MctsPlayerLogic>();
}
