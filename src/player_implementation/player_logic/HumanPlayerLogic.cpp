#include <BombInstant.h>
#include <Game.h>
#include <HumanPlayerLogic.h>
#include <Movement.h>
#include <PlaceOnBoard.h>
#include <PlayerResources.h>
#include <PushBack.h>
#include <Sniper.h>
#include <Colours.h>

#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>
#include <sstream>
#include <vector>

using namespace std;

auto
HumanPlayerLogic::pick_tile_to_use(Game* game) -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    PlayerResources* active_player_resources =
        game->get_data().get_active_player()->get_resources();
    string question = "Please pick tile to use.";
    set<int> valid_indexes;
    for (int i = 0; i < active_player_resources->get_hand_size(); i++) {
        valid_indexes.insert(i);
    }
    int tile_index =
        game->get_data().gui->get_numeric_input(question, valid_indexes);
    decision->set_tile_to_use_index(tile_index);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_how_to_use_tile(Game* game) -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    string question = "Do you want to:\n\
                       Use the tile - 0\n\
                       Keep the tile for next turn - 1\n\
                       Discard the tile - 2\n";
    set<int> valid_ways_of_usage{ 1, 2 };
    if (game->get_data().tile_being_used->is_usable(game))
        valid_ways_of_usage.insert(0);
    int chosen_way_of_usage_int;
    chosen_way_of_usage_int =
        game->get_data().gui->get_numeric_input(question, valid_ways_of_usage);
    decision->set_chosen_way_of_tile_usage(chosen_way_of_usage_int);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_tile_target_location(Game* game)
    -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    vector<PlayerDecision> valid_moves = PlaceOnBoard{}.get_valid_moves(game);
    set<Coordinates> empty_hexes{};
    for (PlayerDecision& decision : valid_moves)
        empty_hexes.insert(decision.get_tile_target_location().first);
    string question = "Please pick where to place the tile";
    Gui* gui = game->get_data().gui.get();
    Coordinates target_index = gui->get_coords(question, empty_hexes);
    DIRECTION chosen_rotation = gui->get_direction();
    decision->set_tile_target_location(
        target_index.x, target_index.y, chosen_rotation);

        cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_movement_source_and_target(Game* game)
    -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    vector<PlayerDecision> valid_moves = Movement{}.get_valid_moves(game);
    set<int> source_xs{};
    set<int> source_ys{};
    set<int> target_xs{};
    set<int> target_ys{};
    set<Coordinates> valid_sources{};
    for (PlayerDecision decision : valid_moves) {
        valid_sources.insert(get<0>(decision.get_movement_source_and_target()));
    }
    string source_question = "Please pick which tile to move";
    Gui* gui = game->get_data().gui.get();
    Coordinates source = gui->get_coords(source_question, valid_sources);

    set<Coordinates> valid_targets{};
    valid_moves = Movement{}.get_valid_moves(game);
    for (PlayerDecision decision : valid_moves) {
        tuple<Coordinates, Coordinates, DIRECTION> source_and_target{};
        source_and_target = decision.get_movement_source_and_target();
        if (get<0>(source_and_target) == source)
            valid_targets.insert(get<1>(source_and_target));
    }
    string target_question = "Please pick where to move the tile";
    Coordinates target = gui->get_coords(target_question, valid_targets);

    int chosen_rotation = gui->get_direction();
    decision->set_movement_source_and_target(
        source, target, (DIRECTION)chosen_rotation);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_bomb_target(Game* game) -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    vector<PlayerDecision> valid_decisions =
        BombInstant{}.get_valid_moves(game);
    set<Coordinates> valid_hexes{};
    for (PlayerDecision decision : valid_decisions)
        valid_hexes.insert(decision.get_tile_target_location().first);

    string target_question = "Please pick where to drop the bomb";
    Coordinates target =
        game->get_data().gui->get_coords(target_question, valid_hexes);

    decision->set_tile_target_location(target.x, target.y, N);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_sniper_target(Game* game) -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    vector<PlayerDecision> valid_decisions = Sniper{}.get_valid_moves(game);
    set<Coordinates> valid_hexes{};
    for (PlayerDecision decision : valid_decisions)
        valid_hexes.insert(decision.get_tile_target_location().first);

    string target_question = "Please pick tile to snipe";
    Coordinates target =
        game->get_data().gui->get_coords(target_question, valid_hexes);

    decision->set_tile_target_location(target.x, target.y, N);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_push_source_and_direction(Game* game)
    -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    vector<PlayerDecision> valid_moves = PushBack{}.get_valid_moves(game);
    map<Coordinates, set<DIRECTION>> valid_combinations{};
    set<Coordinates> valid_sources{};
    for (PlayerDecision valid_move : valid_moves) {
        pair<Coordinates, DIRECTION> move_combination{};
        move_combination = valid_move.get_push_source_and_direction();
        valid_combinations[move_combination.first].insert(
            move_combination.second);
        valid_sources.insert(move_combination.first);
    }
    Gui* gui = game->get_data().gui.get();
    Coordinates push_source =
        gui->get_coords("Pick pushing tile", valid_sources);
    set<DIRECTION> valid_push_dirs = valid_combinations.at(push_source);
    DIRECTION push_direction;
    push_direction =
        gui->get_direction("Pick in which side to push", valid_push_dirs);
    std::unique_ptr<PlayerDecision> result = make_unique<PlayerDecision>();
    result->set_push_source_and_direction(push_source, push_direction);

    cout << COLOURS.NEUTRAL;
    return result;
}

auto
HumanPlayerLogic::pick_pushed_tile_location(Game* game)
    -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    vector<PlayerDecision> valid_moves = game->get_valid_moves();
    set<Coordinates> valid_targets{};
    for (PlayerDecision& decision : valid_moves)
        valid_targets.insert(decision.get_tile_target_location().first);
    string question = "Please pick where to have the tile pushed";
    Coordinates target_index =
        game->get_data().gui->get_coords(question, valid_targets);
    decision->set_tile_target_location(target_index.x, target_index.y, 0);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_innate_movement_target(Game* game)
    -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    vector<PlayerDecision> valid_moves = game->get_valid_moves();
    set<Coordinates> empty_hexes{};
    for (PlayerDecision& decision : valid_moves)
        empty_hexes.insert(decision.get_tile_target_location().first);
    string question = "Please pick where to place the tile";
    Gui* gui = game->get_data().gui.get();
    Coordinates target_index = gui->get_coords(question, empty_hexes);
    DIRECTION chosen_rotation = gui->get_direction();
    decision->set_tile_target_location(
        target_index.x, target_index.y, (int)chosen_rotation);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_whether_to_use_innates(Game* game)
    -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    vector<PlayerDecision> valid_moves = game->get_valid_moves();
    set<int> valid_values{};
    for (PlayerDecision& decision : valid_moves) {
        valid_values.insert(decision.get_whether_to_use_innates());
    }
    string question =
        "Please decide whether to use a tile with innate ability.\n\
                       Use - 1\n\
                       Don't use - 0\n";
    bool whether_to_use =
        game->get_data().gui->get_numeric_input(question, valid_values);
    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    decision->set_whether_to_use_innates(whether_to_use);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_tile_with_innate(Game* game)
    -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    vector<PlayerDecision> valid_decisions = game->get_valid_moves();
    set<Coordinates> valid_hexes{};
    for (PlayerDecision decision : valid_decisions)
        valid_hexes.insert(decision.get_tile_target_location().first);
    string target_question = "Please pick tile with innate ability to use";
    Coordinates target =
        game->get_data().gui->get_coords(target_question, valid_hexes);
    decision->set_tile_target_location(target.x, target.y, N);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_tile_to_heal(Game* game) -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    vector<PlayerDecision> valid_decisions = game->get_valid_moves();
    set<Coordinates> valid_hexes{};
    for (PlayerDecision decision : valid_decisions)
        valid_hexes.insert(decision.get_tile_target_location().first);
    auto medic_coords = game->get_data().current_medic_coordinates;
    string target_question = game->get_data().get_active_player()->get_colour() + "Please pick tile to heal by medic at (" +
                             to_string(medic_coords.x) + ", " +
                             to_string(medic_coords.y) + ")";
    Coordinates target =
        game->get_data().gui->get_coords(target_question, valid_hexes);
    decision->set_tile_target_location(target.x, target.y, N);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_attack_to_heal(Game* game) -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    GameData& game_data = game->get_data();
    string target_question = "Please pick which attack to heal. Options:\n \
        index, damage_value";
    auto damage = game_data.get_board()
                      ->get_tile_ptr(game_data.tile_to_heal_coordinates)
                      ->get_damage();
    set<int> valid_indexes{};
    for (uint i = 0; i < damage.size(); ++i) {
        valid_indexes.insert(i);
        target_question += "\n " + to_string(i) + ", " + to_string(damage[i]);
    }
    int index =
        game->get_data().gui->get_numeric_input(target_question, valid_indexes);
    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    decision->set_attack_to_heal_index(index);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_medic_to_use(Game* game) -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    vector<PlayerDecision> valid_decisions = game->get_valid_moves();
    set<Coordinates> valid_hexes{};
    for (PlayerDecision decision : valid_decisions)
        valid_hexes.insert(decision.get_tile_target_location().first);
    auto target_coords = game->get_data().tile_to_heal_coordinates;
    string target_question = "Please pick which medic should heal tile at (" +
                             to_string(target_coords.x) + ", " +
                             to_string(target_coords.y) + ")";
    Coordinates target =
        game->get_data().gui->get_coords(target_question, valid_hexes);
    decision->set_tile_target_location(target.x, target.y, N);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::pick_medic_to_lose(Game* game) -> unique_ptr<PlayerDecision>
{
    cout << game->get_data().get_active_player()->get_colour();

    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    vector<PlayerDecision> valid_decisions = game->get_valid_moves();
    set<Coordinates> valid_hexes{};
    for (PlayerDecision decision : valid_decisions)
        valid_hexes.insert(decision.get_tile_target_location().first);
    string target_question = "Please pick which medic should be lost";
    Coordinates target =
        game->get_data().gui->get_coords(target_question, valid_hexes);
    decision->set_tile_target_location(target.x, target.y, N);

    cout << COLOURS.NEUTRAL;
    return decision;
}

auto
HumanPlayerLogic::clone() const -> unique_ptr<PlayerLogic>
{
    return make_unique<HumanPlayerLogic>(*this);
}
