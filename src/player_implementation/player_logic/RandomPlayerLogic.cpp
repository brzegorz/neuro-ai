#include <Game.h>
#include <PlayerResources.h>
#include <PushBack.h>
#include <RandomPlayerLogic.h>
#include <Rng.h>
#include <TextGui.h>
#include <Tile.h>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <random>
#include <vector>

using namespace std;

discrete_distribution<int> RandomPlayerLogic::way_of_usage_dist =
    discrete_distribution<int>{ 90, 5, 5 };

auto
RandomPlayerLogic::pick_tile_to_use(Game* game) -> unique_ptr<PlayerDecision>
{
    //~ cout << "Random tile to use" << endl;
    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    PlayerResources* active_player_resources =
        game->get_data().get_active_player()->get_resources();

    int tile_index =
        Rng::random_int(0, active_player_resources->get_hand_size() - 1);
    decision->set_tile_to_use_index(tile_index);
    return decision;
}

auto
RandomPlayerLogic::pick_how_to_use_tile(Game* game)
    -> unique_ptr<PlayerDecision>
{
    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    int chosen_way_of_usage_int;
    if (game->get_data().tile_being_used->is_usable(game)) {
        chosen_way_of_usage_int = way_of_usage_dist(Rng::rng);
    } else {
        chosen_way_of_usage_int = Rng::random_int(1, 2);
    }
    decision->set_chosen_way_of_tile_usage(chosen_way_of_usage_int);
    return decision;
}

auto
RandomPlayerLogic::pick_tile_target_location(Game* game)
    -> unique_ptr<PlayerDecision>
{
    const unordered_set<Coordinates>& empty_hexes =
        game->get_data().get_board()->get_empty_indexes();
    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    Coordinates chosen_coords =
        *next(empty_hexes.begin(), Rng::random_int(0, empty_hexes.size() - 1));
    int chosen_rotation_int = Rng::random_int(0, 5);

    decision->set_tile_target_location(
        chosen_coords.x, chosen_coords.y, chosen_rotation_int);
    return decision;
}

auto
RandomPlayerLogic::pick_movement_source_and_target(Game* game)
    -> unique_ptr<PlayerDecision>
{
    GameData& game_data = game->get_data();
    Player* active_player = game_data.get_active_player();
    Board* board = game_data.get_board();
    set<Coordinates> valid_sources{};
    for (Coordinates coords : board->get_players_indexes(active_player)) {
        if (not board->get_tile_ptr(coords)->is_in_net())
            valid_sources.insert(coords);
    }
    int source_index = Rng::random_int(0, valid_sources.size() - 1);
    Coordinates source = *next(valid_sources.begin(), source_index);

    const bool has_double_movement =
        board->get_tile_ptr(source)->get_owner()->has_double_movement();
    set<Coordinates> valid_targets{};
    for (Coordinates target : board->get_empty_neighbours(source)) {
        valid_targets.insert(target);
        if (has_double_movement) {
            for (Coordinates far_target : board->get_empty_neighbours(target))
                valid_targets.insert(far_target);
        }
    }
    valid_targets.insert(source);
    int target_index = Rng::random_int(0, valid_targets.size() - 1);
    Coordinates target = *next(valid_targets.begin(), target_index);

    DIRECTION chosen_rotation = all_directions[Rng::random_int(0, 5)];

    unique_ptr<PlayerDecision> final_decision = make_unique<PlayerDecision>();
    final_decision->set_movement_source_and_target(
        source, target, chosen_rotation);
    return final_decision;
}

auto RandomPlayerLogic::pick_bomb_target(Game * /*game*/)
    -> unique_ptr<PlayerDecision>
{
    const array<Coordinates, 7> valid_hexes{
        Coordinates(1, 3), Coordinates(1, 5), Coordinates(2, 2),
        Coordinates(2, 4), Coordinates(2, 6), Coordinates(3, 3),
        Coordinates(3, 5)
    };
    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    int chosen_coords_index = Rng::random_int(0, valid_hexes.size() - 1);
    Coordinates bomb_target = valid_hexes.at(chosen_coords_index);
    decision->set_tile_target_location(bomb_target.x, bomb_target.y, N);
    return decision;
}

auto
RandomPlayerLogic::pick_sniper_target(Game* game) -> unique_ptr<PlayerDecision>
{
    vector<Coordinates> valid_targets{};
    const Board* board = game->get_data().get_board();
    const Player* enemy = game->get_data().get_previous_player();
    for (Coordinates coords : board->get_players_indexes(enemy)) {
        valid_targets.push_back(coords);
    }
    int chosen_coords_index = Rng::random_int(0, valid_targets.size() - 1);
    Coordinates sniper_target = valid_targets.at(chosen_coords_index);
    unique_ptr<PlayerDecision> decision = make_unique<PlayerDecision>();
    decision->set_tile_target_location(sniper_target.x, sniper_target.y, N);
    return decision;
}

auto
RandomPlayerLogic::pick_push_source_and_direction(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return slow_universal_choice(game);
}

auto
RandomPlayerLogic::pick_pushed_tile_location(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return slow_universal_choice(game);
}

auto
RandomPlayerLogic::pick_innate_movement_target(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return slow_universal_choice(game);
}

auto
RandomPlayerLogic::pick_whether_to_use_innates(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return slow_universal_choice(game);
}

auto
RandomPlayerLogic::pick_tile_with_innate(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return slow_universal_choice(game);
}

auto
RandomPlayerLogic::pick_tile_to_heal(Game* game) -> unique_ptr<PlayerDecision>
{
    return slow_universal_choice(game);
}

auto
RandomPlayerLogic::pick_attack_to_heal(Game* game) -> unique_ptr<PlayerDecision>
{
    return slow_universal_choice(game);
}

auto
RandomPlayerLogic::pick_medic_to_use(Game* game) -> unique_ptr<PlayerDecision>
{
    return slow_universal_choice(game);
}

auto
RandomPlayerLogic::pick_medic_to_lose(Game* game) -> unique_ptr<PlayerDecision>
{
    return slow_universal_choice(game);
}

auto
RandomPlayerLogic::slow_universal_choice(Game* game)
    -> unique_ptr<PlayerDecision>
{
    vector<PlayerDecision> valid_moves = game->get_valid_moves();
    int result_index = Rng::random_int(0, valid_moves.size() - 1);
    return make_unique<PlayerDecision>(valid_moves.at(result_index));
}

auto
RandomPlayerLogic::clone() const -> unique_ptr<PlayerLogic>
{
    return make_unique<RandomPlayerLogic>(*this);
}
