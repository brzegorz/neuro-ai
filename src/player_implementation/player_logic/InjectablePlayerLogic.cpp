#include <GameData.h>
#include <InjectablePlayerLogic.h>
#include <PlayerResources.h>

#include <algorithm>
#include <iostream>
#include <random>
#include <vector>

using namespace std;

InjectablePlayerLogic::InjectablePlayerLogic()
{
}

InjectablePlayerLogic::InjectablePlayerLogic(unique_ptr<DecisionMaker> decision_maker)
{
    this->decision_maker = move(decision_maker);
}

InjectablePlayerLogic::InjectablePlayerLogic(const InjectablePlayerLogic& other)
    : decision_maker{ other.decision_maker->clone() }
{}

auto
InjectablePlayerLogic::pick_tile_to_use(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked which tile to use");
}

auto
InjectablePlayerLogic::pick_how_to_use_tile(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked how to use the tile");
}

auto
InjectablePlayerLogic::pick_tile_target_location(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked where to place the tile");
}

auto
InjectablePlayerLogic::pick_movement_source_and_target(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked movement parameters");
}

auto
InjectablePlayerLogic::pick_bomb_target(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked bomb target");
}

auto
InjectablePlayerLogic::pick_sniper_target(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked sniper target");
}

auto
InjectablePlayerLogic::pick_push_source_and_direction(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked tile to push");
}

auto
InjectablePlayerLogic::pick_pushed_tile_location(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked where to have tile pushed");
}

auto
InjectablePlayerLogic::pick_innate_movement_target(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked where to move the tile");
}

auto
InjectablePlayerLogic::pick_whether_to_use_innates(Game* game)
    -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked whether to use innate ability");
}

auto
InjectablePlayerLogic::pick_tile_with_innate(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked which tile's innate ability to use");
}

auto
InjectablePlayerLogic::pick_tile_to_heal(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked which tile's to heal");
}

auto
InjectablePlayerLogic::pick_attack_to_heal(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked which attack to heal");
}

auto
InjectablePlayerLogic::pick_medic_to_use(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked which medic to use");
}

auto
InjectablePlayerLogic::pick_medic_to_lose(Game* game) -> unique_ptr<PlayerDecision>
{
    return make_any_decision(game, "Picked which medic to save");
}

auto
InjectablePlayerLogic::make_any_decision(Game* game, const string& information)
    -> unique_ptr<PlayerDecision>
{
    unique_ptr<PlayerDecision> result = decision_maker->get_decision(game);
    if (game->get_data().gui) {
        cout << information << ": ";
        result->print_decision();
        cout << endl;
    }
    return result;
}

auto
InjectablePlayerLogic::clone() const -> unique_ptr<PlayerLogic>
{
    auto result = make_unique<InjectablePlayerLogic>();
    result->decision_maker = this->decision_maker->clone();
    return result;
}
