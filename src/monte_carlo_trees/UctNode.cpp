#include <Army.h>
#include <Colours.h>
#include <RandomPlayerLogic.h>
#include <UctNode.h>

#include <Gui.h>
#include <TextGui.h>

#include <algorithm>
#include <limits>

using namespace std;

UctNode::UctNode(const Game& game)
    : game{ make_unique<Game>(game) }
    , wins{ 0 }
    , visits{ 0 }
    , UCT{ numeric_limits<double>::max() }
    , parent{ nullptr }
{
    active_player_name = this->game->get_data().get_active_player()->get_name();
    this->game->continue_until_input_required();
    this->game->get_data().shuffle_armies();
    possible_moves = this->game->get_valid_moves();
    is_game_finished = this->game->get_data().is_game_finished;
}

UctNode::UctNode(const Game& game, PlayerDecision& next_decision)
    : game{ make_unique<Game>(game) }
    , wins{ 0 }
    , visits{ 0 }
    , UCT{ numeric_limits<double>::max() }
    , parent{ nullptr }
{
    this->game->get_data().get_decision_manager()->add_player_decision(
        make_unique<PlayerDecision>(next_decision));
    active_player_name = this->game->get_data().get_active_player()->get_name();
    this->game->continue_until_input_required();
    is_game_finished = this->game->get_data().is_game_finished;
    if (not is_game_finished)
        possible_moves = this->game->get_valid_moves();
}

UctNode::UctNode(const UctNode& other)
    : game{ make_unique<Game>(*other.game) }
    , active_player_name{ other.active_player_name }
    , is_game_finished{ other.is_game_finished }
    , wins{ other.wins }
    , visits{ other.visits }
    , UCT{ other.UCT }
    , possible_moves{ other.possible_moves }
{
    for (auto const& [move, child_node] : other.children) {
        children[move] = make_unique<UctNode>(*child_node);
    }
}

auto
UctNode::traverse() -> UctNode*
{
    UctNode* analysed_node = this;
    while (analysed_node and analysed_node->is_fully_expanded() and
           analysed_node->get_highest_UCT_child()) {
        analysed_node = analysed_node->get_highest_UCT_child();
    }
    return analysed_node;
}

auto
UctNode::expand() -> UctNode*
{
    if (possible_moves.empty())
        return this;
    PlayerDecision expanding_move = possible_moves.back();
    possible_moves.pop_back();
    children[expanding_move] = make_unique<UctNode>(*game, expanding_move);
    children[expanding_move]->set_parent(this);
    return children[expanding_move].get();
}

auto
UctNode::playout() -> pair<Player*, int>
{
    if (game->get_data().gui)
        cout << COLOURS.WEB;
    if (game->get_data().is_game_finished) {
        GameData& game_data = game->get_data();
        return get_winner_with_hp_advantage(game_data);
    }

    Game playout_game{ *game };
    GameData& game_data = playout_game.get_data();
    game_data.get_active_player()->set_logic(make_shared<RandomPlayerLogic>());
    game_data.get_previous_player()->set_logic(
        make_shared<RandomPlayerLogic>());
    playout_game.loop_game();
    if (game->get_data().gui)
        cout << COLOURS.NEUTRAL;
    return get_winner_with_hp_advantage(game_data);
}

auto
UctNode::get_winner_with_hp_advantage(GameData& game_data) -> pair<Player*, int>
{
    Board* board = game_data.get_board();
    Player* winner = game_data.get_winner();
    if (not winner)
        return make_pair(winner, 1);
    int winner_hp = 1;
    int loser_hp = -1;
    for (const Coordinates& hq_coords : board->get_hq_coords()) {
        Tile* hq = board->get_tile_ptr(hq_coords);
        if (hq and *(hq->get_owner()) == *winner)
            winner_hp = hq->get_hp();
        else
            loser_hp = hq->get_hp();
    }
    return make_pair(winner, winner_hp - loser_hp);
}

void
UctNode::backpropagate(Player* winner, int hp_advantage, UctNode* /*root*/)
{
    UctNode* node_to_update = this;
    while (node_to_update->parent) {
        node_to_update->update(winner, hp_advantage);
        node_to_update = node_to_update->parent;
    }
    node_to_update->update(winner, hp_advantage);
}

void
UctNode::update(Player* winner, int /*hp_advantage*/)
{
    visits++;
    double numerical_stabilizer = 10000;
    if (winner and winner->get_name() == active_player_name) {
        wins++;
    }
    if (parent) {
        double parent_visits = (double)parent->get_visits_count() + 1;
        double explorativeness = 2 * sqrt(2);
        double exploitation_component =
            ((numerical_stabilizer * wins) / visits);
        double exploration_component =
            sqrt(numerical_stabilizer) * explorativeness *
            sqrt((numerical_stabilizer * log(parent_visits)) / visits);
        UCT = exploitation_component + exploration_component;
    }
}

auto
UctNode::get_highest_UCT_child() const -> UctNode*
{
    double max_UCT = -1;
    UctNode* max_UCT_node = nullptr;
    for (auto const& [decision, node] : children) {
        if (node->get_UCT() > max_UCT) {
            max_UCT_node = node.get();
            max_UCT = node->get_UCT();
        }
    }
    return max_UCT_node;
}

auto
UctNode::get_best_move() const -> PlayerDecision
{
    int top_visits_count = -1;
    double top_node_UCT = -100;
    PlayerDecision best_move;
    for (auto const& [move, child] : children) {
        if (child->get_visits_count() > top_visits_count) {
            top_visits_count = child->get_visits_count();
            top_node_UCT = child->get_UCT();
            best_move = move;
        } else if (child->get_visits_count() == top_visits_count) {
            if (child->get_UCT() > top_node_UCT) {
                top_node_UCT = child->get_UCT();
                best_move = move;
            }
        }
    }
    return best_move;
}

auto
UctNode::get_visits_counts_per_move() const -> map<PlayerDecision, int>
{
    map<PlayerDecision, int> result{};
    for (auto const& [move, child] : children) {
        result[move] = child->get_visits_count();
    }
    return result;
}

auto
UctNode::get_UCT() const -> double
{
    return UCT;
}

auto
UctNode::get_visits_count() const -> int
{
    return visits;
}

auto
UctNode::get_children_count() const -> int
{
    return children.size() + possible_moves.size();
}

auto
UctNode::is_fully_expanded() const -> bool
{
    return possible_moves.empty();
}

auto
UctNode::get_child(const PlayerDecision& decision) -> unique_ptr<UctNode>
{
    if (children.count(decision) > 0) {
        return move(children.at(decision));
    } else {
        return nullptr;
    }
}

void
UctNode::set_parent(UctNode* parent)
{
    this->parent = parent;
}

template<typename T>
struct node_by_visits_comparator
{
    auto operator()(const T& a, const T& b) const -> bool
    {
        return a.second->get_visits_count() < b.second->get_visits_count();
    }
};

auto
UctNode::operator<(const UctNode& other) const -> bool
{
    return UCT < other.UCT;
}
