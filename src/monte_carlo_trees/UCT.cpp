#include <DecisionMaker.h>
#include <UCT.h>
#include <memory>

using namespace std;

UCT::UCT()
    : root{ nullptr }
    , moves_queue{}
{}

UCT::UCT(const UCT& other)
    : root{ make_unique<UctNode>(*other.root) }
    , moves_queue{ other.moves_queue }
    , prev_moves_archive_size(other.prev_moves_archive_size)
{}

auto
UCT::get_decision(Game* game) -> unique_ptr<PlayerDecision>
{
    if (not moves_queue.player_decision_available())
        prepare_decision(game);
    prev_moves_archive_size =
        game->get_data().get_decision_manager()->get_archive_size();
    return moves_queue.get_player_decision();
}

void
UCT::prepare_decision(Game* game)
{
    find_root(game);
    const int decisions_count = root->get_children_count();
    int max_iterations = 20000;
    if (decisions_count == 1)
        max_iterations = 1;
    for (int iterations = root->get_visits_count(); iterations < max_iterations;
         iterations++) {
        Gui* gui = game->get_data().gui.get();
        if (gui)
            gui->print_progress_bar(iterations, max_iterations);
        single_iteration();
    }
    unique_ptr<PlayerDecision> best_move;
    best_move = make_unique<PlayerDecision>(root->get_best_move());
    root = root->get_child(*best_move);
    if (root)
        root->set_parent(nullptr);
    moves_queue.add_player_decision(move(best_move));
}

void
UCT::find_root(Game* game)
{
    root = make_unique<UctNode>(*game);
}

void
UCT::single_iteration()
{
    UctNode* node_to_expand = root->traverse();
    if (node_to_expand) {
        UctNode* node_for_playout = node_to_expand->expand();
        const pair<Player*, int> playout_winner =
            node_for_playout->playout();
        node_for_playout->backpropagate(
            playout_winner.first, playout_winner.second, root.get());
    }
}

auto
UCT::clone() const -> unique_ptr<DecisionMaker>
{
    return make_unique<UCT>();
}
