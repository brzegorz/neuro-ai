#include <DecisionMaker.h>
#include <EnsembleUct.h>
#include <memory>

using namespace std;

EnsembleUct::EnsembleUct()
    : roots{ nullptr }
    , moves_queue{}
{}

EnsembleUct::EnsembleUct(const EnsembleUct& other)
    : roots{}
    , moves_queue{ other.moves_queue }
    , prev_moves_archive_size(other.prev_moves_archive_size)
{
    for (int i = 0; i < n_trees; i++)
        roots[i] = make_unique<UctNode>(*(other.roots[i].get()));
}

auto
EnsembleUct::get_decision(Game* game) -> unique_ptr<PlayerDecision>
{
    if (not moves_queue.player_decision_available())
        prepare_decision(game);
    prev_moves_archive_size =
        game->get_data().get_decision_manager()->get_archive_size();
    return moves_queue.get_player_decision();
}

void
EnsembleUct::prepare_decision(Game* game)
{
    find_root(game);
    int max_iterations = 20000;
    int max_iterations_per_tree = max_iterations / n_trees;
    map<PlayerDecision, int> visits_per_move{};
    for (int tree_index = 0; tree_index < n_trees; tree_index++) {
        auto root = roots[tree_index].get();
        int decisions_count = root->get_children_count();
        if (decisions_count == 1)
            max_iterations = 1;
        Gui* gui = game->get_data().gui.get();
        if (gui)
            gui->print_progress_bar(tree_index, n_trees);
        for (int iterations = root->get_visits_count();
             iterations < max_iterations_per_tree;
             iterations++) {

            single_iteration(root);
            auto tree_visits_per_move = root->get_visits_counts_per_move();
            for (auto it = tree_visits_per_move.begin();
                 it != tree_visits_per_move.end();
                 ++it) {
                auto move = (*it).first;
                auto visits = (*it).second;
                if (visits_per_move.find(move) != visits_per_move.end()) {
                    visits_per_move[move] += visits;
                } else {
                    visits_per_move[move] = visits;
                }
            }
        }
    }
    int maxVisits = 0;
    PlayerDecision best_move;
    for (auto it = visits_per_move.cbegin(); it != visits_per_move.cend(); ++it)
        if (it->second > maxVisits) {
            best_move = it->first;
            maxVisits = it->second;
        }

    moves_queue.add_player_decision(make_unique<PlayerDecision>(best_move));
}

void
EnsembleUct::find_root(Game* game)
{
    for (int i = 0; i < n_trees; i++) {
        roots[i] = make_unique<UctNode>(*game);
    }
}

void
EnsembleUct::single_iteration(UctNode* root)
{
    UctNode* node_to_expand = root->traverse();
    if (node_to_expand) {
        UctNode* node_for_playout = node_to_expand->expand();
        const pair<Player*, int> playout_winner = node_for_playout->playout();
        node_for_playout->backpropagate(
            playout_winner.first, playout_winner.second, root);
    }
}

auto
EnsembleUct::clone() const -> unique_ptr<DecisionMaker>
{
    return make_unique<EnsembleUct>();
}