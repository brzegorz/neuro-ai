#include <DecisionMaker.h>
#include <MonteCarloTreeSearch.h>
#include <memory>

using namespace std;

MonteCarloTreeSearch::MonteCarloTreeSearch()
    : root{ nullptr }
    , moves_queue{}
{}

MonteCarloTreeSearch::MonteCarloTreeSearch(const MonteCarloTreeSearch& other)
    : root{ make_unique<Node>(*other.root) }
    , moves_queue{ other.moves_queue }
    , prev_moves_archive_size(other.prev_moves_archive_size)
{}

auto
MonteCarloTreeSearch::get_decision(Game* game) -> unique_ptr<PlayerDecision>
{
    if (not moves_queue.player_decision_available())
        prepare_decision(game);
    prev_moves_archive_size =
        game->get_data().get_decision_manager()->get_archive_size();
    return moves_queue.get_player_decision();
}

void
MonteCarloTreeSearch::prepare_decision(Game* game)
{
    find_root(game);
    const int decisions_count = root->get_children_count();
    int max_iterations = 5000;
    if (decisions_count == 1)
        max_iterations = 1;
    for (int iterations = root->get_visits_count(); iterations < max_iterations;
         iterations++) {
        Gui* gui = game->get_data().gui.get();
        if (gui)
            gui->print_progress_bar(iterations, max_iterations);
        single_iteration();
    }
    unique_ptr<PlayerDecision> best_move;
    best_move = make_unique<PlayerDecision>(root->get_best_move());
    root = root->get_child(*best_move);
    if (root)
        root->set_parent(nullptr);
    moves_queue.add_player_decision(move(best_move));
}

void
MonteCarloTreeSearch::find_root(Game* game)
{
    root = make_unique<Node>(*game);
}

void
MonteCarloTreeSearch::single_iteration()
{
    Node* node_to_expand = root->traverse();
    if (node_to_expand) {
        Node* node_for_playout = node_to_expand->expand();
        // Weird decisions if only one playout is used because of army shuffling
        for (int i = 0; i < 4; i++) {
            const pair<Player*, int> playout_winner =
                node_for_playout->playout();
            node_for_playout->backpropagate(
                playout_winner.first, playout_winner.second, root.get());
        }
    }
}

auto
MonteCarloTreeSearch::clone() const -> unique_ptr<DecisionMaker>
{
    return make_unique<MonteCarloTreeSearch>();
}