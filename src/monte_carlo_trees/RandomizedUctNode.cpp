#include <Army.h>
#include <Colours.h>
#include <RandomPlayerLogic.h>
#include <RandomizedUctNode.h>
#include <Rng.h>

#include <Gui.h>
#include <TextGui.h>

#include <algorithm>
#include <limits>

using namespace std;

RandomizedUctNode::RandomizedUctNode(const Game& game)
    : game{ make_unique<Game>(game) }
    , wins{ 0 }
    , visits{ 0 }
    , UCT{ numeric_limits<double>::max() }
    , parent{ nullptr }
{
    active_player_name = this->game->get_data().get_active_player()->get_name();
    this->game->continue_until_input_required();
    this->game->get_data().shuffle_armies();
    possible_moves = this->game->get_valid_moves();
    is_game_finished = this->game->get_data().is_game_finished;
}

RandomizedUctNode::RandomizedUctNode(const Game& game, PlayerDecision& next_decision)
    : game{ make_unique<Game>(game) }
    , wins{ 0 }
    , visits{ 0 }
    , UCT{ numeric_limits<double>::max() }
    , parent{ nullptr }
{
    this->game->get_data().get_decision_manager()->add_player_decision(
        make_unique<PlayerDecision>(next_decision));
    active_player_name = this->game->get_data().get_active_player()->get_name();
    this->game->continue_until_input_required();
    is_game_finished = this->game->get_data().is_game_finished;
    if (not is_game_finished)
        possible_moves = this->game->get_valid_moves();
}

RandomizedUctNode::RandomizedUctNode(const RandomizedUctNode& other)
    : game{ make_unique<Game>(*other.game) }
    , active_player_name{ other.active_player_name }
    , is_game_finished{ other.is_game_finished }
    , wins{ other.wins }
    , visits{ other.visits }
    , UCT{ other.UCT }
    , possible_moves{ other.possible_moves }
{
    for (auto const& [move, child_node_array] : other.children) {
        for (int i = 0; i < count_of_states; i++)
            children[move][i] = make_unique<RandomizedUctNode>(*(child_node_array[i]));
    }
}

auto
RandomizedUctNode::traverse() -> RandomizedUctNode*
{
    RandomizedUctNode* analysed_node = this;
    while (analysed_node and analysed_node->is_fully_expanded() and
           analysed_node->get_highest_UCT_child()) {
        analysed_node = analysed_node->get_highest_UCT_child();
    }
    return analysed_node;
}

auto
RandomizedUctNode::expand() -> RandomizedUctNode*
{
    if (possible_moves.empty())
        return this;
    PlayerDecision expanding_move = possible_moves.back();
    possible_moves.pop_back();
    children[expanding_move] = vector<unique_ptr<RandomizedUctNode>>(count_of_states);
    for (int index = 0; index < count_of_states; index++)
    {
        children[expanding_move][index] = make_unique<RandomizedUctNode>(*game, expanding_move);
        children[expanding_move][index]->set_parent(this);
        children[expanding_move][index]->game->get_data().shuffle_armies();
    }
    int random_index = Rng::random_int(0, count_of_states-1);
    return children[expanding_move][random_index].get();
}

auto
RandomizedUctNode::playout() -> pair<Player*, int>
{
    if (game->get_data().gui)
        cout << COLOURS.WEB;
    if (game->get_data().is_game_finished) {
        GameData& game_data = game->get_data();
        return get_winner(game_data);
    }

    Game playout_game{ *game };
    GameData& game_data = playout_game.get_data();
    game_data.get_active_player()->set_logic(make_shared<RandomPlayerLogic>());
    game_data.get_previous_player()->set_logic(
        make_shared<RandomPlayerLogic>());
    playout_game.loop_game();
    if (game->get_data().gui)
        cout << COLOURS.NEUTRAL;
    return get_winner(game_data);
}

auto
RandomizedUctNode::get_winner(GameData& game_data) -> pair<Player*, int>
{
    Board* board = game_data.get_board();
    Player* winner = game_data.get_winner();
    if (not winner)
        return make_pair(winner, 1);
    int winner_hp = 1;
    int loser_hp = -1;
    for (const Coordinates& hq_coords : board->get_hq_coords()) {
        Tile* hq = board->get_tile_ptr(hq_coords);
        if (hq and *(hq->get_owner()) == *winner)
            winner_hp = hq->get_hp();
        else
            loser_hp = hq->get_hp();
    }
    return make_pair(winner, 1);
}

void
RandomizedUctNode::backpropagate(Player* winner, int hp_advantage, RandomizedUctNode* /*root*/)
{
    RandomizedUctNode* node_to_update = this;
    while (node_to_update->parent) {
        node_to_update->update(winner, hp_advantage);
        node_to_update = node_to_update->parent;
    }
    node_to_update->update(winner, hp_advantage);
}

void
RandomizedUctNode::update(Player* winner, int /*hp_advantage*/)
{
    visits++;
    if (winner and winner->get_name() == active_player_name) {
        wins++;
    }
    if (parent) {
        double parent_visits = (double)parent->get_visits_count() + 1;
        UCT = calculate_UCT(wins, visits, parent_visits);
    }
}

auto
RandomizedUctNode::calculate_UCT(int wins, int visits, int parent_visits) const -> double
{
    double numerical_stabilizer = 10000;
    double explorativeness = 2 * sqrt(2);
    double exploitation_component =
        ((numerical_stabilizer * wins) / visits);
    double exploration_component =
        sqrt(numerical_stabilizer) * explorativeness *
        sqrt((numerical_stabilizer * log(parent_visits)) / visits);
    return exploitation_component + exploration_component;
}

auto
RandomizedUctNode::get_highest_UCT_child() const -> RandomizedUctNode*
{
    double max_UCT = -1;
    RandomizedUctNode* max_UCT_node = nullptr;
    for (auto const& [decision, nodes] : children) {
        int decision_visits = 0;
        int decision_wins = 0;
        for (int i = 0; i < count_of_states; i++)
        {
            decision_visits += nodes[i]-> get_visits_count();
            decision_wins += nodes[i] -> get_wins();
        }
        auto UCT = calculate_UCT(decision_wins, decision_visits, visits);
        if (UCT > max_UCT) {
            int index = Rng::random_int(0, count_of_states-1);
            max_UCT_node = nodes[index].get();
            max_UCT = UCT;
        }
    }
    return max_UCT_node;
}

auto
RandomizedUctNode::get_best_move() const -> PlayerDecision
{
    int top_visits_count = -1;
    double top_node_UCT = -100;
    PlayerDecision best_move;
    for (auto const& [move, child] : children) {
        int visits_count = 0;
        int average_UCT = 0;
        for (int i = 0; i < count_of_states; i++)
        {    
            visits_count+=child[i]->get_visits_count();
            average_UCT+=child[i]->get_UCT()/count_of_states;
        }
        if (visits_count > top_visits_count) {
            top_visits_count = visits_count;
            top_node_UCT = average_UCT;
            best_move = move;
        } else if (visits_count == top_visits_count) {
            if (average_UCT > top_node_UCT) {
                top_node_UCT = average_UCT;
                best_move = move;
            }
        }
    }
    return best_move;
}

auto
RandomizedUctNode::get_visits_counts_per_move() const -> map<PlayerDecision, int>
{
    map<PlayerDecision, int> result{};
    for (auto const& [move, child] : children) {
        int visits_count = 0;
        for (int i = 0; i < count_of_states; i++)
        {    
            visits_count+=child[i]->get_visits_count();
        }
        result[move] = visits_count;
    }
    return result;
}

auto
RandomizedUctNode::get_UCT() const -> double
{
    return UCT;
}

auto
RandomizedUctNode::get_visits_count() const -> int
{
    return visits;
}

auto
RandomizedUctNode::get_children_count() const -> int
{
    return children.size() + possible_moves.size();
}

auto
RandomizedUctNode::is_fully_expanded() const -> bool
{
    return possible_moves.empty();
}

auto
RandomizedUctNode::get_child(const PlayerDecision& decision) -> unique_ptr<RandomizedUctNode>
{
    if (children.count(decision) > 0) {
        int index = Rng::random_int(0, count_of_states-1);
        return move(children.at(decision)[index]);
    } else {
        return nullptr;
    }
}

void
RandomizedUctNode::set_parent(RandomizedUctNode* parent)
{
    this->parent = parent;
}

template<typename T>
struct node_by_visits_comparator
{
    auto operator()(const T& a, const T& b) const -> bool
    {
        return a.second->get_visits_count() < b.second->get_visits_count();
    }
};

auto
RandomizedUctNode::operator<(const RandomizedUctNode& other) const -> bool
{
    return UCT < other.UCT;
}
