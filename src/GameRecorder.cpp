#include <GameRecorder.h>
#include <Rng.h>

#include <chrono>
#include <fstream>
#include <iostream>

using namespace std;

void
GameRecorder::add_game_vector(const VectorizedBoard& game_vector)
{
    archive.emplace_back(game_vector);
}

void
GameRecorder::add_winner(Player* winner)
{
    for (VectorizedBoard& board : archive) {
        board.add_winner(winner);
    }
}

void
GameRecorder::save_to_file() const
{
    string file_path = make_file_path();
    string file_content = archive.at(0).get_csv_header();
    for (const VectorizedBoard& board : archive) {
        file_content += "\n" + board.get_csv_row();
    }
    cout << "Saving game to " << file_path << endl;
    ofstream of(file_path);
    if (of.is_open()) {
        of << file_content << endl;
        of.flush();
        of.close();
        cout << "wrote the file successfully!" << endl;
    } else {
        cerr << "Failed to open file" << endl;
    }
}

auto
GameRecorder::make_file_path() -> string
{
    string file_path = "/home/brzegorz/Projects/neuro-ai/data/game_";
    string date = to_string(chrono::system_clock::to_time_t(
                      chrono::system_clock::now())) +
                  "_" + to_string(Rng::random_int(1000, 10000));
    file_path += date;
    file_path += ".csv";
    return file_path;
}
