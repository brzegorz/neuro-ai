#include <Army.h>
#include <Battle.h>
#include <Game.h>
#include <GameState.h>
#include <MakeFinalTurn.h>
#include <MakePlayerTurn.h>
#include <ObtainTileToUse.h>
#include <ObtainWhetherToUseInnates.h>
#include <PlaceHeadquarters.h>
#include <PlayerLogic.h>
#include <PlayerResources.h>
#include <Tile.h>

#include <algorithm>
#include <random>

using namespace std;

Game::Game()
    : game_data{}
    , next_state{ make_unique<PlaceHeadquarters>() }
{}

Game::Game(const Game& other)
    : game_data{ other.game_data }
    , next_state{ other.next_state->clone() }
{}

void
Game::execute_whole_game()
{
    if (not game_data.get_active_player()->get_resources()->get_army() or
        not game_data.get_previous_player()->get_resources()->get_army())
        game_data.assign_default_armies();
    game_data.get_board()->set_vectorized_board(
        make_unique<VectorizedBoard>(this));
    game_data.shuffle_players();
    game_data.shuffle_armies();
    loop_game();
}

void
Game::loop_game()
{
    while (not game_data.is_endgame_criteria_fulfilled()) {
        if (not next_state)
            throw logic_error("Trying to continue a finished game");
        next_state->advance_game(this);
    }
    finish_game();
}

void
Game::loop_for_single_battle()
{
    this->set_next_state(make_unique<Battle>());
    do {
        next_state->advance_game(this);
    } while (game_data.current_battle_turn != -2);
}

void
Game::continue_until_input_required()
{
    if (not next_state)
        return;
    if (game_data.is_endgame_criteria_fulfilled()) {
        while (not(game_data.is_game_finished or is_player_input_needed())) {
            next_state->advance_game(this);
        }
    } else {
        while (not(game_data.is_endgame_criteria_fulfilled() or
                   is_player_input_needed())) {
            next_state->advance_game(this);
        }
        if (game_data.is_endgame_criteria_fulfilled())
            next_state = make_unique<MakeFinalTurn>();
        while (not(game_data.is_game_finished or is_player_input_needed())) {
            next_state->advance_game(this);
        }
    }
}

void
Game::pick_next_state()
{
    Board* board = game_data.get_board();
    if (game_data.is_final_battle_over)
        finish_game();
    if (board->is_full()) {
        set_next_state(make_unique<Battle>());
        return;
    }
    PlayerResources* player_resources =
        game_data.get_active_player()->get_resources();
    int player_hand_size = player_resources->get_hand_size();
    if (game_data.get_decisions_left_count() > 0 and player_hand_size > 0) {
        set_next_state(make_unique<ObtainWhetherToUseInnates>());
    } else if (not game_data.is_one_player_out_of_tiles) {
        player_resources->erase_hand();
        for (const Coordinates& coords : board->get_occupied_indexes()) {
            board->get_tile_ptr(coords)->allow_innate_function_usage();
        }
        game_data.next_turn();
        set_next_state(make_unique<MakePlayerTurn>());
    } else {
        set_next_state(make_unique<MakeFinalTurn>());
    }
}

void
Game::set_next_state(unique_ptr<GameState> next_state_ptr)
{
    next_state = move(next_state_ptr);
}

auto
Game::get_valid_moves() -> vector<PlayerDecision>
{
    if (next_state)
        return next_state->get_valid_moves(this);
    return vector<PlayerDecision>{};
}

auto
Game::get_data() -> GameData&
{
    return game_data;
}

void
Game::finish_game()
{
    next_state = make_unique<MakeFinalTurn>();
    while (not game_data.is_game_finished) {
        // cout << next_state->get_name()<<endl;
        next_state->advance_game(this);
    }
    if (recorder) {
        recorder->add_winner(game_data.get_winner());
        recorder->save_to_file();
    }
}

auto
Game::is_player_input_needed() -> bool
{
    PlayerDecisionManager* decision_mgr = game_data.get_decision_manager();
    return next_state and next_state->requires_player_input(this) and
           not decision_mgr->player_decision_available();
}

void
Game::set_gui(unique_ptr<Gui> gui)
{
    game_data.gui = move(gui);
}
