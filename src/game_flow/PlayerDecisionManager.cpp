#include <PlayerDecisionManager.h>

#include <iostream>

using namespace std;

PlayerDecisionManager::PlayerDecisionManager(const PlayerDecisionManager& other)
{
    for (auto const& player_decision : other.player_decisions) {
        unique_ptr<PlayerDecision> tmp;
        tmp = make_unique<PlayerDecision>(*player_decision);
        player_decisions.push_front(move(tmp));
    }
    decisions_made = other.decisions_made;
}

auto
PlayerDecisionManager::player_decision_available() const -> bool
{
    return not player_decisions.empty();
}

auto
PlayerDecisionManager::get_player_decision() -> unique_ptr<PlayerDecision>
{
    unique_ptr<PlayerDecision> result = move(player_decisions.front());
    player_decisions.pop_front();
    decisions_made++;
    return result;
}

void
PlayerDecisionManager::add_player_decision(unique_ptr<PlayerDecision> decision)
{
    player_decisions.push_front(move(decision));
}

void
PlayerDecisionManager::add_test_decision(unique_ptr<PlayerDecision> decision)
{
    player_decisions.push_back(move(decision));
}

auto
PlayerDecisionManager::get_archive_size() -> int
{
    return decisions_made;
}
