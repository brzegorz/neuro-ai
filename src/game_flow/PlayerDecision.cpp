#include <PlayerDecision.h>

#include <iostream>
#include <tuple>

using namespace std;

auto
PlayerDecision::get_tile_to_use_index() -> int
{
    if (type != TILE_TO_USE_INDEX)
        crash_if_wrong_type_read();
    int result = decisions.front();
    decisions.pop_front();
    return result;
}

void
PlayerDecision::set_tile_to_use_index(int index)
{
    crash_if_already_set();
    type = TILE_TO_USE_INDEX;
    decisions.push_back(index);
}

auto
PlayerDecision::get_chosen_way_of_tile_usage() -> TILE_USAGE_WAY
{
    if (type != WAY_OF_TILE_USAGE)
        crash_if_wrong_type_read();
    auto result = (TILE_USAGE_WAY)decisions.front();
    decisions.pop_front();
    return result;
}

void
PlayerDecision::set_chosen_way_of_tile_usage(int way)
{
    crash_if_already_set();
    type = WAY_OF_TILE_USAGE;
    decisions.push_back(way);
}

auto
PlayerDecision::get_tile_target_location() -> pair<Coordinates, DIRECTION>
{
    if (type != TILE_TARGET_LOCATION)
        crash_if_wrong_type_read();
    int x = decisions.front();
    decisions.pop_front();
    int y = decisions.front();
    decisions.pop_front();
    auto direction = (DIRECTION)decisions.front();
    decisions.pop_front();
    return make_pair(Coordinates(x, y), direction);
}

void
PlayerDecision::set_tile_target_location(int x, int y, int direction)
{
    crash_if_already_set();
    type = TILE_TARGET_LOCATION;
    decisions.push_back(x);
    decisions.push_back(y);
    decisions.push_back(direction);
}

auto
PlayerDecision::get_movement_source_and_target() -> tuple<Coordinates,

                                                          Coordinates,

                                                          DIRECTION>
{
    if (type != MOVEMENT_PARAMS)
        crash_if_wrong_type_read();
    int source_x = decisions.front();
    decisions.pop_front();
    int source_y = decisions.front();
    decisions.pop_front();
    int target_x = decisions.front();
    decisions.pop_front();
    int target_y = decisions.front();
    decisions.pop_front();
    auto rotation = (DIRECTION)decisions.front();
    decisions.pop_front();
    return make_tuple(Coordinates(source_x, source_y),
                      Coordinates(target_x, target_y),
                      rotation);
}

void
PlayerDecision::set_movement_source_and_target(Coordinates source,
                                               Coordinates target,
                                               DIRECTION rotation)
{
    crash_if_already_set();
    type = MOVEMENT_PARAMS;
    decisions.push_back(source.x);
    decisions.push_back(source.y);
    decisions.push_back(target.x);
    decisions.push_back(target.y);
    decisions.push_back(rotation);
}

auto
PlayerDecision::get_push_source_and_direction() -> pair<Coordinates, DIRECTION>
{
    if (type != PUSH_PARAMS)
        crash_if_wrong_type_read();
    int source_x = decisions.front();
    decisions.pop_front();
    int source_y = decisions.front();
    decisions.pop_front();
    auto push_direction = (DIRECTION)decisions.front();
    decisions.pop_front();
    return make_pair(Coordinates(source_x, source_y), push_direction);
}

void
PlayerDecision::set_push_source_and_direction(Coordinates source,
                                              DIRECTION rotation)
{
    crash_if_already_set();
    type = PUSH_PARAMS;
    decisions.push_back(source.x);
    decisions.push_back(source.y);
    decisions.push_back(rotation);
}

auto
PlayerDecision::get_whether_to_use_innates() -> bool
{
    if (type != WHETHER_TO_USE_INNATES)
        crash_if_wrong_type_read();
    bool result = decisions.front();
    decisions.pop_front();
    return result;
}

void
PlayerDecision::set_whether_to_use_innates(bool whether_to_use)
{
    crash_if_already_set();
    type = WHETHER_TO_USE_INNATES;
    decisions.push_back(whether_to_use);
}

auto
PlayerDecision::get_attack_to_heal_index() -> int
{
    if (type != ATTACK_TO_HEAL_INDEX)
        crash_if_wrong_type_read();
    int result = decisions.front();
    decisions.pop_front();
    return result;
}

void
PlayerDecision::set_attack_to_heal_index(int index)
{
    crash_if_already_set();
    type = ATTACK_TO_HEAL_INDEX;
    decisions.push_back(index);
}

void
PlayerDecision::print_decision() const
{
    if (decisions.empty())
        cout << "empty move";
    else
        for (auto const& decision_int : decisions) {
            cout << decision_int << " ";
        }
}

auto
PlayerDecision::operator<(const PlayerDecision& other) const -> bool
{
    return decisions < other.decisions;
}

void
PlayerDecision::crash_if_already_set()
{
    if (not decisions.empty())
        throw logic_error("Setting a single decision value twice");
}

void
PlayerDecision::crash_if_wrong_type_read()
{
    throw logic_error("Wrong type of decision have been read");
}
