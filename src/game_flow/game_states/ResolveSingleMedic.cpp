#include <Game.h>
#include <GameState.h>
#include <HealSingleTile.h>
#include <PlayerLogic.h>
#include <ResolveMedics.h>
#include <ResolveSingleMedic.h>

#include <algorithm>
#include <map>
#include <numeric>

using namespace std;

void
ResolveSingleMedic::advance_game(Game* game)
{

    vector<Coordinates> coords_to_heal{};
    GameData& game_data = game->get_data();
    Board* board = game->get_data().get_board();
    Player* medic_owner =
        board->get_tile_ptr(game_data.current_medic_coordinates)->get_owner();
    bool active_player_need_switching =
        medic_owner == game->get_data().get_active_player();
    if (active_player_need_switching)
        game_data.switch_active_player();
    game_data.tile_to_heal_coordinates = get_tile_to_heal(game);
    if (active_player_need_switching)
        game_data.switch_active_player();
    game->set_next_state(make_unique<HealSingleTile>());
}

auto
ResolveSingleMedic::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    GameData& game_data = game->get_data();
    vector<PlayerDecision> result{};
    for (auto const coords : game_data.current_medic_valid_targets) {
        PlayerDecision valid_target{};
        valid_target.set_tile_target_location(coords.x, coords.y, N);
        result.push_back(valid_target);
    }
    return result;
}

auto
ResolveSingleMedic::clone() -> unique_ptr<GameState>
{
    return make_unique<ResolveSingleMedic>();
}

auto
ResolveSingleMedic::get_tile_to_heal(Game* game) -> Coordinates
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        unique_ptr<PlayerDecision> healing_decision;
        healing_decision =
            game_data.get_active_player()->get_logic()->pick_tile_to_heal(game);
        game_data.get_decision_manager()->add_player_decision(
            move(healing_decision));
    }
    unique_ptr<PlayerDecision> result =
        game_data.get_decision_manager()->get_player_decision();
    return result->get_tile_target_location().first;
}