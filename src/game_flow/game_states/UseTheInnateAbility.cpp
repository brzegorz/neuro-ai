#include <Army.h>
#include <Game.h>
#include <MakePlayerTurn.h>
#include <ObtainWhetherToUseInnates.h>
#include <PlaceOnBoard.h>
#include <UseTheInnateAbility.h>

using namespace std;

void
UseTheInnateAbility::advance_game(Game* game)
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    const Coordinates& tile_with_innate_coords =
        game_data.tile_using_innate_coordinates;
    Tile* tile_with_innate = board->get_tile_ptr(tile_with_innate_coords);
    tile_with_innate->get_innate_function()->change_board(game);
    game->set_next_state(make_unique<ObtainWhetherToUseInnates>());
}

auto
UseTheInnateAbility::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    GameData& game_data = game->get_data();
    const Board* board = game_data.get_board();
    return board->get_tile_ptr(game_data.tile_using_innate_coordinates)
        ->get_innate_function()
        ->get_valid_moves(game);
}

auto
UseTheInnateAbility::clone() -> unique_ptr<GameState>
{
    return make_unique<UseTheInnateAbility>();
}
