#include <Colours.h>
#include <Game.h>
#include <ObtainWayOfUsingTheTile.h>
#include <PlayerLogic.h>
#include <Tile.h>
#include <UseTheTile.h>

using namespace std;

void
ObtainWayOfUsingTheTile::advance_game(Game* game)
{
    TILE_USAGE_WAY result = get_way_of_using_the_tile(game);
    game->get_data().set_way_of_using_current_tile(result);
    game->set_next_state(make_unique<UseTheTile>());
}

auto
ObtainWayOfUsingTheTile::get_way_of_using_the_tile(Game* game) -> TILE_USAGE_WAY
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        PlayerLogic* player_logic =
            game_data.get_active_player()->get_logic().get();
        game_data.get_decision_manager()->add_player_decision(
            player_logic->pick_how_to_use_tile(game));
    }
    TILE_USAGE_WAY result = game_data.get_decision_manager()
                                ->get_player_decision()
                                ->get_chosen_way_of_tile_usage();
    return result;
}

auto
ObtainWayOfUsingTheTile::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    vector<PlayerDecision> ways_of_using_tile{};
    bool use_possible = true;
    if (not game->get_data().tile_being_used->is_usable(game))
        use_possible = false;
    int first_int = 0;
    if (not use_possible)
        first_int = 1;
    for (int i = first_int; i < 3; i++) {
        PlayerDecision way_of_using_tile{};
        way_of_using_tile.set_chosen_way_of_tile_usage(i);
        ways_of_using_tile.push_back(way_of_using_tile);
    }
    return ways_of_using_tile;
}

auto
ObtainWayOfUsingTheTile::clone() -> unique_ptr<GameState>
{
    return make_unique<ObtainWayOfUsingTheTile>();
}
