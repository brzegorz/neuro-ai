#include <Colours.h>
#include <Game.h>
#include <MakeFinalTurn.h>
#include <MakePlayerTurn.h>
#include <ObtainWhetherToUseInnates.h>
#include <TextGui.h>
#include <Tile.h>
#include <UseTheTile.h>

using namespace std;

void
UseTheTile::advance_game(Game* game)
{
    GameData& game_data = game->get_data();

    if (game_data.tile_being_used) {
        use_the_tile(game);
    } else {
        game->pick_next_state();
    }
}

void
UseTheTile::use_the_tile(Game* game)
{
    GameData& game_data = game->get_data();
    const TILE_USAGE_WAY& how_to_use =
        game_data.get_way_of_using_current_tile();
    switch (how_to_use) {
        case USE: {
            if (game->recorder) {
                const VectorizedBoard& game_record{
                    game->get_data().get_board()->get_vectorized_board()
                };
                game->recorder->add_game_vector(game_record);
            }
            game_data.decrement_decisions_left_count();
            game_data.tile_being_used->use(game);
        }; break;
        case KEEP: {
            game_data.get_active_player()
                ->get_resources()
                ->keep_tile_for_next_turn(move(game_data.tile_being_used));
            game_data.decrement_decisions_left_count();
        }; break;
        case DISCARD: {
        }; break;
        default:
            throw invalid_argument("Invalid tile usage way passed");
    }
    game_data.tile_being_used = nullptr;
}

auto
UseTheTile::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    vector<PlayerDecision> valid_moves{};
    if (not this->requires_player_input(game))
        throw logic_error(
            "Valid moves requested from gamestate which doesn't require input");
    else {
        valid_moves = game->get_data().tile_being_used->get_valid_moves(game);
    }
    return valid_moves;
}

auto
UseTheTile::clone() -> unique_ptr<GameState>
{
    return make_unique<UseTheTile>();
}
