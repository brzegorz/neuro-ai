#include <Game.h>
#include <ObtainTileWithInnate.h>
#include <ObtainWayOfUsingTheTile.h>
#include <PlayerLogic.h>
#include <Tile.h>
#include <UseTheInnateAbility.h>

using namespace std;

void
ObtainTileWithInnate::advance_game(Game* game)
{
    GameData& game_data = game->get_data();
    Coordinates innate_coords = get_coords_of_tile_with_innate(game);
    game_data.tile_using_innate_coordinates = innate_coords;
    game->set_next_state(make_unique<UseTheInnateAbility>());
}

auto
ObtainTileWithInnate::get_coords_of_tile_with_innate(Game* game) -> Coordinates
{
    GameData& game_data = game->get_data();
    PlayerLogic* player_logic =
        game_data.get_active_player()->get_logic().get();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        if (game_data.gui) {
            game_data.gui->print_board(*game_data.get_board());
            game_data.gui->print_player_hand(game_data.get_active_player());
        }
        game_data.get_decision_manager()->add_player_decision(
            player_logic->pick_tile_with_innate(game));
    }
    Coordinates innate_coords = game_data.get_decision_manager()
                                    ->get_player_decision()
                                    ->get_tile_target_location()
                                    .first;
    return innate_coords;
}

auto
ObtainTileWithInnate::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    //~ cout << "ObtainTileWithInnate" << endl;
    vector<PlayerDecision> result{};
    GameData& game_data = game->get_data();
    const Player* active_player = game_data.get_active_player();
    const Board* board = game_data.get_board();
    for (const Coordinates& coords :
         board->get_players_indexes(active_player)) {
        const Tile* tile = board->get_tile_ptr(coords);
        game_data.tile_using_innate_coordinates = coords;
        if (tile->get_innate_function() and
            tile->get_innate_function()->is_usable(game)) {
            PlayerDecision tile_with_innate{};
            tile_with_innate.set_tile_target_location(coords.x, coords.y, N);
            result.push_back(tile_with_innate);
        }
    }
    return result;
}

auto
ObtainTileWithInnate::clone() -> unique_ptr<GameState>
{
    return make_unique<ObtainTileWithInnate>();
}
