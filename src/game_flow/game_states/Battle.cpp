#include <Battle.h>
#include <BattleSingleTurn.h>
#include <Game.h>
#include <GameState.h>
#include <MakePlayerTurn.h>

#include <algorithm>

using namespace std;

void
Battle::advance_game(Game* game)
{
    GameData& game_data = game->get_data();
    auto board = game_data.get_board();
    if (game_data.current_battle_turn == -2) {
        game_data.decisions_left_count = 0; // a battle finishes player turn
        game_data.update_hp_of_headquarters();
        if (game_data.hp_of_headquarters.size() < game_data.players_count) {
            game_data.is_final_turn_in_place = true;
            game_data.is_final_battle_over = true;
            game_data.is_one_player_out_of_tiles = true;
            game_data.update_winner();
        }
        //TODO: Make final two consecutive battles work without inducing infinite loops in MCTS bots
        if (board->is_full())
            game_data.is_final_battle_over = true;
        game_data.current_battle_turn = -1;
        game->pick_next_state();
        return;
    }

    if (game_data.current_battle_turn == -1)
        game_data.current_battle_turn =
            get_max_initiative(game_data.get_board());

    game->set_next_state(make_unique<BattleSingleTurn>());
}

auto Battle::get_valid_moves(Game * /*game*/) -> vector<PlayerDecision>
{
    throw logic_error(
        "Valid moves requested from gamestate which doesn't require input");
    return vector<PlayerDecision>{};
}

auto
Battle::clone() -> unique_ptr<GameState>
{
    return make_unique<Battle>();
}

auto
Battle::get_max_initiative(const Board* board) -> int
{
    int max_initiative = -1;
    int tile_max_initiative;
    for (auto const& coords : board->get_occupied_indexes()) {
        Tile* tile = board->get_tile_ptr(coords);
        const set<int>& tile_initiatives = tile->get_initiative();
        if (tile_initiatives.size() > 0) {
            tile_max_initiative =
                *max_element(begin(tile_initiatives), end(tile_initiatives));
            if (tile_max_initiative > max_initiative)
                max_initiative = tile_max_initiative;
        }
    }
    if (max_initiative == -1)
        throw domain_error("Board is empty, no max initiative");
    return max_initiative;
}