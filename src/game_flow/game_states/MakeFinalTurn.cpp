#include <Battle.h>
#include <Game.h>
#include <GameState.h>
#include <MakeFinalTurn.h>
#include <MakePlayerTurn.h>

using namespace std;

void
MakeFinalTurn::advance_game(Game* game)
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    if (game_data.get_winner() or board->is_full() or
        game_data.is_final_battle_over) {
        game_data.update_winner();
        game_data.is_game_finished = true;
        game->set_next_state(nullptr);
        if (game_data.gui) {
            game_data.gui->print_board(*board);
            game_data.gui->print_game_result(game_data.get_winner());
        }
    } else if (game_data.is_one_player_out_of_tiles) {
        game->set_next_state(make_unique<Battle>());
        game_data.is_final_battle_over = true;
    } else {
        game_data.is_one_player_out_of_tiles = true;
        game->set_next_state(make_unique<MakePlayerTurn>());
    }
}

auto MakeFinalTurn::get_valid_moves(Game * /*game*/) -> vector<PlayerDecision>
{
    throw logic_error(
        "Valid moves requested from gamestate which doesn't require input");
    return vector<PlayerDecision>{};
}

auto
MakeFinalTurn::clone() -> unique_ptr<GameState>
{
    return make_unique<MakeFinalTurn>();
}
