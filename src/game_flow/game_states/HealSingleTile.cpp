#include <Game.h>
#include <GameState.h>
#include <HealSingleTile.h>
#include <PlayerLogic.h>
#include <ResolveMedics.h>

#include <algorithm>
#include <map>
#include <numeric>

using namespace std;

void
HealSingleTile::advance_game(Game* game)
{
    int index_of_attack_to_heal = get_attack_to_heal_index(game);
    GameData& game_data = game->get_data();
    Tile* tile_to_heal =
        game_data.get_board()->get_tile_ptr(game_data.tile_to_heal_coordinates);
    tile_to_heal->get_healed(index_of_attack_to_heal);
    game_data.get_board()
        ->get_tile_ptr(game_data.current_medic_coordinates)
        ->receive_damage_in_battle(1, nullptr);
    game->set_next_state(make_unique<ResolveMedics>());
}

auto
HealSingleTile::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    Tile* tile_to_heal =
        board->get_tile_ptr(game_data.tile_to_heal_coordinates);
    auto damage = tile_to_heal->get_damage();
    vector<PlayerDecision> result{};
    for (uint i = 0; i < damage.size(); ++i) {
        PlayerDecision decision{};
        decision.set_attack_to_heal_index(i);
        result.push_back(decision);
    }
    return result;
}

auto
HealSingleTile::clone() -> unique_ptr<GameState>
{
    return make_unique<HealSingleTile>();
}

auto
HealSingleTile::get_attack_to_heal_index(Game* game) -> int
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        unique_ptr<PlayerDecision> healing_decision;
        healing_decision =
            game_data.get_active_player()->get_logic()->pick_attack_to_heal(
                game);
        game_data.get_decision_manager()->add_player_decision(
            move(healing_decision));
    }
    unique_ptr<PlayerDecision> result =
        game_data.get_decision_manager()->get_player_decision();
    return result->get_attack_to_heal_index();
}