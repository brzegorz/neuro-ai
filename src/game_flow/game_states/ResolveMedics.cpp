#include <Battle.h>
#include <BattleCleanBoard.h>
#include <Board.h>
#include <Game.h>
#include <GameState.h>
#include <PickMedicToLose.h>
#include <PickMedicToResolve.h>
#include <PlayerLogic.h>
#include <ResolveMedics.h>
#include <ResolveSingleMedic.h>

#include <algorithm>
#include <numeric>

using namespace std;

void
ResolveMedics::advance_game(Game* game)
{
    map<Coordinates, set<DIRECTION>> valid_targets_by_medic{};
    map<Coordinates, set<DIRECTION>> valid_medics_by_target{};
    set<Coordinates> guarded_medics{};
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    update_medics_data(*board, valid_targets_by_medic, valid_medics_by_target);
    if (are_medics_resolved(valid_targets_by_medic)) {
        game->set_next_state(make_unique<BattleCleanBoard>());
        return;
    }

    Coordinates current_medic = get_coords_of_medic_to_resolve(
        *board, valid_targets_by_medic, valid_medics_by_target);
    game_data.current_medic_coordinates = current_medic;
    vector<Coordinates> coords_to_heal{};
    for (auto const dir : valid_targets_by_medic[current_medic])
        coords_to_heal.push_back(Board::get_neighbour(current_medic, dir));
    game_data.current_medic_valid_targets = coords_to_heal;
    game->set_next_state(make_unique<ResolveSingleMedic>());
    if (coords_to_heal.size() > 1)
        return;

    Coordinates target_coords = coords_to_heal[0];
    if (valid_medics_by_target[target_coords].size() > 1) {
        game_data.current_target_valid_medics = vector<Coordinates>{};
        for (auto medic_dir : valid_medics_by_target[target_coords]) {
            auto medic_coords = Board::get_neighbour(target_coords, medic_dir);
            game_data.current_target_valid_medics.push_back(medic_coords);
        }

        game->set_next_state(make_unique<PickMedicToResolve>());
        return;
    }

    if (can_target_heal_the_medic(board, current_medic, target_coords)) {
        game->set_next_state(make_unique<PickMedicToLose>());
    }
}

auto ResolveMedics::get_valid_moves(Game * /*game*/) -> vector<PlayerDecision>
{
    throw logic_error(
        "Valid moves requested from gamestate which doesn't require input");
    return vector<PlayerDecision>{};
}

auto
ResolveMedics::clone() -> unique_ptr<GameState>
{
    return make_unique<ResolveMedics>();
}

void
ResolveMedics::update_medics_data(
    Board& board,
    map<Coordinates, set<DIRECTION>>& valid_targets_by_medic,
    map<Coordinates, set<DIRECTION>>& valid_medics_by_target)
{
    valid_targets_by_medic = map<Coordinates, set<DIRECTION>>();
    valid_medics_by_target = map<Coordinates, set<DIRECTION>>();
    auto medic_sources = board.get_medic_sources();
    for (auto const& coordsDirPair : medic_sources) {
        auto const protected_coords = coordsDirPair.first;
        auto const protected_tile = board.get_tile_ptr(protected_coords);
        if (not protected_tile)
            continue;
        for (auto const healer_direction : coordsDirPair.second) {
            auto const medic_coords =
                board.get_neighbour(protected_coords, healer_direction);
            auto const healing_direction = opposite_direction(healer_direction);
            auto const medic = board.get_tile_ptr(medic_coords);
            if (medic->get_owner() == protected_tile->get_owner() and medic->get_heals_enemy())
                continue;
            if (medic->get_owner() != protected_tile->get_owner() and not medic->get_heals_enemy())
                continue;
            if (medic->is_damaged())
                continue;
            if (not(protected_tile->is_damaged())) {
                add_info_of_undamaged_protected_tile(valid_targets_by_medic,
                                                     valid_medics_by_target,
                                                     medic_coords,
                                                     healing_direction);
                continue;
            }
            add_info_of_damaged_protected_tile(valid_targets_by_medic,
                                               valid_medics_by_target,
                                               medic_coords,
                                               healing_direction);
        }
    }
}

void
ResolveMedics::add_info_of_undamaged_protected_tile(
    map<Coordinates, set<DIRECTION>>& valid_targets_by_medic,
    map<Coordinates, set<DIRECTION>>& valid_medics_by_target,
    const Coordinates medic_coords,
    const DIRECTION healing_direction)
{
    const auto protected_coords =
        Board::get_neighbour(medic_coords, healing_direction);
    if (valid_medics_by_target.find(protected_coords) ==
        valid_medics_by_target.end())
        valid_medics_by_target.insert(
            { protected_coords, { opposite_direction(healing_direction) } });
    else
        valid_medics_by_target[protected_coords].insert(healing_direction);
}

void
ResolveMedics::add_info_of_damaged_protected_tile(
    map<Coordinates, set<DIRECTION>>& valid_targets_by_medic,
    map<Coordinates, set<DIRECTION>>& valid_medics_by_target,
    const Coordinates medic_coords,
    const DIRECTION healing_direction)
{
    if (valid_targets_by_medic.find(medic_coords) ==
        valid_targets_by_medic.end())
        valid_targets_by_medic.insert({ medic_coords, { healing_direction } });
    else
        valid_targets_by_medic[medic_coords].insert(healing_direction);

    const auto protected_coords =
        Board::get_neighbour(medic_coords, healing_direction);
    if (valid_medics_by_target.find(protected_coords) ==
        valid_medics_by_target.end())
        valid_medics_by_target.insert(
            { protected_coords, { opposite_direction(healing_direction) } });
    else
        valid_medics_by_target[protected_coords].insert(
            opposite_direction(healing_direction));
}

auto
ResolveMedics::are_medics_resolved(
    const map<Coordinates, set<DIRECTION>>& valid_targets_by_medic) -> bool
{
    int sum_of_protected_units =
        accumulate(begin(valid_targets_by_medic),
                   end(valid_targets_by_medic),
                   0,
                   [](const int previous,
                      const pair<const Coordinates, set<DIRECTION>>& p) {
                       return previous + p.second.size();
                   });
    return sum_of_protected_units == 0;
}

auto
ResolveMedics::get_coords_of_medic_to_resolve(
    Board& board,
    map<Coordinates, set<DIRECTION>>& valid_targets_by_medic,
    const map<Coordinates, set<DIRECTION>>& valid_medics_by_target)
    -> Coordinates
{
    // Precedence:
    // 1. Medics which are guarded by other medics
    // 2. Medics with multiple targets to choose
    // 3. Trivial cases
    vector<Coordinates> guarded_medics =
        get_guarded_medics(valid_targets_by_medic, valid_medics_by_target);

    if (guarded_medics.size() != 0) {
        // filter out non-guarded medics
        auto it = valid_targets_by_medic.begin();
        while (it != valid_targets_by_medic.end()) {
            if (find(guarded_medics.begin(),
                     guarded_medics.end(),
                     (*it).first) == guarded_medics.end()) {
                valid_targets_by_medic.erase(it++);
            } else {
                ++it;
            }
        }
    }

    auto medic_with_most_options =
        *max_element(begin(valid_targets_by_medic),
                     end(valid_targets_by_medic),
                     [](const pair<Coordinates, set<DIRECTION>>& p1,
                        const pair<Coordinates, set<DIRECTION>>& p2) {
                         return p1.second < p2.second;
                     });
    return medic_with_most_options.first;
}

auto
ResolveMedics::get_guarded_medics(
    const map<Coordinates, set<DIRECTION>>& valid_targets_by_medic,
    const map<Coordinates, set<DIRECTION>>& valid_medics_by_target)
    -> vector<Coordinates>
{
    vector<Coordinates> result{};
    for (auto const& medic_coords_with_targets : valid_targets_by_medic) {
        auto medic_coords = medic_coords_with_targets.first;
        if (valid_medics_by_target.find(medic_coords) !=
            valid_medics_by_target.end())
            result.push_back(medic_coords);
    }
    return result;
}

auto
ResolveMedics::can_target_heal_the_medic(Board* board,
                                         Coordinates medic,
                                         Coordinates target) -> bool
{
    auto const& medic_sources = board->get_medic_sources();
    if (medic_sources.find(medic) == medic_sources.end())
        return false;
    for (auto dir : medic_sources.at(medic))
        if (board->get_neighbour(medic, dir) == target)
            return true;
    return false;
}