#include <Battle.h>
#include <BattleCleanBoard.h>
#include <BattleSingleTurn.h>
#include <Game.h>
#include <GameState.h>
#include <MakePlayerTurn.h>
#include <ResolveMedics.h>

#include <algorithm>

using namespace std;

void
BattleSingleTurn::advance_game(Game* game)
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    for (auto const& coords : board->get_occupied_indexes()) {
        Tile* tile = board->get_tile_ptr(coords);
        if (is_tile_acting(*tile, game_data.current_battle_turn))
            tile->do_battle_actions(*board, coords);
    }
    game->set_next_state(make_unique<ResolveMedics>());
}

auto BattleSingleTurn::get_valid_moves(Game * /*game*/)
    -> vector<PlayerDecision>
{
    throw logic_error(
        "Valid moves requested from gamestate which doesn't require input");
    return vector<PlayerDecision>{};
}

auto
BattleSingleTurn::clone() -> unique_ptr<GameState>
{
    return make_unique<BattleSingleTurn>();
}

auto
BattleSingleTurn::is_tile_acting(const Tile& tile, int initiative) -> bool
{
    const set<int>& initiatives = tile.get_initiative();
    return find(initiatives.begin(), initiatives.end(), initiative) !=
           initiatives.end();
}