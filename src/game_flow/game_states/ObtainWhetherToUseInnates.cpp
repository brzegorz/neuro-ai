#include <Game.h>
#include <ObtainTileToUse.h>
#include <ObtainTileWithInnate.h>
#include <ObtainWhetherToUseInnates.h>
#include <PlayerLogic.h>
#include <Tile.h>

using namespace std;

void
ObtainWhetherToUseInnates::advance_game(Game* game)
{
    bool using_innates = get_whether_to_use_innates(game);
    if (using_innates) {
        game->set_next_state(make_unique<ObtainTileWithInnate>());
    } else {
        game->set_next_state(make_unique<ObtainTileToUse>());
    }
}

auto
ObtainWhetherToUseInnates::get_whether_to_use_innates(Game* game) -> bool
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        PlayerLogic* player_logic =
            game_data.get_active_player()->get_logic().get();
        if (game_data.gui) {
            game_data.gui->print_board(*game_data.get_board());
            game_data.gui->print_player_hand(game_data.get_active_player());
        }
        game_data.get_decision_manager()->add_player_decision(
            player_logic->pick_whether_to_use_innates(game));
    }
    bool using_innates = game_data.get_decision_manager()
                             ->get_player_decision()
                             ->get_whether_to_use_innates();
    return using_innates;
}

auto
ObtainWhetherToUseInnates::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    GameData& game_data = game->get_data();
    vector<PlayerDecision> result{};
    PlayerDecision dont_use{};
    dont_use.set_whether_to_use_innates(false);
    result.push_back(dont_use);
    const Player* active_player = game_data.get_active_player();
    const Board* board = game_data.get_board();
    for (const Coordinates& coords :
         board->get_players_indexes(active_player)) {
        const Tile* tile = board->get_tile_ptr(coords);
        game_data.tile_using_innate_coordinates = coords;
        if (tile->get_innate_function() and
            tile->get_innate_function()->is_usable(game)) {
            PlayerDecision use_innates{};
            use_innates.set_whether_to_use_innates(true);
            result.push_back(use_innates);
            return result;
        }
    }
    return result;
}

auto
ObtainWhetherToUseInnates::clone() -> unique_ptr<GameState>
{
    return make_unique<ObtainWhetherToUseInnates>();
}
