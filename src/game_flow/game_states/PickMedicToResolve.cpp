#include <Game.h>
#include <GameState.h>
#include <HealSingleTile.h>
#include <PickMedicToResolve.h>
#include <PlayerLogic.h>
#include <ResolveMedics.h>
#include <ResolveSingleMedic.h>

#include <algorithm>
#include <map>
#include <numeric>

using namespace std;

void
PickMedicToResolve::advance_game(Game* game)
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        PlayerLogic* player_logic =
            game_data.get_active_player()->get_logic().get();
        game_data.get_decision_manager()->add_player_decision(
            player_logic->pick_medic_to_use(game));
    }
    game_data.current_medic_coordinates = game_data.get_decision_manager()
                                              ->get_player_decision()
                                              ->get_tile_target_location()
                                              .first;
    game->set_next_state(make_unique<ResolveSingleMedic>());
}

auto
PickMedicToResolve::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    GameData& game_data = game->get_data();
    vector<PlayerDecision> result{};
    for (auto const coords : game_data.current_target_valid_medics) {
        PlayerDecision valid_target{};
        valid_target.set_tile_target_location(coords.x, coords.y, N);
        result.push_back(valid_target);
    }
    return result;
}

auto
PickMedicToResolve::clone() -> unique_ptr<GameState>
{
    return make_unique<PickMedicToResolve>();
}