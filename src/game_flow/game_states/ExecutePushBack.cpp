#include <Army.h>
#include <ExecutePushBack.h>
#include <Game.h>
#include <MakePlayerTurn.h>
#include <PlaceOnBoard.h>
#include <PlayerLogic.h>
#include <UseTheTile.h>

using namespace std;

void
ExecutePushBack::advance_game(Game* game)
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    Coordinates pushed_tile_coords =
        board->get_neighbour(pusher_coords, push_direction);
    game_data.switch_active_player();
    Coordinates target_index = get_target_location(game);
    unique_ptr<Tile> pushed_tile =
        game_data.get_board()->pick_up_tile(pushed_tile_coords);
    board->set_tile(target_index, move(pushed_tile));
    game_data.switch_active_player();
    game->pick_next_state();
}

auto
ExecutePushBack::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    vector<Coordinates> valid_target_coords{};
    Coordinates pushed_tile_coords =
        board->get_neighbour(pusher_coords, push_direction);
    for (DIRECTION rotation : set<DIRECTION>{ N, NE, NW }) {
        DIRECTION pushed_enemy_dir =
            rotated_direction(push_direction, rotation);
        Coordinates target_coords =
            board->get_neighbour(pushed_tile_coords, pushed_enemy_dir);
        if (board->is_valid_position(target_coords) and
            not board->get_tile_ptr(target_coords)) {
            valid_target_coords.push_back(target_coords);
        }
    }

    vector<PlayerDecision> result{};
    for (const Coordinates& target : valid_target_coords) {
        PlayerDecision valid_decision{};
        valid_decision.set_tile_target_location(target.x, target.y, N);
        result.push_back(valid_decision);
    }
    return result;
}

auto
ExecutePushBack::get_target_location(Game* game) -> Coordinates
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        unique_ptr<PlayerDecision> placement_decision;
        placement_decision = game_data.get_active_player()
                                 ->get_logic()
                                 ->pick_pushed_tile_location(game);
        game_data.get_decision_manager()->add_player_decision(
            move(placement_decision));
    }
    unique_ptr<PlayerDecision> result =
        game_data.get_decision_manager()->get_player_decision();
    pair<Coordinates, DIRECTION> push_target;
    push_target = result->get_tile_target_location();
    return Coordinates(push_target.first.x, push_target.first.y);
}

auto
ExecutePushBack::clone() -> unique_ptr<GameState>
{
    return make_unique<ExecutePushBack>(
        make_pair(pusher_coords, push_direction));
}
