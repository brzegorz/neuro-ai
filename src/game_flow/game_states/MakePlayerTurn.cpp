#include <Army.h>
#include <Game.h>
#include <GameState.h>
#include <MakePlayerTurn.h>
#include <ObtainWhetherToUseInnates.h>

#include <iostream>

using namespace std;

void
MakePlayerTurn::advance_game(Game* game)
{
    GameData& game_data = game->get_data();
    PlayerResources* player_resources =
        game_data.get_active_player()->get_resources();
    make_player_draw_tiles(game);
    game_data.set_decisions_left_count(
        min(2, player_resources->get_hand_size()));
    game->set_next_state(make_unique<ObtainWhetherToUseInnates>());
}

auto MakePlayerTurn::get_valid_moves(Game * /*game*/) -> vector<PlayerDecision>
{
    throw logic_error(
        "Valid moves requested from gamestate which doesn't require input");
    return vector<PlayerDecision>{};
}

void
MakePlayerTurn::make_player_draw_tiles(Game* game)
{
    GameData& game_data = game->get_data();
    PlayerResources* player_resources =
        game_data.get_active_player()->get_resources();
    Army* active_army = player_resources->get_army();
    player_resources->receive_previous_turn_tiles();
    int present_hand_size = player_resources->get_hand_size();
    int final_hand_size = min(3, game_data.get_turn_counter() - 1);
    int tiles_to_draw_count = final_hand_size - present_hand_size;
    while (tiles_to_draw_count > 0 and not active_army->is_empty()) {
        player_resources->receive_tile(active_army->draw_tile());
        tiles_to_draw_count = tiles_to_draw_count - 1;
    }
}

auto
MakePlayerTurn::clone() -> unique_ptr<GameState>
{
    return make_unique<MakePlayerTurn>();
}
