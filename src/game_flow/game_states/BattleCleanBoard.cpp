#include <Battle.h>
#include <BattleCleanBoard.h>
#include <Game.h>
#include <GameState.h>
#include <MakePlayerTurn.h>

using namespace std;

void
BattleCleanBoard::advance_game(Game* game)
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    for (auto const& coords : board->get_tile_indexes()) {
        Tile* tile = board->get_tile_ptr(coords);
        if (tile) {
            tile->resolve_damage();
            if (tile->get_hp() < 1)
                board->kill_tile(coords);
        }
    }
    --game_data.current_battle_turn;
    if (game_data.current_battle_turn == -1)
        game_data.current_battle_turn = -2;
    if (game_data.current_battle_turn < -2)
        throw logic_error("Negative battle turn");
    game->set_next_state(make_unique<Battle>());
}

auto BattleCleanBoard::get_valid_moves(Game * /*game*/)
    -> vector<PlayerDecision>
{
    throw logic_error(
        "Valid moves requested from gamestate which doesn't require input");
    return vector<PlayerDecision>{};
}

auto
BattleCleanBoard::clone() -> unique_ptr<GameState>
{
    return make_unique<BattleCleanBoard>();
}
