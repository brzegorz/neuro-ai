#include <Game.h>
#include <GameState.h>
#include <HealSingleTile.h>
#include <PickMedicToLose.h>
#include <PlayerLogic.h>
#include <ResolveMedics.h>
#include <ResolveSingleMedic.h>

#include <algorithm>
#include <map>
#include <numeric>

using namespace std;

void
PickMedicToLose::advance_game(Game* game)
{
    GameData& game_data = game->get_data();
    Board* board = game_data.get_board();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        PlayerLogic* player_logic =
            game_data.get_active_player()->get_logic().get();
        game_data.get_decision_manager()->add_player_decision(
            player_logic->pick_medic_to_lose(game));
    }
    Coordinates tile_to_kill = game_data.get_decision_manager()
                                   ->get_player_decision()
                                   ->get_tile_target_location()
                                   .first;
    Coordinates tile_to_preserve;
    if (tile_to_kill == game_data.current_medic_coordinates) {
        tile_to_preserve = game_data.current_medic_valid_targets[0];
    } else {
        tile_to_preserve = game_data.current_medic_coordinates;
    }
    if (board->get_tile_ptr(tile_to_preserve)->is_damaged())
        board->get_tile_ptr(tile_to_preserve)->get_healed(0);
    board->kill_tile(tile_to_kill);
    game->set_next_state(make_unique<ResolveMedics>());
}

auto
PickMedicToLose::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    GameData& game_data = game->get_data();
    vector<PlayerDecision> result{};
    PlayerDecision medic1{};
    medic1.set_tile_target_location(game_data.current_medic_coordinates.x,
                                    game_data.current_medic_coordinates.y,
                                    N);
    result.push_back(medic1);

    PlayerDecision medic2{};
    Coordinates targetCoords = game_data.current_medic_valid_targets[0];
    medic2.set_tile_target_location(targetCoords.x, targetCoords.y, N);
    result.push_back(medic2);

    return result;
}

auto
PickMedicToLose::clone() -> unique_ptr<GameState>
{
    return make_unique<PickMedicToLose>();
}