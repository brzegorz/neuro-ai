#include <Colours.h>
#include <Game.h>
#include <ObtainTileToUse.h>
#include <ObtainWayOfUsingTheTile.h>
#include <PlayerLogic.h>
#include <Tile.h>

using namespace std;

void
ObtainTileToUse::advance_game(Game* game)
{
    GameData& game_data = game->get_data();
    PlayerResources* player_resources =
        game_data.get_active_player()->get_resources();
    int tile_to_use_index = get_tile_to_use_index(game);
    game_data.tile_being_used =
        player_resources->pick_tile_from_hand(tile_to_use_index);
    game->set_next_state(make_unique<ObtainWayOfUsingTheTile>());
}

auto
ObtainTileToUse::get_tile_to_use_index(Game* game) -> int
{
    GameData& game_data = game->get_data();
    if (not game_data.get_decision_manager()->player_decision_available()) {
        GameData& game_data = game->get_data();
        PlayerLogic* player_logic =
            game_data.get_active_player()->get_logic().get();
        if (game_data.gui) {
            game_data.gui->print_board(*game_data.get_board());
            game_data.gui->print_player_hand(game_data.get_active_player());
        }
        game_data.get_decision_manager()->add_player_decision(
            player_logic->pick_tile_to_use(game));
    }
    int tile_to_use_index = game_data.get_decision_manager()
                                ->get_player_decision()
                                ->get_tile_to_use_index();
    return tile_to_use_index;
}

auto
ObtainTileToUse::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    int player_hand_size =
        game->get_data().get_active_player()->get_resources()->get_hand_size();
    vector<PlayerDecision> tiles_on_hand_indexes{};
    for (int i = 0; i < player_hand_size; i++) {
        PlayerDecision possible_tile_index{};
        possible_tile_index.set_tile_to_use_index(i);
        tiles_on_hand_indexes.push_back(possible_tile_index);
    }
    return tiles_on_hand_indexes;
}

auto
ObtainTileToUse::clone() -> unique_ptr<GameState>
{
    return make_unique<ObtainTileToUse>();
}
