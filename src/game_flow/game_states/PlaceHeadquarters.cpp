#include <Army.h>
#include <Game.h>
#include <MakePlayerTurn.h>
#include <PlaceHeadquarters.h>
#include <PlaceOnBoard.h>

using namespace std;

void
PlaceHeadquarters::advance_game(Game* game)
{
    GameData& game_data = game->get_data();
    Player* active_player = game_data.get_active_player();
    if (game_data.gui)
        game_data.gui->print_board(*game_data.get_board());
    if (not game_data.tile_being_used) {
        unique_ptr<Tile> headquarter =
            active_player->get_resources()->get_army()->get_headquarter();
        game_data.tile_being_used = move(headquarter);
    }
    game_data.tile_being_used->use(game);
    game_data.next_turn();
    if (game_data.get_turn_counter() < 2)
        game->set_next_state(make_unique<PlaceHeadquarters>());
    else
        game->set_next_state(make_unique<MakePlayerTurn>());
}

auto
PlaceHeadquarters::get_valid_moves(Game* game) -> vector<PlayerDecision>
{
    return PlaceOnBoard{}.get_valid_moves(game);
}

auto
PlaceHeadquarters::clone() -> unique_ptr<GameState>
{
    return make_unique<PlaceHeadquarters>();
}
