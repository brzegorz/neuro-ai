#include <Coordinates.h>

using namespace std;

auto
operator<<(ostream& os, const Coordinates& m) -> ostream&
{
    return os << m.x << ", " << m.y;
}
