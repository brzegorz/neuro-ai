#include <Directions.h>

auto
opposite_direction(DIRECTION base_direction) -> DIRECTION
{
    return (DIRECTION)((base_direction + 3) % 6);
}

auto
rotated_direction(DIRECTION base_direction, DIRECTION rotation) -> DIRECTION
{
    return (DIRECTION)((base_direction + rotation) % 6);
}
