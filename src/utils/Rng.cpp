#include <Rng.h>
using namespace std;

unsigned Rng::seed = chrono::system_clock::now().time_since_epoch().count();
// unsigned Rng::seed = 3147600176;
mt19937 Rng::rng{ Rng::seed };

auto
Rng::random_int(int min, int max) -> int
{
    if (min == max)
        return min;
    if (max < min)
        throw logic_error("Bad interval for random int generation");
    uniform_int_distribution<> uni(min, max);
    return uni(rng);
}
