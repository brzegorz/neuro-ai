#include "Coordinates.h"
#include <Board.h>
#include <Tile.h>

#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>
#include <stdexcept>
#include <string>

using namespace std;

Board::Board()
    : vectorized_board{ nullptr }
{
    create_standard_board();
}

Board::Board(const Board& other)
    : width{ other.width }
    , height{ other.height }
    , passive_bonus_sources{ other.passive_bonus_sources }
    , medic_sources{ other.medic_sources }
    , tile_indexes{ other.tile_indexes }
    , empty_tile_indexes{ other.empty_tile_indexes }
    , occupied_tile_indexes{ other.occupied_tile_indexes }
    , hq_coords{ other.hq_coords }
    , vectorized_board{ nullptr }
{
    for (auto const& coords : tile_indexes) {
        tiles[coords] = nullptr;
    }
    for (auto const& coords : get_occupied_indexes()) {
        Tile* tile_at_coords = other.get_tile_ptr(coords);
        unique_ptr<Tile> tile_copy = make_unique<Tile>(*tile_at_coords);
        tiles.at(coords) = move(tile_copy);
    }
}

Board::Board(TYPE type)
{
    switch (type) {
        case STANDARD:
            create_standard_board();
            break;
        case HAND:
            create_hand_board();
            break;
        default:
            throw invalid_argument("Board TYPE in constructor");
    }
}

Board::~Board()
{
    unordered_set<Coordinates> occupied_coords{};
    occupied_coords.insert(get_occupied_indexes().begin(), get_occupied_indexes().end());
    for (const Coordinates coords : occupied_coords)
    {
        kill_tile((coords));
    }
}

void
Board::create_standard_board()
{
    this->width = 5;
    this->height = 9;
    bool is_valid_hex = false;
    bool is_within_board_borders = false;
    Coordinates current_coords;
    for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
            is_valid_hex = false;
            is_within_board_borders = false;
            if ((x + y) % 2 == 0)
                is_valid_hex = true;
            if ((x == 0 or x == 4) and y > 1 and y < 7)
                is_within_board_borders = true;
            if ((x == 1 or x == 3) and y > 0 and y < 9)
                is_within_board_borders = true;
            if (x == 2)
                is_within_board_borders = true;
            if (is_valid_hex and is_within_board_borders) {
                current_coords = Coordinates(x, y);
                tiles[current_coords] = nullptr; // add element to map
                passive_bonus_sources[current_coords] =
                    unordered_set<DIRECTION>();
                medic_sources[current_coords] = unordered_set<DIRECTION>();
                tile_indexes.insert(current_coords);
                empty_tile_indexes.insert(current_coords);
            }
        }
    }
}

void
Board::create_hand_board()
{
    this->width = 5;
    this->height = 1;
    array<int, 3> xs{ { 0, 2, 4 } };
    for (int x : xs) {
        tiles[Coordinates(x, 0)] = nullptr; // add element to map
        tile_indexes.insert(Coordinates(x, 0));
        empty_tile_indexes.insert(Coordinates(x, 0));
        passive_bonus_sources[Coordinates(x, 0)] = unordered_set<DIRECTION>();
        medic_sources[Coordinates(x, 0)] = unordered_set<DIRECTION>();
    }
}

auto
Board::get_tile_indexes() const -> const unordered_set<Coordinates>&
{
    return tile_indexes;
}

auto
Board::get_empty_indexes() const -> const unordered_set<Coordinates>&
{
    return empty_tile_indexes;
}

auto
Board::get_occupied_indexes() const -> const unordered_set<Coordinates>&
{
    return occupied_tile_indexes;
}

auto
Board::get_hq_coords() const -> const unordered_set<Coordinates>&
{
    return hq_coords;
}

auto
Board::get_players_indexes(const Player* owner) const
    -> const unordered_set<Coordinates>
{
    unordered_set<Coordinates> result{};
    for (auto const& coords : get_occupied_indexes()) {
        if (*(get_tile_ptr(coords)->get_owner()) == *owner) {
            result.insert(coords);
        }
    }
    return result;
}

auto
Board::is_full() const -> bool
{
    return empty_tile_indexes.empty();
}

auto
Board::get_neighbours(const Coordinates& coords) const -> set<Coordinates>
{
    set<Coordinates> neighbours;
    for (DIRECTION neighbour_direction : all_directions) {
        Coordinates neighbour = get_neighbour(coords, neighbour_direction);
        if (is_valid_position(neighbour)) {
            neighbours.insert(neighbour);
        }
    }
    return neighbours;
}

auto
Board::get_empty_neighbours(const Coordinates& coords) const -> set<Coordinates>
{
    set<Coordinates> empty_neighbours;
    for (Coordinates neighbour : get_neighbours(coords)) {
        if (empty_tile_indexes.count(neighbour) == 1)
            empty_neighbours.insert(neighbour);
    }
    return empty_neighbours;
}

auto
Board::get_neighbour(const Coordinates& coords, DIRECTION dir) -> Coordinates
{
    switch (dir) {
        case N:
            return Coordinates(coords.x, coords.y - 2);
        case NE:
            return Coordinates(coords.x + 1, coords.y - 1);
        case SE:
            return Coordinates(coords.x + 1, coords.y + 1);
        case S:
            return Coordinates(coords.x, coords.y + 2);
        case SW:
            return Coordinates(coords.x - 1, coords.y + 1);
        case NW:
            return Coordinates(coords.x - 1, coords.y - 1);
        default:
            throw invalid_argument("Invalid direction specified");
    }
}

auto
Board::is_valid_position(const Coordinates& coords) const -> bool
{
    return (tiles.count(coords) == 1); // element exists in map
}

auto
Board::get_tile_ptr(const Coordinates& coords) const -> Tile*
{
    return tiles.at(coords).get();
}

void
Board::set_tile(const Coordinates& coords, unique_ptr<Tile> new_tile)
{
    if (new_tile->is_headquarter)
        hq_coords.insert(coords);
    empty_tile_indexes.erase(coords);
    occupied_tile_indexes.insert(coords);
    if (vectorized_board)
        vectorized_board->add_tile(coords, new_tile.get());
    tiles.at(coords) = move(new_tile);
    // Copying passive directions, as they can change in the loop
    const unordered_set<DIRECTION> passive_directions =
        passive_bonus_sources.at(coords);
    for (const DIRECTION& bonus_source_dir : passive_directions) {
        DIRECTION bonus_direction = opposite_direction(bonus_source_dir);
        Coordinates bonus_source_coords;
        bonus_source_coords = get_neighbour(coords, bonus_source_dir);
        tiles.at(bonus_source_coords)
            ->apply_passive_bonuses(
                *this, bonus_source_coords, bonus_direction);
    }
    tiles.at(coords)->apply_passive_bonuses(*this, coords);
}

void
Board::kill_tile(Coordinates coords)
{
    pick_up_tile(coords);
}

auto
Board::pick_up_tile(const Coordinates& coords) -> unique_ptr<Tile>
{
    empty_tile_indexes.insert(coords);
    occupied_tile_indexes.erase(coords);
    hq_coords.erase(coords);
    for (DIRECTION bonus_source_dir : passive_bonus_sources.at(coords)) {
        DIRECTION bonus_direction = opposite_direction(bonus_source_dir);
        Coordinates bonus_source_coords;
        bonus_source_coords = get_neighbour(coords, bonus_source_dir);
        tiles.at(bonus_source_coords)
            ->reverse_passive_bonuses(
                *this, bonus_source_coords, bonus_direction);
    }
    tiles.at(coords)->reverse_passive_bonuses(*this, coords);
    for (DIRECTION bonus_source_dir : passive_bonus_sources.at(coords)) {
        DIRECTION bonus_direction = opposite_direction(bonus_source_dir);
        Coordinates bonus_source_coords;
        bonus_source_coords = get_neighbour(coords, bonus_source_dir);
        tiles.at(bonus_source_coords)
            ->reverse_passive_bonuses(
                *this, bonus_source_coords, bonus_direction);
    }
    if (vectorized_board)
        vectorized_board->delete_tile(tiles.at(coords).get());
    return move(tiles.at(coords));
}

void
Board::rotate_tile(Coordinates coords, const DIRECTION& new_direction)
{
    unique_ptr<Tile> rotated_tile = pick_up_tile(coords);
    rotated_tile->rotate(new_direction);
    set_tile(coords, move(rotated_tile));
    if (vectorized_board) {
        vectorized_board->delete_tile(tiles.at(coords).get());
        vectorized_board->add_tile(coords, tiles.at(coords).get());
    }
}

void
Board::damage_tile_outside_battle(Coordinates coords, int damage)
{
    Tile* tile_to_damage = get_tile_ptr(coords);
    if (not tile_to_damage)
        return;
    tile_to_damage->receive_damage_in_battle(damage, nullptr);
    tile_to_damage->resolve_damage();
    if (vectorized_board) {
        vectorized_board->delete_tile(tile_to_damage);
        vectorized_board->add_tile(coords, tile_to_damage);
    }
    if (tile_to_damage->get_hp() < 1)
        kill_tile(coords);
}

void
Board::add_passive_bonus_source(const Coordinates& coords,
                                DIRECTION source_direction)
{
    passive_bonus_sources.at(coords).insert(source_direction);
}

void
Board::remove_passive_bonus_source(const Coordinates& coords,
                                   DIRECTION source_direction)
{
    passive_bonus_sources.at(coords).erase(source_direction);
}

void
Board::add_medic_source(const Coordinates& coords, DIRECTION source_direction)
{
    medic_sources.at(coords).insert(source_direction);
}

void
Board::remove_medic_source(const Coordinates& coords,
                           DIRECTION source_direction)
{
    medic_sources.at(coords).erase(source_direction);
}

auto
Board::get_medic_sources() const
    -> const std::unordered_map<Coordinates, std::unordered_set<DIRECTION>>&
{
    return medic_sources;
}

auto
Board::get_vectorized_board() const -> const VectorizedBoard&
{
    return *vectorized_board;
}

void
Board::set_vectorized_board(unique_ptr<VectorizedBoard> board)
{
    vectorized_board = move(board);
}

auto
Board::operator==(const Board& other) const -> bool
{
    if (this->width != other.width or this->height != other.height)
        return false;
    if (this->get_empty_indexes() != other.get_empty_indexes())
        return false;
    Tile* this_tile;
    Tile* other_tile;
    for (auto coords : get_occupied_indexes()) {
        if (not other.is_valid_position(coords))
            return false;
        this_tile = this->get_tile_ptr(coords);
        other_tile = other.get_tile_ptr(coords);
        if (*this_tile != *other_tile)
            return false;
    }
    return true;
}

auto
Board::operator!=(const Board& other) const -> bool
{
    return !(*this == other);
}
