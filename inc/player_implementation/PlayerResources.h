#ifndef PLAYER_RESOURCES_CLASS
#define PLAYER_RESOURCES_CLASS

#include <Army.h>
#include <memory>
#include <vector>

class Army;
class Tile;

class PlayerResources
{
  public:
    PlayerResources();
    PlayerResources(const PlayerResources& other);
    // Hand manipulations
    void receive_tile(std::unique_ptr<Tile> drawn_tile);
    auto pick_tile_from_hand(int index) -> std::unique_ptr<Tile>;
    void keep_tile_for_next_turn(std::unique_ptr<Tile> tile_to_keep);
    void receive_previous_turn_tiles();
    void erase_hand();
    // Getters/setters
    [[nodiscard]] auto get_hand_size() const -> int;
    [[nodiscard]] auto get_army() const -> Army*;
    void set_army(std::unique_ptr<Army> a);
    auto operator==(const PlayerResources& other) const -> bool;
    auto operator!=(const PlayerResources& other) const -> bool;

  protected:
    std::unique_ptr<Army> army;
    std::vector<std::unique_ptr<Tile>> hand;
    std::vector<std::unique_ptr<Tile>> next_turn_tiles;
    [[nodiscard]] auto compare_hands(const PlayerResources& other) const
        -> bool;
    [[nodiscard]] auto compare_next_turn_tiles(
        const PlayerResources& other) const -> bool;
};

#endif
