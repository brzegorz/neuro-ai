#ifndef MCTS_PLAYER_CLASS
#define MCTS_PLAYER_CLASS

#include <MonteCarloTreeSearch.h>
#include <PlayerLogic.h>

#include <memory>

class MctsPlayerLogic : public PlayerLogic
{
  public:
    MctsPlayerLogic();
    MctsPlayerLogic(const MctsPlayerLogic& other);
    auto pick_tile_to_use(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_how_to_use_tile(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_tile_target_location(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_movement_source_and_target(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_bomb_target(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_sniper_target(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_push_source_and_direction(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_pushed_tile_location(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_innate_movement_target(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_whether_to_use_innates(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_tile_with_innate(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_tile_to_heal(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_attack_to_heal(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_medic_to_use(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_medic_to_lose(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<PlayerLogic> override;

  private:
    MonteCarloTreeSearch mcts;
    auto make_any_decision(Game* game, const std::string& information)
        -> std::unique_ptr<PlayerDecision>;
};

#endif
