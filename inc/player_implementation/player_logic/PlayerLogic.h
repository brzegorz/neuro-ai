#ifndef PLAYERLOGIC_CLASS
#define PLAYERLOGIC_CLASS

#include <memory>

class Game;
class PlayerDecision;

class PlayerLogic
{
  public:
    virtual auto pick_tile_to_use(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_how_to_use_tile(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_tile_target_location(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_movement_source_and_target(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_bomb_target(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_sniper_target(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_push_source_and_direction(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_pushed_tile_location(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_innate_movement_target(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_whether_to_use_innates(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_tile_with_innate(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_tile_to_heal(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_attack_to_heal(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_medic_to_use(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    virtual auto pick_medic_to_lose(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    [[nodiscard]] virtual auto clone() const
        -> std::unique_ptr<PlayerLogic> = 0;
    virtual ~PlayerLogic() = default;
};

#endif
