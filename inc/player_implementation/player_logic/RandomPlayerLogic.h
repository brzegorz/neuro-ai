#ifndef RANDOM_PLAYER_CLASS
#define RANDOM_PLAYER_CLASS

#include <ArmyBuilder.h>
#include <PlayerLogic.h>

#include <memory>
#include <random>

class RandomPlayerLogic : public PlayerLogic
{
  public:
    auto pick_tile_to_use(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_how_to_use_tile(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_tile_target_location(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_movement_source_and_target(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_bomb_target(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_sniper_target(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_push_source_and_direction(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_pushed_tile_location(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_innate_movement_target(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_whether_to_use_innates(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_tile_with_innate(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_tile_to_heal(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_attack_to_heal(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_medic_to_use(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    auto pick_medic_to_lose(Game* game)
        -> std::unique_ptr<PlayerDecision> override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<PlayerLogic> override;

  private:
    static std::discrete_distribution<int> way_of_usage_dist;
    static auto slow_universal_choice(Game* game)
        -> std::unique_ptr<PlayerDecision>;
};

#endif
