#ifndef PLAYER_CLASS
#define PLAYER_CLASS

#include <PlayerResources.h>

#include <memory>

class PlayerLogic;

class Player
{
  public:
    Player();
    Player(std::string name, std::string colour);
    Player(const Player& other);
    const int id;
    void set_logic(std::shared_ptr<PlayerLogic> new_logic);
    void set_name(std::string new_name) { name = new_name; };
    void set_colour(std::string new_colour) { colour = new_colour; };
    auto get_resources() -> PlayerResources* { return &resources; };
    auto get_logic() -> std::shared_ptr<PlayerLogic> { return logic; };
    [[nodiscard]] auto get_name() const -> const std::string { return name; };
    [[nodiscard]] auto get_colour() const -> const std::string
    {
        return colour;
    };
    [[nodiscard]] auto has_double_movement() const -> bool
    {
        return double_movement_bonus_count > 0;
    }
    void give_double_movement() { ++double_movement_bonus_count; };
    void remove_double_movement() { --double_movement_bonus_count; };
    auto operator==(const Player& other) const -> bool;
    auto operator!=(const Player& other) const -> bool;

  protected:
    std::string name;
    std::string colour;
    PlayerResources resources;
    std::shared_ptr<PlayerLogic> logic;
    int double_movement_bonus_count;
    static int count;
};

#endif
