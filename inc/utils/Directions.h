#ifndef DIRECTIONS_ENUM
#define DIRECTIONS_ENUM

#include <array>

enum DIRECTION
{
    N,
    NE,
    SE,
    S,
    SW,
    NW
};

auto
opposite_direction(DIRECTION base_direction) -> DIRECTION;
auto
rotated_direction(DIRECTION base_direction, DIRECTION rotation) -> DIRECTION;
const std::array<DIRECTION, 6> all_directions{ N, NE, SE, S, SW, NW };

#endif
