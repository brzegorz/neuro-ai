#ifndef POINTER_DEREFERENCE_COMPARATOR
#define POINTER_DEREFERENCE_COMPARATOR

template<typename T>
struct pointer_dereference_comparator
{
    auto operator()(const T& a, const T& b) const -> bool { return *a < *b; }
};

#endif
