#ifndef COORDINATES_CLASS
#define COORDINATES_CLASS

#include <iostream>
#include <memory>
#include <vector>

class Coordinates
{
  public:
    Coordinates()
        : x{ -1 }
        , y{ -1 } {};
    Coordinates(int x, int y)
        : x{ x }
        , y{ y } {};
    int x;
    int y;
    auto operator==(const Coordinates& other) const -> bool
    {
        return x == other.x and y == other.y;
    };
    auto operator!=(const Coordinates& other) const -> bool
    {
        return not(*this == other);
    };
    auto operator<(const Coordinates& other) const -> bool
    {
        if (x < other.x)
            return true;
        if (x == other.x and y < other.y)
            return true;
        return false;
    };
};

auto
operator<<(std::ostream& os, const Coordinates& m) -> std::ostream&;

namespace std {
template<>
struct hash<Coordinates>
{
    auto operator()(const Coordinates& p) const -> size_t
    {
        // We know that x is in <0, 5> and y in <0, 9>.
        // Thus, x*16 >> y, unless x == 0
        return std::hash<int>{}((p.x << 4) | p.y);
    }
};
} // namespace std

#endif
