#ifndef TILE_WAYS_OF_USAGE_ENUM
#define TILE_WAYS_OF_USAGE_ENUM

enum TILE_USAGE_WAY
{
    USE,
    KEEP,
    DISCARD
};

#endif
