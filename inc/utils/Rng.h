#ifndef RNG_UTIL
#define RNG_UTIL

#include <PlayerLogic.h>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <random>
class Rng
{
  public:
    static unsigned seed;
    static std::mt19937 rng;
    static auto random_int(int min, int max) -> int;
};
#endif
