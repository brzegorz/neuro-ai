#ifndef TILE_PARAMS_GUI
#define TILE_PARAMS_GUI

struct tile_parameters
{
    int tile_width;
    int tile_height;
    int start_x;
    int start_y;
    std::string colour;
};

#endif
