#ifndef COLOURS_STRUCT
#define COLOURS_STRUCT

#include <string>

struct colours
{
    std::string NEUTRAL;
    std::string WEB;
    std::string ARMOUR;
    std::string YELLOW;
    std::string RED;
    std::string BLUE;
    std::string GREEN;
};

static const colours COLOURS = {
    "\033[0;0m",          // neutral
    "\033[1;9m\033[1;2m", // web
    "\033[1;37m",         // armour
    "\033[1;33m",         // yellow
    "\033[1;31m",         // red
    "\033[1;34m",         // blue
    "\033[1;32m"          // green

};

#endif
