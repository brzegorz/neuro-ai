#ifndef GAMESTATE_CLASS
#define GAMESTATE_CLASS

#include <Board.h>
#include <Gui.h>
#include <Player.h>
#include <PlayerDecisionManager.h>
#include <Tile.h>

#include <array>
#include <memory>
#include <queue>

class GameData
{
    friend class Battle;

  public:
    GameData();
    GameData(const GameData& other);
    // Once the last turn begins(is_final_turn_in_place)
    //      - Player makes his final move(is_one_player_out_of_tiles)
    //      - A battle is executed(is_final_battle_over)
    //      - The game is over(is_finished)
    bool is_one_player_out_of_tiles;
    bool is_final_turn_in_place;
    bool is_final_battle_over;
    bool is_game_finished;
    int current_battle_turn;
    static int games_created_count;
    void next_turn();
    auto get_active_player() const -> Player*;
    auto get_previous_player() const -> Player*;
    void switch_active_player();
    void shuffle_players();
    void shuffle_armies();
    auto is_endgame_criteria_fulfilled() -> bool;
    // Getters
    auto get_board() -> Board*;
    auto get_winner() const -> Player*;
    auto get_decision_manager() -> PlayerDecisionManager*;
    auto get_way_of_using_current_tile() const -> const TILE_USAGE_WAY&;
    auto get_turn_counter() const -> const int&;
    auto get_decisions_left_count() const -> const int&;
    // Setters
    void set_way_of_using_current_tile(TILE_USAGE_WAY usage);
    void set_decisions_left_count(int new_count);
    void decrement_decisions_left_count();
    void set_players(std::unique_ptr<Player> player1,
                     std::unique_ptr<Player> player2);
    void assign_default_armies();
    void update_winner();
    auto operator==(const GameData& other) const -> bool;
    auto operator!=(const GameData& other) const -> bool;
    // Kind of globals
    std::unique_ptr<Gui> gui;
    std::unique_ptr<Tile> tile_being_used;
    Coordinates tile_using_innate_coordinates;
    Coordinates current_medic_coordinates;
    std::vector<Coordinates> current_target_valid_medics;
    std::vector<Coordinates> current_medic_valid_targets;
    Coordinates tile_to_heal_coordinates;

  private:
    Board board;
    static const int players_count = 2;
    std::array<std::unique_ptr<Player>, players_count> players;
    int active_player_index;
    PlayerDecisionManager decision_manager;
    TILE_USAGE_WAY way_of_using_the_tile;
    int decisions_left_count;
    int turn_count; // Increments after one player makes all his decisions
    std::vector<std::pair<Player*, int>> hp_of_headquarters;
    Player* winner;
    void update_hp_of_headquarters();
};

#endif
