#ifndef ARMY_CLASS
#define ARMY_CLASS

#include <Tile.h>

#include <string>
#include <unordered_set>
#include <vector>

class Army
{
  public:
    Army();
    Army(std::unique_ptr<Tile> headquarter,
         std::vector<std::unique_ptr<Tile>>& tiles);
    Army(const Army& other);
    void shuffle();
    auto is_empty() const -> bool;
    auto draw_tile() -> std::unique_ptr<Tile>;
    auto get_headquarter() -> std::unique_ptr<Tile>;
    void set_headquarter(std::unique_ptr<Tile> headquarter);
    auto get_tile_names() const -> const std::unordered_set<std::string>&;
    auto operator==(const Army& other) const -> bool;
    auto operator!=(const Army& other) const -> bool;

  private:
    std::unordered_set<std::string> tile_names{};
    std::vector<std::unique_ptr<Tile>> tiles;
    std::unique_ptr<Tile> headquarter;
    void assure_unique_tile_name(Tile* tile);
};

#endif
