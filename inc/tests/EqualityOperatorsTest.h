#ifndef EqualityOperatorTestSuite
#define EqualityOperatorTestSuite

#include <Test.h>

class EqualityOperatorsTest : public Test
{
  protected:
    auto test_function(bool verbose) -> bool override;
    auto get_name() -> std::string override
    {
        return "EQUALITY OPERATORS TESTS";
    };

  private:
    static auto shooting_eq_op_test(bool verbose) -> bool;
    static auto usage_func_eq_op_test(bool verbose) -> bool;
    static auto bonus_attack_eq_op_test(bool verbose) -> bool;
    static auto tile_eq_op_test(bool verbose) -> bool;
    static auto board_eq_op_test(bool verbose) -> bool;
    static auto game_data_eq_op_test(bool verbose) -> bool;
};

#endif
