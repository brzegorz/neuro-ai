#ifndef CloningTestSuite
#define CloningTestSuite

#include <Test.h>

class CloningTest : public Test
{
  protected:
    auto test_function(bool verbose) -> bool override;
    auto get_name() -> std::string override { return "CLONE FUNCTION TESTS"; };

  private:
    static auto shooting_action_clone_test(bool verbose) -> bool;
    static auto usage_func_clone_test(bool verbose) -> bool;
    static auto tile_clone_test(bool verbose) -> bool;
    static auto board_clone_test(bool verbose) -> bool;
    static auto game_data_clone_test(bool verbose) -> bool;
    static auto game_clone_test(bool verbose) -> bool;
};

#endif
