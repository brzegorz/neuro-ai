#ifndef MedicTestSuite
#define MedicTestSuite

#include <Test.h>

class MedicTest : public Test
{
  protected:
    auto test_function(bool verbose) -> bool override;
    auto get_name() -> std::string override { return "MEDIC FUNCTION TESTS"; };

  private:
    static auto saves_one_unit(bool verbose) -> bool;
    static auto player_can_choose_an_attack_to_heal(bool verbose) -> bool;
    static auto medic_released_from_web(bool verbose) -> bool;
    static auto one_medic_many_units(bool verbose) -> bool;
    static auto many_medics_one_unit(bool verbose) -> bool;
    static auto mutualy_healing_medics(bool verbose) -> bool;
    static auto does_not_save_unit_when_scoper_used(bool verbose) -> bool;
    static auto saves_enemy_unit_when_scoper_used(bool verbose) -> bool;
};

#endif
