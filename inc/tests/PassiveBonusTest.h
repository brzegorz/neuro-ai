#ifndef PassiveBonusTestSuite
#define PassiveBonusTestSuite

#include <Test.h>

class PassiveBonusTest : public Test
{
  protected:
    auto test_function(bool verbose) -> bool override;
    auto get_name() -> std::string override { return "PASSIVE BONUSES TESTS"; };

  private:
    static auto bonus_gets_applied(bool verbose) -> bool;
    static auto bonus_gets_applied2(bool verbose) -> bool;
    static auto bonus_gets_removed(bool verbose) -> bool;
    static auto bonus_gets_moved(bool verbose) -> bool;
    static auto bonus_gets_rotated(bool verbose) -> bool;
    static auto tiles_get_attacked(bool verbose) -> bool;
    static auto malus_initiative_zero(bool verbose) -> bool;
    static auto set_bonus_next_to_scoper(bool verbose) -> bool;
    static auto net_unit(bool verbose) -> bool;
    static auto net_the_net(bool verbose) -> bool;
    static auto net_mutual(bool verbose) -> bool;
    static auto net_scoper_in_last_turn(bool verbose) -> bool;
    static auto recon_center_defaults_to_false(bool verbose) -> bool;
    static auto recon_center_gets_applied(bool verbose) -> bool;
    static auto recon_center_gets_removed(bool verbose) -> bool;
};

#endif
