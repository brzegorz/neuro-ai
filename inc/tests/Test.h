#ifndef TEST_BASE_CLASS
#define TEST_BASE_CLASS

#include <Game.h>

#include <string>

class Game;

class Test
{
  public:
    virtual ~Test() = default;
    void make_tests();
    static void print_ok_message(const std::string& message);
    static void print_error_message(const std::string& message);

  protected:
    virtual auto test_function(bool verbose) -> bool = 0;
    virtual auto get_name() -> std::string = 0;
    static auto single_test(bool (*test_func)(bool verbose), bool verbose)
        -> bool;
    static auto prepare_basic_game() -> Game;
    static const int ARMY_SIZE = 5;
};

#endif
