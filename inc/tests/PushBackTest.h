#ifndef PushBackTestSuite
#define PushBackTestSuite

#include <Test.h>

class PushBackTest : public Test
{
  protected:
    auto test_function(bool verbose) -> bool override;
    auto get_name() -> std::string override
    {
        return "PUSH BACK FUNCTION TESTS";
    };

  private:
    static auto lonely_tile_unusubale(bool verbose) -> bool;
    static auto invalid_enemy_untargetable(bool verbose) -> bool;
    static auto valid_enemy_targetable(bool verbose) -> bool;
    static auto proper_valid_decisions(bool verbose) -> bool;
};

#endif
