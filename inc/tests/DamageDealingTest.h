#ifndef DamageDealingTestSuite
#define DamageDealingTestSuite

#include <Test.h>

class DamageDealingTest : public Test
{
  protected:
    auto test_function(bool verbose) -> bool override;
    auto get_name() -> std::string override { return "DAMAGE DEALING TESTS"; };

  private:
    static auto damage_is_applied_to_enemy(bool verbose) -> bool;
    static auto armour_blocks_weak_attacks(bool verbose) -> bool;
    static auto armour_weakens_strong_attacks(bool verbose) -> bool;
    static auto hqs_dont_damage_hqs(bool verbose) -> bool;
    static auto sniper_doesnt_damage_hqs(bool verbose) -> bool;
    static auto bomb_works_properly(bool verbose) -> bool;
};

#endif
