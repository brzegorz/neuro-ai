#ifndef EndgameTestSuite
#define EndgameTestSuite

#include <Test.h>

class EndgameTest : public Test
{
  protected:
    auto test_function(bool verbose) -> bool override;
    auto get_name() -> std::string override
    {
        return "ENDGAME VALIDITY TESTS";
    };

  private:
    static auto out_of_tiles_with_winner(bool verbose) -> bool;
    static void out_of_tiles_with_winner_prepare_moves(Game* game);

    static auto out_of_tiles_tie(bool verbose) -> bool;
    static void out_of_tiles_tie_prepare_moves(Game* game);

    static auto HQ_destroyed_with_winner(bool verbose) -> bool;

    static auto HQ_destroyed_both(bool verbose) -> bool;
    static void HQ_destroyed_both_prepare_moves(Game* game);

    static auto out_of_tiles_board_full(bool verbose) -> bool;
    static void out_of_tiles_board_full_prepare_moves(Game* game);

    static auto make_a_weak_HQ(Player* owner) -> std::unique_ptr<Tile>;
};

#endif
