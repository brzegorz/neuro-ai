#ifndef SIMPLE_BATTLE_TEST
#define SIMPLE_BATTLE_TEST

#include <Test.h>

class SimpleBattleTest : public Test
{
  protected:
    auto test_function(bool verbose) -> bool override;
    auto get_name() -> std::string override { return "SIMPLE_BATTLE_TEST"; };
};

#endif
