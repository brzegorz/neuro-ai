#ifndef MCTS_NODE
#define MCTS_NODE

#include <Game.h>
#include <PlayerDecision.h>
#include <PointerDereferenceComparator.h>

#include <map>
#include <memory>

class Node
{
  public:
    Node(const Game& game);
    Node(const Game& game, PlayerDecision& next_decision);
    Node(const Node& other);
    auto traverse() -> Node*;
    auto expand() -> Node*;
    auto playout() -> std::pair<Player*, int>;
    void backpropagate(Player* winner, int hp_advantage, Node* root);
    [[nodiscard]] auto get_best_move() const -> PlayerDecision;
    [[nodiscard]] auto get_visits_count() const -> int;
    [[nodiscard]] auto get_children_count() const -> int;
    auto get_child(const PlayerDecision& decision) -> std::unique_ptr<Node>;
    void set_parent(Node* parent);
    void print_children(int level, int depth);
    void print_node();
    auto operator<(const Node& other) const -> bool;

  private:
    std::unique_ptr<Game> game;
    std::string active_player_name;
    bool is_game_finished;
    int wins;
    int visits;
    double UCT;
    Node* parent{};
    std::map<PlayerDecision, std::unique_ptr<Node>> children;
    std::vector<PlayerDecision> possible_moves;
    void update(Player* winner, int hp_advantage);
    [[nodiscard]] auto is_fully_expanded() const -> bool;
    [[nodiscard]] auto get_highest_UCT_child() const -> Node*;
    [[nodiscard]] auto get_UCT() const -> double;
    [[nodiscard]] auto get_wins() const -> int { return wins; };
    static auto get_winner_with_hp_advantage(GameData& game_data)
        -> std::pair<Player*, int>;
};

#endif
