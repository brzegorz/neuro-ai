#ifndef RandomizedUct_INTERFACE
#define RandomizedUct_INTERFACE

#include <Army.h>
#include <DecisionMaker.h>
#include <RandomizedUctNode.h>
#include <PlayerDecisionManager.h>
#include <Tile.h>

#include <memory>
#include <queue>

class PlayerDecision;

class RandomizedUct : public DecisionMaker
{
  public:
    RandomizedUct();
    RandomizedUct(const RandomizedUct& other);
    auto get_decision(Game* game) -> std::unique_ptr<PlayerDecision> override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<DecisionMaker> override;
  private:
    void prepare_decision(Game* game);
    void find_root(Game* game);
    void single_iteration();
    std::unique_ptr<RandomizedUctNode> root;
    PlayerDecisionManager moves_queue;
    int prev_moves_archive_size{ 0 };
};

#endif
