#ifndef EnsembleUct_INTERFACE
#define EnsembleUct_INTERFACE

#include <Army.h>
#include <DecisionMaker.h>
#include <UctNode.h>
#include <PlayerDecisionManager.h>
#include <Tile.h>

#include <memory>
#include <queue>

class PlayerDecision;

class EnsembleUct : public DecisionMaker
{
  public:
    EnsembleUct();
    EnsembleUct(const EnsembleUct& other);
    auto get_decision(Game* game) -> std::unique_ptr<PlayerDecision> override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<DecisionMaker> override;
  private:
    const int n_trees = 4;
    void prepare_decision(Game* game);
    void find_root(Game* game);
    void single_iteration(UctNode* root);
    std::array<std::unique_ptr<UctNode>, 4> roots;
    PlayerDecisionManager moves_queue;
    int prev_moves_archive_size{ 0 };
};

#endif
