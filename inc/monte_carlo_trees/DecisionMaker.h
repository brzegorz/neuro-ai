#ifndef DECISION_MAKER_INTERFACE
#define DECISION_MAKER_INTERFACE

#include <memory>

class Game;
class PlayerDecision;

class DecisionMaker
{
  public:
    virtual auto get_decision(Game* game)
        -> std::unique_ptr<PlayerDecision> = 0;
    [[nodiscard]] virtual auto clone() const
        -> std::unique_ptr<DecisionMaker> = 0;
};

#endif
