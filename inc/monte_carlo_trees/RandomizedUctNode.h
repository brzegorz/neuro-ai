#ifndef RANDOMIZED_UCT_NODE
#define RANDOMIZED_UCT_NODE

#include <Game.h>
#include <PlayerDecision.h>
#include <PointerDereferenceComparator.h>

#include <map>
#include <vector>
#include <memory>

class RandomizedUctNode
{
  public:
    RandomizedUctNode(const Game& game);
    RandomizedUctNode(const Game& game, PlayerDecision& next_decision);
    RandomizedUctNode(const RandomizedUctNode& other);
    auto traverse() -> RandomizedUctNode*;
    auto expand() -> RandomizedUctNode*;
    auto playout() -> std::pair<Player*, int>;
    void backpropagate(Player* winner, int hp_advantage, RandomizedUctNode* root);
    [[nodiscard]] auto get_best_move() const -> PlayerDecision;
    [[nodiscard]] auto get_visits_counts_per_move() const -> std::map<PlayerDecision, int>;
    [[nodiscard]] auto get_visits_count() const -> int;
    [[nodiscard]] auto get_children_count() const -> int;
    auto get_child(const PlayerDecision& decision) -> std::unique_ptr<RandomizedUctNode>;
    void set_parent(RandomizedUctNode* parent);
    void print_children(int level, int depth);
    void print_node();
    auto operator<(const RandomizedUctNode& other) const -> bool;

  private:
    const int count_of_states = 5;
    std::unique_ptr<Game> game; 
    std::string active_player_name;
    bool is_game_finished;
    int wins;
    int visits;
    double UCT;
    RandomizedUctNode* parent{};
    std::map<PlayerDecision, std::vector<std::unique_ptr<RandomizedUctNode>>> children;
    std::vector<PlayerDecision> possible_moves;
    void update(Player* winner, int hp_advantage);
    auto calculate_UCT(int wins, int visits, int parent_visits) const -> double;
    [[nodiscard]] auto is_fully_expanded() const -> bool;
    [[nodiscard]] auto get_highest_UCT_child() const -> RandomizedUctNode*;
    [[nodiscard]] auto get_UCT() const -> double;
    [[nodiscard]] auto get_wins() const -> int { return wins; };
    static auto get_winner(GameData& game_data)
        -> std::pair<Player*, int>;
};

#endif
