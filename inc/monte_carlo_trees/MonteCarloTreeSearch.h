#ifndef MCTS_INTERFACE
#define MCTS_INTERFACE

#include <Army.h>
#include <DecisionMaker.h>
#include <Node.h>
#include <PlayerDecisionManager.h>
#include <Tile.h>

#include <memory>
#include <queue>

class PlayerDecision;

class MonteCarloTreeSearch : public DecisionMaker
{
  public:
    MonteCarloTreeSearch();
    MonteCarloTreeSearch(const MonteCarloTreeSearch& other);
    auto get_decision(Game* game) -> std::unique_ptr<PlayerDecision> override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<DecisionMaker> override;
  private:
    void prepare_decision(Game* game);
    void find_root(Game* game);
    void single_iteration();
    std::unique_ptr<Node> root;
    PlayerDecisionManager moves_queue;
    int prev_moves_archive_size{ 0 };
};

#endif
