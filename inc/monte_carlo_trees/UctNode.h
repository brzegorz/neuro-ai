#ifndef UCT_NODE
#define UCT_NODE

#include <Game.h>
#include <PlayerDecision.h>
#include <PointerDereferenceComparator.h>

#include <map>
#include <memory>

class UctNode
{
  public:
    UctNode(const Game& game);
    UctNode(const Game& game, PlayerDecision& next_decision);
    UctNode(const UctNode& other);
    auto traverse() -> UctNode*;
    auto expand() -> UctNode*;
    auto playout() -> std::pair<Player*, int>;
    void backpropagate(Player* winner, int hp_advantage, UctNode* root);
    [[nodiscard]] auto get_best_move() const -> PlayerDecision;
    [[nodiscard]] auto get_visits_counts_per_move() const -> std::map<PlayerDecision, int>;
    [[nodiscard]] auto get_visits_count() const -> int;
    [[nodiscard]] auto get_children_count() const -> int;
    auto get_child(const PlayerDecision& decision) -> std::unique_ptr<UctNode>;
    void set_parent(UctNode* parent);
    auto operator<(const UctNode& other) const -> bool;

  private:
    std::unique_ptr<Game> game;
    std::string active_player_name;
    bool is_game_finished;
    int wins;
    int visits;
    double UCT;
    UctNode* parent{};
    std::map<PlayerDecision, std::unique_ptr<UctNode>> children;
    std::vector<PlayerDecision> possible_moves;
    void update(Player* winner, int hp_advantage);
    [[nodiscard]] auto is_fully_expanded() const -> bool;
    [[nodiscard]] auto get_highest_UCT_child() const -> UctNode*;
    [[nodiscard]] auto get_UCT() const -> double;
    [[nodiscard]] auto get_wins() const -> int { return wins; };
    static auto get_winner_with_hp_advantage(GameData& game_data)
        -> std::pair<Player*, int>;
};

#endif
