#ifndef VectorizedBoard_CLASS
#define VectorizedBoard_CLASS

#include <Coordinates.h>
#include <Directions.h>

#include <array>
#include <set>
#include <unordered_map>

class Game;
class Player;
class Tile;

class VectorizedBoard
{
    friend class Hit;
    friend class Shoot;

  public:
    VectorizedBoard(Game* game);
    VectorizedBoard(const VectorizedBoard& other);
    void add_tile(Coordinates coords, Tile* tile);
    void delete_tile(Tile* tile);
    void add_winner(Player* winner);
    auto get_csv_header() const -> const std::string;
    auto get_csv_row() const -> std::string;

  private:
    int TILES_COUNT;
    static const int COORDS_SIZE = 3;
    static const int HP_SIZE = 1;
    static const int DEPENDENT_VAR_SIZE = 1;
    static const int TILE_SIZE = COORDS_SIZE + HP_SIZE;
    int VECTOR_SIZE;
    std::vector<int> vectorized_game;
    std::set<std::string> tile_names;
    void add_player_tiles(Player* player);
    auto get_tile_index(Tile* tile) -> int;
    static auto make_csv_field(const std::string& tile_name,
                               const std::string& field_name)
        -> const std::string;
    std::unordered_map<std::string, int> indexes_map;
};

#endif
