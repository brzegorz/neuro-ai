#ifndef GAME_RECORDER_CLASS
#define GAME_RECORDER_CLASS

#include <VectorizedBoard.h>

#include <vector>

class Player;

class GameRecorder
{
  public:
    void add_game_vector(const VectorizedBoard& game_vector);
    void add_winner(Player* winner);
    void save_to_file() const;

  private:
    std::vector<VectorizedBoard> archive;
    static auto make_file_path() -> std::string;
};

#endif
