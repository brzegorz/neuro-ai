#ifndef ARMY_BUILDER_INTERFACE
#define ARMY_BUILDER_INTERFACE

#include <memory>
#include <vector>

class Player;
class Tile;

class ArmyBuilder
{
  public:
    virtual ~ArmyBuilder() = default;
    virtual void assign_army(Player* owner) = 0;
    static auto make_movement_tile(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_battle_tile(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_sniper_tile(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_bomb_tile(Player* owner) -> std::unique_ptr<Tile>;
};

#endif
