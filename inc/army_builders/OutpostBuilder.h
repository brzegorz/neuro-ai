#ifndef OutpostBuilder_INTERFACE
#define OutpostBuilder_INTERFACE

#include <ArmyBuilder.h>

#include <memory>
#include <vector>

class OutpostBuilder : public ArmyBuilder
{
  public:
    void assign_army(Player* owner) override;
    static auto make_headquarter(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_runner(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_HMG(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_commando(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_annihilator(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_mobile_armor(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_brawler(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_saboteur(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_recon_center(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_medic(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_officer(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_scoper(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_scout(Player* owner) -> std::unique_ptr<Tile>;
};

#endif
