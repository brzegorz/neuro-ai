#ifndef TEST_ARMY_BUILDER_INTERFACE
#define TEST_ARMY_BUILDER_INTERFACE

#include <ArmyBuilder.h>

#include <memory>
#include <vector>

class TestArmyBuilder : public ArmyBuilder
{
  public:
    void assign_army(Player* owner) override;
    static void assign_army_of_size_n(Player* owner, int n);
    static auto make_headquarter(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_shooter(Player* owner) -> std::unique_ptr<Tile>;
};

#endif
