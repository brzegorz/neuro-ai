#ifndef MolochBuilder_INTERFACE
#define MolochBuilder_INTERFACE

#include <ArmyBuilder.h>

#include <memory>
#include <vector>

class MolochBuilder : public ArmyBuilder
{
  public:
    void assign_army(Player* owner) override;
    static auto make_headquarter(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_blocker(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_hybrid(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_gauss_cannon(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_juggernaut(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_hunter_killer(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_clown(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_armored_hunter(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_armored_guard(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_guard(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_protector(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_hornet(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_net_fighter(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_stormtrooper(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_mother_module(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_medic(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_brain(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_officer(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_scout(Player* owner) -> std::unique_ptr<Tile>;
    static auto make_push_back(Player* owner) -> std::unique_ptr<Tile>;
};

#endif
