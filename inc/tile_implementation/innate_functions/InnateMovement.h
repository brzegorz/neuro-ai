#ifndef MOVEMENT_INNATE_FUNC
#define MOVEMENT_INNATE_FUNC

#include <InnateFunction.h>

class Game;

class InnateMovement : public InnateFunction
{
  public:
    void change_board(Game* game) override;
    auto is_usable(Game* game) const -> bool override;
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    [[nodiscard]] auto get_ascii_description() const -> std::string override;
    [[nodiscard]] auto clone() const
        -> std::unique_ptr<InnateFunction> override;
    auto operator==(const InnateFunction& other) const -> bool override;

  private:
    static auto get_target_location(Game* game)
        -> std::pair<Coordinates, DIRECTION>;
};

#endif
