#ifndef INNATE_FUNC
#define INNATE_FUNC

#include <PlayerDecision.h>

class Game;

class InnateFunction
{
  public:
    InnateFunction()
        : already_used{ false } {};
    virtual ~InnateFunction() = default;
    virtual void change_board(Game* game) = 0;
    virtual auto is_usable(Game* game) const -> bool = 0;
    virtual auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> = 0;
    void allow_usage() { already_used = false; };
    [[nodiscard]] virtual auto get_ascii_description() const -> std::string = 0;
    [[nodiscard]] virtual auto clone() const
        -> std::unique_ptr<InnateFunction> = 0;
    virtual auto operator==(const InnateFunction& other) const -> bool = 0;
    auto operator!=(const InnateFunction& other) const -> bool;

  protected:
    bool already_used;
};

#endif
