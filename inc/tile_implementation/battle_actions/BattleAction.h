#ifndef ACTION_CLASS
#define ACTION_CLASS

#include <Directions.h>
#include <VectorizedBoard.h>

#include <map>
#include <memory>
#include <vector>

struct tile_parameters;
class Board;
class TextGui;

class BattleAction
{
  public:
    BattleAction(DIRECTION dir)
        : direction{ dir } {};
    virtual ~BattleAction() = default;
    virtual void change_board(Board& board, Coordinates tile_coords) = 0;
    virtual void update_ascii_gui(const tile_parameters& p,
                                  TextGui* gui) const = 0;
    [[nodiscard]] auto get_direction() const -> DIRECTION { return direction; };
    void set_direction(DIRECTION new_dir);
    virtual void modify_shoot_strength(int change);
    virtual void modify_hit_strength(int change);
    [[nodiscard]] virtual auto clone() const
        -> std::unique_ptr<BattleAction> = 0;
    virtual auto operator==(const BattleAction& other) const -> bool = 0;
    // Ordered set of actions is way easier to compare
    virtual auto operator<(const BattleAction& other) const -> bool = 0;
    auto operator!=(const BattleAction& other) const -> bool;
    virtual void print_action() const = 0;

  protected:
    DIRECTION direction;
};

#endif
