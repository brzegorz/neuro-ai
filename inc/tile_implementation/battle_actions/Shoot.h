#ifndef SHOOT_ACTION
#define SHOOT_ACTION

#include <BattleAction.h>
#include <Directions.h>
#include <VectorizedBoard.h>

#include <string>

class Board;
class Tile;

class Shoot : public BattleAction
{
  public:
    Shoot(int str, DIRECTION dir)
        : BattleAction(dir)
        , strength(str)
    {}
    void change_board(Board& board, Coordinates tile_coords) override;
    void update_ascii_gui(const tile_parameters& p,
                          TextGui* gui) const override;
    [[nodiscard]] auto get_strength() const -> int { return strength; };
    void modify_shoot_strength(int change) override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<BattleAction> override;
    auto operator==(const BattleAction& other) const -> bool override;
    auto operator<(const BattleAction& other) const -> bool override;
    void print_action() const override;

  private:
    int strength;
    auto get_target(Board& board, Coordinates tile_coords) -> Tile*;
};

#endif
