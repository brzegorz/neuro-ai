#ifndef MOVEMENT_USAGE_FUNC
#define MOVEMENT_USAGE_FUNC

#include <UsageFunction.h>

class Movement : public UsageFunction
{
  public:
    void change_board(Game* game) override;
    auto is_usable(Game* game) const -> bool override;
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<UsageFunction> override;
    auto operator==(const UsageFunction& other) const -> bool override;

  private:
    static auto get_source_and_target(Game* game)
        -> std::tuple<Coordinates, Coordinates, DIRECTION>;
};

#endif
