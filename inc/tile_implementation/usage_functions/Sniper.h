#ifndef SNIPER_INSTANT_FUNC
#define SNIPER_INSTANT_FUNC

#include <UsageFunction.h>

#include <memory>
#include <vector>

class Sniper : public UsageFunction
{
  public:
    void change_board(Game* game) override;
    auto is_usable(Game* game) const -> bool override;
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<UsageFunction> override;
    auto operator==(const UsageFunction& other) const -> bool override;

  private:
    static auto get_sniper_target(Game* game) -> Coordinates;
};

#endif
