#ifndef USAGE_FUNC_INTERFACE
#define USAGE_FUNC_INTERFACE

#include <PlayerDecision.h>

#include <memory>
#include <vector>

class Game;

class UsageFunction
{
  public:
    virtual ~UsageFunction() = default;
    virtual void change_board(Game* game) = 0;
    virtual auto is_usable(Game* game) const -> bool = 0;
    virtual auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> = 0;
    [[nodiscard]] virtual auto clone() const
        -> std::unique_ptr<UsageFunction> = 0;
    virtual auto operator==(const UsageFunction& other) const -> bool = 0;
    auto operator!=(const UsageFunction& other) const -> bool;
};

#endif
