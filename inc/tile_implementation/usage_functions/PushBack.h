#ifndef PUSH_BACK_USAGE_FUNC
#define PUSH_BACK_USAGE_FUNC

#include <UsageFunction.h>

#include <memory>
#include <vector>

class PushBack : public UsageFunction
{
  public:
    void change_board(Game* game) override;
    auto is_usable(Game* game) const -> bool override;
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<UsageFunction> override;
    auto operator==(const UsageFunction& other) const -> bool override;

  private:
    static auto can_tile_push(const Board* board,
                              const Player* active_player,
                              const Player* enemy,
                              Coordinates pusher_coords) -> bool;
    static auto can_tile_be_pushed(const Board* board,
                                   const Player* active_player,
                                   const Player* enemy,
                                   Coordinates pusher_coords,
                                   DIRECTION push_dir) -> bool;
    static auto get_push_parameters(Game* game)
        -> std::pair<Coordinates, DIRECTION>;
};

#endif
