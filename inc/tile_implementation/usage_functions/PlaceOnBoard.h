#ifndef PLACE_ON_BOARD_USAGE_FUNC
#define PLACE_ON_BOARD_USAGE_FUNC

#include <UsageFunction.h>

#include <memory>
#include <vector>

class PlaceOnBoard : public UsageFunction
{
  public:
    void change_board(Game* game) override;
    auto is_usable(Game* game) const -> bool override;
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<UsageFunction> override;
    auto operator==(const UsageFunction& other) const -> bool override;

  private:
    static auto get_target_location(Game* game)
        -> std::pair<Coordinates, DIRECTION>;
};

#endif
