#ifndef TILE_CLASS
#define TILE_CLASS

#include <BattleAction.h>
#include <Board.h>
#include <Directions.h>
#include <InnateFunction.h>
#include <PassiveBonus.h>
#include <Player.h>
#include <PointerDereferenceComparator.h>
#include <UsageFunction.h>

#include <memory>
#include <set>
#include <utility>

#include <vector>

class Game;
class PlayerDecision;
struct tile_parameters;

class Tile
{
  public:
    // Tile construction
    Tile(bool is_headquarter,
         std::string name,
         Player* owner,
         int hp,
         std::set<int> initiative = std::set<int>(),
         std::unique_ptr<std::set<std::unique_ptr<BattleAction>>>
             battle_actions =
                 std::make_unique<std::set<std::unique_ptr<BattleAction>>>(),
         std::unordered_set<DIRECTION> armour_directions =
             std::unordered_set<DIRECTION>{});
    Tile(bool is_headquarter,
         std::string name,
         Player* owner,
         int hp,
         std::unique_ptr<std::set<std::unique_ptr<PassiveBonus>>>
             passive_bonuses);
    Tile(
        bool is_headquarter,
        std::string name,
        Player* owner,
        int hp,
        std::set<int> initiative,
        std::unique_ptr<std::set<std::unique_ptr<BattleAction>>> battle_actions,
        std::unique_ptr<std::set<std::unique_ptr<PassiveBonus>>>
            passive_bonuses);
    Tile(std::string name, Player* owner, std::unique_ptr<UsageFunction> usage);
    void set_innate_function(std::unique_ptr<InnateFunction> innate_func);
    Tile(const Tile& other);
    // Functional
    const bool is_headquarter;
    void use(Game* game) const;
    void use_innate(Game* game);
    void do_battle_actions(Board& board, Coordinates tile_coords) const;
    void apply_passive_bonuses(Board& board, Coordinates source_coords) const;
    void reverse_passive_bonuses(Board& board, Coordinates source_coords) const;
    void apply_passive_bonuses(Board& board,
                               Coordinates source_coords,
                               DIRECTION target_direction) const;
    void add_medic_bonus(Board& board,
                         Coordinates source_coords,
                         DIRECTION target_direction) const;
    void reverse_passive_bonuses(Board& board,
                                 Coordinates source_coords,
                                 DIRECTION target_direction) const;
    void remove_medic_bonus(Board& board,
                            Coordinates source_coords,
                            DIRECTION target_direction) const;
    // getters
    auto get_name() const -> const std::string& { return name; };
    auto get_owner() const -> Player* { return owner; };
    auto get_hp() const -> int { return hp; };
    auto is_damaged() const -> bool { return not damage_received.empty(); };
    auto get_initiative() const -> const std::set<int>& { return initiative; };
    auto get_direction() const -> DIRECTION { return direction; };
    auto get_damage() const -> const std::vector<int>&
    {
        return damage_received;
    };
    auto is_armoured(DIRECTION where) const -> bool;
    auto is_medic(DIRECTION where) const -> bool;
    auto is_in_net() const -> bool;
    auto is_usable(Game* game) const -> bool;
    auto get_heals_enemy() const -> bool { return heals_enemy; };
    auto get_innate_function() const -> InnateFunction*
    {
        return innate.get();
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision>;
    // setters
    void rotate(const DIRECTION& new_direction);
    void set_owner(Player& owner) { this->owner = &owner; };
    void set_name(std::string new_name) { name = std::move(new_name); };
    void set_initiative(std::set<int> init) { initiative = std::move(init); };
    void receive_damage_in_battle(int damage, Tile* damage_dealer);
    void resolve_damage();
    void add_medic_direction(DIRECTION dir);
    void allow_innate_function_usage()
    {
        if (innate)
            innate->allow_usage();
    };
    void modify_shoot_strength(int change);
    void modify_hit_strength(int change);
    void switch_passives_allegiance(Board& board, Coordinates target_coords);
    void net_unit(Board& board, Coordinates unit_coords);
    void unnet_unit(Board& board, Coordinates unit_coords);
    void unnet_when_mutual_netting();
    void get_healed(int index_of_attack_to_heal);
    // text UI helpers
    const bool is_name_visible;
    void update_ascii_gui(const tile_parameters& p, TextGui* gui) const;
    auto get_passives_string() const -> std::string;
    auto get_innate_function_string() const -> std::string;
    auto operator==(const Tile& other) const -> bool;
    auto operator!=(const Tile& other) const -> bool;

  private:
    std::string name;
    Player* owner; // Managed by GameData.h
    int hp;
    std::vector<int> damage_received;
    DIRECTION direction;
    std::set<int> initiative;
    std::set<std::unique_ptr<BattleAction>,
             pointer_dereference_comparator<std::unique_ptr<BattleAction>>>
        battle_actions;
    std::set<std::unique_ptr<PassiveBonus>,
             pointer_dereference_comparator<std::unique_ptr<PassiveBonus>>>
        passive_bonuses;
    std::unique_ptr<UsageFunction> usage;
    std::unique_ptr<InnateFunction> innate;
    std::unordered_set<DIRECTION> directions_of_armour;
    std::unordered_set<DIRECTION> directions_of_medic;
    bool heals_enemy;
    int count_of_nets_on_unit;
    friend void Board::rotate_tile(Coordinates coords,
                                   const DIRECTION& new_direction);
    auto compare_battle_actions(const Tile& other) const -> bool;
    auto compare_passive_bonuses(const Tile& other) const -> bool;
};

#endif
