#ifndef PASSIVE_BONUS_STRENGTH_SHOOT_CLASS
#define PASSIVE_BONUS_STRENGTH_SHOOT_CLASS

#include <Directions.h>
#include <PassiveBonus.h>
#include <VectorizedBoard.h>

#include <map>
#include <memory>
#include <vector>

class Board;

class BonusStrengthShoot : public PassiveBonus
{
  public:
    BonusStrengthShoot(DIRECTION dir)
        : PassiveBonus(dir, AFFECTS_FRIENDS){};
    void apply_bonus(Board& board, Coordinates target_coords) override;
    void reverse_bonus(Board& board, Coordinates target_coords) override;
    [[nodiscard]] auto get_string() const -> std::string override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<PassiveBonus> override;
    auto operator==(const PassiveBonus& other) const -> bool override;
    // Ordered set of passives is way easier to compare
    auto operator<(const PassiveBonus& other) const -> bool override;
    auto operator!=(const PassiveBonus& other) const -> bool;
};

#endif
