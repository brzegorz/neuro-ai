#ifndef PASSIVE_BONUS_Net_CLASS
#define PASSIVE_BONUS_Net_CLASS

#include <Directions.h>
#include <PassiveBonus.h>
#include <VectorizedBoard.h>

#include <iostream>
#include <map>
#include <memory>
#include <vector>

class Board;

class MalusNet : public PassiveBonus
{
  public:
    MalusNet(DIRECTION dir)
        : PassiveBonus(dir, AFFECTS_ENEMIES){};
    void apply_bonus(Board& board, Coordinates target_coords) override;
    void reverse_bonus(Board& board, Coordinates target_coords) override;
    void switch_allegiance(Board& board, Coordinates bonus_coords) override;
    void update_ascii_gui(const tile_parameters& p,
                          TextGui* gui) const override;
    [[nodiscard]] auto get_string() const -> std::string override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<PassiveBonus> override;
    auto operator==(const PassiveBonus& other) const -> bool override;
    // Ordered set of passives is way easier to compare
    auto operator<(const PassiveBonus& other) const -> bool override;
    auto operator!=(const PassiveBonus& other) const -> bool;

  private:
    void resolve_mutual_nets(Board& board, Coordinates victim_coords);
    static bool is_mutual_nets_being_resolved_now;
};

#endif
