#ifndef PASSIVE_MALUS_INITIATIVE_CLASS
#define PASSIVE_MALUS_INITIATIVE_CLASS

#include <Directions.h>
#include <PassiveBonus.h>
#include <VectorizedBoard.h>

#include <map>
#include <memory>
#include <vector>

class Board;

class MalusInitiative : public PassiveBonus
{
  public:
    MalusInitiative(DIRECTION dir)
        : PassiveBonus(dir, AFFECTS_ENEMIES)
        , old_target_initiative{} {};
    void apply_bonus(Board& board, Coordinates target_coords) override;
    void reverse_bonus(Board& board, Coordinates target_coords) override;
    [[nodiscard]] auto get_string() const -> std::string override;
    [[nodiscard]] auto clone() const -> std::unique_ptr<PassiveBonus> override;
    auto operator==(const PassiveBonus& other) const -> bool override;
    // Ordered set of passives is way easier to compare
    auto operator<(const PassiveBonus& other) const -> bool override;
    auto operator!=(const PassiveBonus& other) const -> bool;

  private:
    std::set<int> old_target_initiative;
};

#endif
