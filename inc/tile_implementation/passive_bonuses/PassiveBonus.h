#ifndef PASSIVE_BONUS_CLASS
#define PASSIVE_BONUS_CLASS

#include <Directions.h>
#include <VectorizedBoard.h>

#include <map>
#include <memory>
#include <vector>

struct tile_parameters;
class Board;
class TextGui;

class PassiveBonus
{
  public:
    PassiveBonus(DIRECTION dir, bool affects_enemies);
    virtual ~PassiveBonus() = default;
    virtual void apply_bonus(Board& board, Coordinates target_coords) = 0;
    virtual void reverse_bonus(Board& board, Coordinates target_coords) = 0;
    virtual void switch_allegiance(Board& board, Coordinates bonus_coords);
    void set_direction(DIRECTION new_dir);
    virtual void update_ascii_gui(const tile_parameters& p, TextGui* gui) const;
    [[nodiscard]] virtual auto get_string() const -> std::string = 0;
    [[nodiscard]] auto get_direction() const -> DIRECTION { return direction; };
    [[nodiscard]] virtual auto clone() const
        -> std::unique_ptr<PassiveBonus> = 0;
    virtual auto operator==(const PassiveBonus& other) const -> bool = 0;
    // Ordered set of passives is way easier to compare
    virtual auto operator<(const PassiveBonus& other) const -> bool = 0;
    auto operator!=(const PassiveBonus& other) const -> bool;

  protected:
    DIRECTION direction;
    bool affects_enemies;
    bool was_target_changed;
    auto is_target_valid(Board& board, Coordinates target_coords) -> bool;
    auto is_target_enemy(Board& board, Coordinates target_coords) -> bool;
    static const bool AFFECTS_ENEMIES = true;
    static const bool AFFECTS_FRIENDS = false;
};

#endif
