#ifndef ALL_BONUSES
#define ALL_BONUSES

#include <BonusAttack.h>
#include <BonusInitiative.h>
#include <BonusReconCenter.h>
#include <BonusStrengthHit.h>
#include <BonusStrengthShoot.h>
#include <MalusInitiative.h>
#include <MalusNet.h>
#include <MalusScoper.h>
#include <PassiveBonus.h>

#endif
