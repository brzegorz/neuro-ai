#ifndef GUI_CLASS
#define GUI_CLASS

#include <iostream>
#include <memory>
#include <set>
#include <vector>

#include <Coordinates.h>

class Board;
class Player;

class Gui
{
  public:
    virtual ~Gui() = default;
    // Output to player
    virtual void print_board(const Board& board) = 0;
    virtual void print_player_hand(Player* player) = 0;
    virtual void print_game_result(Player* winner) = 0;
    virtual void print_progress_bar(int current, int max) const = 0;
    // Input from player
    [[nodiscard]] virtual auto get_numeric_input(
        std::string question,
        const std::set<int>& valid_values) const -> int = 0;
    [[nodiscard]] virtual auto get_direction(
        std::string direction_question = "Please pick direction",
        const std::set<DIRECTION>& valid_directions =
            std::set<DIRECTION>{ N, NE, SE, S, SW, NW }) const -> DIRECTION = 0;
    [[nodiscard]] virtual auto get_coords(
        std::string question,
        const std::set<Coordinates>& valid_coords) const -> Coordinates = 0;
};

#endif
