#ifndef TEXT_GUI_CLASS
#define TEXT_GUI_CLASS

#include <Directions.h>
#include <Gui.h>

#include <iostream>
#include <memory>
#include <vector>

struct tile_parameters;
class Board;
class Tile;

class TextGui : public Gui
{
  public:
    // Output to player
    void print_board(const Board& board) override;
    void print_player_hand(Player* player) override;
    void print_game_result(Player* winner) override;
    void print_progress_bar(int current, int max) const override;
    // Input from player
    [[nodiscard]] auto get_numeric_input(
        std::string question,
        const std::set<int>& valid_values) const -> int override;
    [[nodiscard]] auto get_direction(
        std::string direction_question,
        const std::set<DIRECTION>& valid_directions) const
        -> DIRECTION override;
    [[nodiscard]] auto get_coords(std::string question,
                                  const std::set<Coordinates>& valid_coords)
        const -> Coordinates override;
    void add_text_at_particular_hex(const tile_parameters& p,
                                    int x,
                                    int y,
                                    const std::string& text);

  private:
    static const int tile_width = 13;
    static const int tile_height = 4;
    std::vector<std::vector<std::string>> ascii_vec;
    void construct_ascii_vector(const Board& board);
    void initialize_ascii_vector(const Board& board);
    void add_coords_axes(const Board& board, int gui_width, int gui_height);
    void add_tile_borders(const tile_parameters& p);
    void add_tile_ascii(Tile* tile, tile_parameters p);
    // Tile properties
    void add_tile_hp(const tile_parameters& p, Tile* tile);
    void add_tile_initiative(const tile_parameters& p, Tile* tile);
    void add_tile_name(const tile_parameters& p, Tile* tile);
    void add_tile_passives(const tile_parameters& p, Tile* tile);
    void add_tile_innate(const tile_parameters& p, Tile* tile);
    void add_tile_medic(const tile_parameters& p, Tile* tile);
    void add_single_medic_direction(const tile_parameters& p, DIRECTION dir);
    // Borders
    void add_border(DIRECTION border_direction, const tile_parameters& p);
    // underscores
    void add_border_north(const tile_parameters& p);
    void add_border_south(const tile_parameters& p);
    // backslashes
    void add_border_north_east(const tile_parameters& p);
    void add_border_south_west(const tile_parameters& p);
    // slashes
    void add_border_north_west(const tile_parameters& p);
    void add_border_south_east(const tile_parameters& p);
    static auto board_x_to_ascii_x(int board_x) -> int;
    static auto board_y_to_ascii_y(int board_y) -> int;
    static auto ascii_x_to_board_x(int ascii_x) -> int;
    static auto ascii_y_to_board_y(int ascii_y) -> int;
};

#endif
