#ifndef PLAYER_DECISIONCLASS
#define PLAYER_DECISIONCLASS

#include <Coordinates.h>
#include <Directions.h>
#include <Tile_usage_way.h>

#include <deque>

class PlayerDecision
{
  public:
    PlayerDecision()
        : type{}
        , decisions{} {};
    //~ PlayerDecision(const PlayerDecision& other){decisions =
    // other.decisions;};
    [[nodiscard]] auto is_available() const -> bool
    {
        return not decisions.empty();
    };
    auto get_tile_to_use_index() -> int;
    void set_tile_to_use_index(int index);
    auto get_chosen_way_of_tile_usage() -> TILE_USAGE_WAY;
    void set_chosen_way_of_tile_usage(int way);
    auto get_tile_target_location() -> std::pair<Coordinates, DIRECTION>;
    void set_tile_target_location(int x, int y, int direction);
    auto get_movement_source_and_target()
        -> std::tuple<Coordinates, Coordinates, DIRECTION>;
    void set_movement_source_and_target(Coordinates source,
                                        Coordinates target,
                                        DIRECTION rotation);
    auto get_push_source_and_direction() -> std::pair<Coordinates, DIRECTION>;
    void set_push_source_and_direction(Coordinates push_source,
                                       DIRECTION push_dir);
    auto get_whether_to_use_innates() -> bool;
    void set_whether_to_use_innates(bool whether_to_use);
    auto get_attack_to_heal_index() -> int;
    void set_attack_to_heal_index(int index);
    void print_decision() const;
    auto operator<(const PlayerDecision& other) const -> bool;

  private:
    enum DECISION_TYPE
    {
        TILE_TO_USE_INDEX,
        WAY_OF_TILE_USAGE,
        TILE_TARGET_LOCATION,
        MOVEMENT_PARAMS,
        PUSH_PARAMS,
        WHETHER_TO_USE_INNATES,
        ATTACK_TO_HEAL_INDEX
    };
    DECISION_TYPE type;
    std::deque<int> decisions;
    void crash_if_already_set();
    static void crash_if_wrong_type_read();
};

#endif
