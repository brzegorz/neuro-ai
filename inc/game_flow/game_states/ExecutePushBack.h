#ifndef ObtanPushDirection_CLASS
#define ObtanPushDirection_CLASS

#include <GameState.h>

class ExecutePushBack : public GameState
{
  public:
    ExecutePushBack(std::pair<Coordinates, DIRECTION> push_params)
        : pusher_coords{ push_params.first }
        , push_direction{ push_params.second } {};
    void advance_game(Game* game) override;
    auto requires_player_input(Game * /*game*/) -> bool override
    {
        return true;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override { return "ExecutePushBack"; };
    auto clone() -> std::unique_ptr<GameState> override;

  private:
    Coordinates pusher_coords;
    DIRECTION push_direction;
    static auto get_target_location(Game* game) -> Coordinates;
};

#endif
