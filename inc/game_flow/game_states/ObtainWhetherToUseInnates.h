#ifndef ObtainWhetherToUseInnates_CLASS
#define ObtainWhetherToUseInnates_CLASS

#include <GameState.h>

class ObtainWhetherToUseInnates : public GameState
{
  public:
    void advance_game(Game* game) override;
    auto requires_player_input(Game * /*game*/) -> bool override
    {
        return true;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override
    {
        return "ObtainWhetherToUseInnates";
    };
    auto clone() -> std::unique_ptr<GameState> override;

  private:
    static auto get_whether_to_use_innates(Game* game) -> bool;
};

#endif
