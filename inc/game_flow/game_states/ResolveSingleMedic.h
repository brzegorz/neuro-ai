#ifndef ResolveSingleMedic_CLASS
#define ResolveSingleMedic_CLASS

#include <GameState.h>

class Tile;

class ResolveSingleMedic : public GameState
{
  public:
    void advance_game(Game* game) override;
    auto requires_player_input(Game * /*game*/) -> bool override
    {
        return true;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override { return "ResolveSingleMedic"; };
    auto clone() -> std::unique_ptr<GameState> override;
    auto get_tile_to_heal(Game* game) -> Coordinates;
};

#endif
