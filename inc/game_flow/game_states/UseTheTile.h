#ifndef UseTheTile_CLASS
#define UseTheTile_CLASS

#include <GameState.h>

class UseTheTile : public GameState
{
  public:
    void advance_game(Game* game) override;
    auto requires_player_input(Game* game) -> bool override
    {
        return game->get_data().get_way_of_using_current_tile() == USE and
               game->get_data().tile_being_used;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override { return "UseTheTile"; };
    auto clone() -> std::unique_ptr<GameState> override;

  private:
    static void use_the_tile(Game* game);
};

#endif
