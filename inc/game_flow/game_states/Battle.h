#ifndef ExecuteBattle_CLASS
#define ExecuteBattle_CLASS

#include <GameState.h>

class Board;
class Game;
class Tile;

class Battle : public GameState
{
  public:
    void advance_game(Game* game) override;
    auto requires_player_input(Game * /*game*/) -> bool override
    {
        return false;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override { return "Battle"; };
    auto clone() -> std::unique_ptr<GameState> override;
    static void battle_without_player_input(Board* board);

  private:
    static void battle(Game* game);
    static auto get_max_initiative(const Board* board) -> int;
};
#endif
