#ifndef ObtainWayOfUsingTheTile_CLASS
#define ObtainWayOfUsingTheTile_CLASS

#include <GameState.h>
#include <Tile_usage_way.h>

class ObtainWayOfUsingTheTile : public GameState
{
  public:
    void advance_game(Game* game) override;
    auto requires_player_input(Game * /*game*/) -> bool override
    {
        return true;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override
    {
        return "ObtainWayOfUsingTheTile";
    };
    auto clone() -> std::unique_ptr<GameState> override;

  private:
    static auto get_way_of_using_the_tile(Game* game) -> TILE_USAGE_WAY;
};

#endif
