#ifndef ExecuteBattleTurn_CLASS
#define ExecuteBattleTurn_CLASS

#include <GameState.h>

class Board;
class Tile;

class BattleSingleTurn : public GameState
{
  public:
    void advance_game(Game* game) override;
    auto requires_player_input(Game * /*game*/) -> bool override
    {
        return false;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override { return "BattleSingleTurn"; };
    auto clone() -> std::unique_ptr<GameState> override;
    static void battle_turn_without_player_input(Board* board, int initiative);

  private:
    static auto is_tile_acting(const Tile& tile, int initiative) -> bool;
    static void clean_board_without_player_input(Board* board);
};

#endif
