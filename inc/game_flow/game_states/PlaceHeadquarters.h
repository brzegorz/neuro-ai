#ifndef PlaceHeadquarters_CLASS
#define PlaceHeadquarters_CLASS

#include <GameState.h>

class PlaceHeadquarters : public GameState
{
  public:
    void advance_game(Game* game) override;
    auto requires_player_input(Game * /*game*/) -> bool override
    {
        return true;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override { return "PlaceHeadquarters"; };
    auto clone() -> std::unique_ptr<GameState> override;
};

#endif
