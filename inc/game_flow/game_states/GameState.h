#ifndef GAME_STATE_CLASS
#define GAME_STATE_CLASS

#include <PlayerDecision.h>

#include <vector>

class Game;

class GameState
{
  public:
    virtual ~GameState() = default;
    virtual void advance_game(Game* game) = 0;
    virtual auto requires_player_input(Game* game) -> bool = 0;
    virtual auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> = 0;
    virtual auto get_name() -> std::string = 0;
    virtual auto clone() -> std::unique_ptr<GameState> = 0;
};

#endif
