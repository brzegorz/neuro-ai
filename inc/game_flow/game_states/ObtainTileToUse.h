#ifndef ObtainTileToUse_CLASS
#define ObtainTileToUse_CLASS

#include <GameState.h>

class ObtainTileToUse : public GameState
{
  public:
    void advance_game(Game* game) override;
    auto requires_player_input(Game * /*game*/) -> bool override
    {
        return true;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override { return "ObtainTileToUse"; };
    auto clone() -> std::unique_ptr<GameState> override;

  private:
    static auto get_tile_to_use_index(Game* game) -> int;
};

#endif
