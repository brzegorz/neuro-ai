#ifndef MakeFinalTurn_CLASS
#define MakeFinalTurn_CLASS

#include <GameState.h>

class MakeFinalTurn : public GameState
{
  public:
    void advance_game(Game* game) override;
    auto requires_player_input(Game * /*game*/) -> bool override
    {
        return false;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override { return "MakeFinalTurn"; };
    auto clone() -> std::unique_ptr<GameState> override;
};

#endif
