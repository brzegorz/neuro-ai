#ifndef ResolveMedics_CLASS
#define ResolveMedics_CLASS

#include <Board.h>
#include <GameState.h>

#include <map>

class ResolveMedics : public GameState
{
  public:
    void advance_game(Game* game) override;
    auto requires_player_input(Game * /*game*/) -> bool override
    {
        return false;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override { return "ResolveMedics"; };
    auto clone() -> std::unique_ptr<GameState> override;

  private:
    void update_medics_data(
        Board& board,
        std::map<Coordinates, std::set<DIRECTION>>& valid_targets_by_medic,
        std::map<Coordinates, std::set<DIRECTION>>& valid_medics_by_target);

    void add_info_of_undamaged_protected_tile(
        std::map<Coordinates, std::set<DIRECTION>>& valid_targets_by_medic,
        std::map<Coordinates, std::set<DIRECTION>>& valid_medics_by_target,
        const Coordinates medic_coords,
        const DIRECTION healing_direction);
    void

    add_info_of_damaged_protected_tile(
        std::map<Coordinates, std::set<DIRECTION>>& valid_targets_by_medic,
        std::map<Coordinates, std::set<DIRECTION>>& valid_medics_by_target,
        const Coordinates medic_coords,
        const DIRECTION healing_direction);

    auto are_medics_resolved(const std::map<Coordinates, std::set<DIRECTION>>&
                                 valid_targets_by_medic) -> bool;

    auto get_coords_of_medic_to_resolve(
        Board& board,
        std::map<Coordinates, std::set<DIRECTION>>& valid_targets_by_medic,
        const std::map<Coordinates, std::set<DIRECTION>>&
            valid_medics_by_target) -> Coordinates;

    auto get_guarded_medics(const std::map<Coordinates, std::set<DIRECTION>>&
                                valid_targets_by_medic,
                            const std::map<Coordinates, std::set<DIRECTION>>&
                                valid_medics_by_target)
        -> std::vector<Coordinates>;
    auto can_target_heal_the_medic(Board* board,
                                   Coordinates medic,
                                   Coordinates target) -> bool;
};

#endif
