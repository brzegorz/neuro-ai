#ifndef MakePlayerTurn_CLASS
#define MakePlayerTurn_CLASS

#include <GameState.h>

class MakePlayerTurn : public GameState
{
  public:
    void advance_game(Game* game) override;
    auto requires_player_input(Game * /*game*/) -> bool override
    {
        return false;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override { return "MakePlayerTurn"; };
    auto clone() -> std::unique_ptr<GameState> override;

  private:
    static void make_player_draw_tiles(Game* game);
};

#endif
