#ifndef HealSingleTile_CLASS
#define HealSingleTile_CLASS

#include <GameState.h>

class Tile;

class HealSingleTile : public GameState
{
  public:
    void advance_game(Game* game) override;
    auto requires_player_input(Game * /*game*/) -> bool override
    {
        return true;
    };
    auto get_valid_moves(Game* game) -> std::vector<PlayerDecision> override;
    auto get_name() -> std::string override { return "HealSingleTile"; };
    auto clone() -> std::unique_ptr<GameState> override;

  private:
    auto get_attack_to_heal_index(Game* game) -> int;
};

#endif
