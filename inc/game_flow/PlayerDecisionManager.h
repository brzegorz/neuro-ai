#ifndef PLAYER_DECISION_MANAGER_CLASS
#define PLAYER_DECISION_MANAGER_CLASS

#include <PlayerDecision.h>

#include <memory>
#include <queue>

class PlayerDecisionManager
{
  public:
    PlayerDecisionManager()
        : player_decisions{}
        , decisions_made{ 0 } {};
    PlayerDecisionManager(const PlayerDecisionManager& other);
    [[nodiscard]] auto player_decision_available() const -> bool;
    auto get_player_decision() -> std::unique_ptr<PlayerDecision>;
    void add_player_decision(std::unique_ptr<PlayerDecision> decision);
    void add_test_decision(std::unique_ptr<PlayerDecision> decision);
    auto get_archive_size() -> int;

  private:
    std::deque<std::unique_ptr<PlayerDecision>> player_decisions;
    int decisions_made;
};

#endif
