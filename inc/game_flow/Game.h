#ifndef GAME_CLASS
#define GAME_CLASS

#include <GameData.h>
#include <GameRecorder.h>
#include <PlaceHeadquarters.h>
#include <PlayerDecision.h>

#include <array>
#include <memory>
#include <queue>

class GameState;
class Battle;

class Game
{
    friend class Battle;

  public:
    Game();
    Game(const Game& other);
    void execute_whole_game();
    void loop_game();
    void loop_for_single_battle();
    void continue_until_input_required();
    void pick_next_state();
    void set_next_state(std::unique_ptr<GameState> next_state_ptr);
    auto get_valid_moves() -> std::vector<PlayerDecision>;
    auto get_data() -> GameData&;
    std::unique_ptr<GameRecorder> recorder;
    void set_gui(std::unique_ptr<Gui> gui);
    auto has_next_state() -> bool { return (bool)next_state; };

  private:
    GameData game_data;
    std::unique_ptr<GameState> next_state;
    void finish_game();
    auto is_player_input_needed() -> bool;
};

#endif
