#ifndef BOARD_CLASS
#define BOARD_CLASS

#include <Coordinates.h>
#include <Directions.h>
#include <VectorizedBoard.h>

#include <memory>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>

class Tile;

class Board
{
  public:
    enum TYPE
    {
        STANDARD, // For normal gameplay
        HAND      // For displaying player hand(GUI)
    };
    Board();
    Board(const Board& other);
    Board(TYPE type);
    ~Board();
    int width{};
    int height{};
    auto get_tile_indexes() const -> const std::unordered_set<Coordinates>&;
    auto get_empty_indexes() const -> const std::unordered_set<Coordinates>&;
    auto get_occupied_indexes() const -> const std::unordered_set<Coordinates>&;
    auto get_hq_coords() const -> const std::unordered_set<Coordinates>&;
    auto get_players_indexes(const Player* owner) const
        -> const std::unordered_set<Coordinates>;
    auto is_full() const -> bool;
    auto get_neighbours(const Coordinates& coords) const
        -> std::set<Coordinates>;
    auto get_empty_neighbours(const Coordinates& coords) const
        -> std::set<Coordinates>;
    static auto get_neighbour(const Coordinates& coords, DIRECTION dir)
        -> Coordinates;
    auto is_valid_position(const Coordinates& coords) const -> bool;
    auto get_tile_ptr(const Coordinates& coords) const -> Tile*;
    void set_tile(const Coordinates& coords, std::unique_ptr<Tile> new_tile);
    void kill_tile(Coordinates coords);
    auto pick_up_tile(const Coordinates& coords) -> std::unique_ptr<Tile>;
    void rotate_tile(Coordinates coords, const DIRECTION& new_direction);
    void damage_tile_outside_battle(Coordinates coords, int damage);
    void add_passive_bonus_source(const Coordinates& coords,
                                  DIRECTION source_direction);
    void remove_passive_bonus_source(const Coordinates& coords,
                                     DIRECTION source_direction);
    void add_medic_source(const Coordinates& coords,
                          DIRECTION source_direction);
    void remove_medic_source(const Coordinates& coords,
                             DIRECTION source_direction);
    auto get_medic_sources() const -> const
        std::unordered_map<Coordinates, std::unordered_set<DIRECTION>>&;
    auto get_vectorized_board() const -> const VectorizedBoard&;
    void set_vectorized_board(std::unique_ptr<VectorizedBoard> board);
    auto operator==(const Board& other) const -> bool;
    auto operator!=(const Board& other) const -> bool;

  private:
    std::unordered_map<Coordinates, std::unique_ptr<Tile>> tiles;
    std::unordered_map<Coordinates, std::unordered_set<DIRECTION>>
        passive_bonus_sources;
    std::unordered_map<Coordinates, std::unordered_set<DIRECTION>>
        medic_sources;
    std::unordered_set<Coordinates> tile_indexes;
    std::unordered_set<Coordinates> empty_tile_indexes;
    std::unordered_set<Coordinates> occupied_tile_indexes;
    std::unordered_set<Coordinates> hq_coords;
    std::unique_ptr<VectorizedBoard> vectorized_board;
    void create_standard_board();
    void create_hand_board();
};

#endif
