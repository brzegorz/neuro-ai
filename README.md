### A.I. for the Polish board game Neuroshima hex - W.I.P

- See http://manandmachine.joomla.com/ for detailed history

### How to run(Linux):

##### Prequisites:
- GNU make
- gcc

##### Instructions:
- git clone https://bitbucket.org/brzegorz/neuro-ai
- cd neuro-ai
- make run

### What is done:

- Main game loop
- Tiles that: shoot, hit
- Instant tiles: Bomb, Sniper, Movement, Battle, Push
- Modules: Bonus Attack, Initiative, Hit Strength, Shoot Strength, Malus
  Initiative, Net, Recon Center
- Armour
- Text interface
- Possibility to play on hotseat
- A bot making random decisions
- Several MCTS bots
- Two armies mirroring the Outpost and Moloch from the actual Neuroshima Hex
- Simple vectorization
- Initial successful ML models

### What remains to be done:

##### Bugs:
- Bots place Moloch's blocker in absurd places, seemingly not caring about preventing damage to their HQ

##### Code quality:
- The GameData class is a massive dump of public variables. Fix that!
- In many cases abstraction is broken quite deeply. 
  A method in order to work needs to access public methods of it's grandchildren classes and deeper.
  Fixing this requires a major refactor.
- Test coverage is not good enough. At the very least those elements should receive
  tests:
    - Monte Carlo Tree Search(MCTS and Node)
    - Vectorization
    - Various UsageFunctions, such as Bomb/Sniper
- Switch Tile from bazilion of constructors to method injection

##### Game implementation:

- Clown
- Option to discard&redraw when only instantaneous tiles were drawn

##### Artificial intelligence:

- A bot which actually uses ML to make decisions

##### Performance

- Allowing the game to be reversed, so MCTS doesn't have to make thousands of
  copies
- More profiling afterwards
- Possibly moving Tiles to stack instead of heap and devirtualization of some
  classes

### Explanation of the arcane stuff:

##### Dictionary:

- Turn - everything that happens from player A drawing his tiles until player B draws his tiles
- Tile - a single unit, or instant action like move/sniper
- Action/Battle action - shooting, hitting, mortar shots etc. Possibly passives??
- Tile usage function - function invoked when player decides to use the tile. Usually, this will be placing a tile on board

##### Code flow
Game is the main object, which creates and controls all the others.

##### How is tile implemented?
Tile is a class, every tile instiantates it. It's not meant to be extended. Rather than that, UsageFunction and BattleActions are separate objects, which are members of Tile. In the future it's possible that other classes will be defined. For examle, a TileDeath class might implement special behaviour which happens when the tile is killed.

##### Explanation of some classes
- GameState: Several subclasses make the game loop. Each of them exposes an advance_game method, which changes the game and set's appropriate next state. It's made that way so that AI has easy way to pause game, copy it, resume and query about possible moves.
- PlayerDecision: Not abstract, but fits here. A PlayerDecision is a standard decision that can be sent to game and update it.
- Action: Each new action like hitting or shooting needs to extend this class and store appropriate object in a Tile
- UsageFunction: 
    - Each new usage function like placing tile on board, using a sniper, or immediate move tile need to have appropiate UsageFunction, which will have it's change_board method executed upon usage.
    - change_board calls appropriate method in the Player class - this new method also needs to be implemented! 
    - Example: most tiles store PlaceOnBoard object as it's UsageFunction. When a player chooses to use such a tile, PlaceOnBoard.change_board() will call Player.pick_tile_target_location in order to discover tiles on which it is ought to place a tile. PlaceOnBoard.change_board() implements guard to ensure that player chooses proper tile. Player.pick_tile_target_location doesn't change the gamestate - it only returns coordinates and rotation, so PlaceOnBoard.change_board() can change the gamestate.
- PlayerLogic: an entity making decisions in game. HumanPlayerLogic and RandomPlayerLogic can be considered a reference implementations as of now. 
