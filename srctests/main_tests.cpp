#include <CloningTest.h>
#include <DamageDealingTest.h>
#include <EndgameTest.h>
#include <EqualityOperatorsTest.h>
#include <MedicTest.h>
#include <PassiveBonusTest.h>
#include <PushBackTest.h>
#include <SimpleBattleTest.h>
#include <Test.h>

#include <iostream>
#include <memory>
#include <vector>

using namespace std;

int
main()
{
    vector<unique_ptr<Test>> tests;
    tests.push_back(make_unique<SimpleBattleTest>());
    tests.push_back(make_unique<EqualityOperatorsTest>());
    tests.push_back(make_unique<CloningTest>());
    tests.push_back(make_unique<PassiveBonusTest>());
    tests.push_back(make_unique<EndgameTest>());
    tests.push_back(make_unique<DamageDealingTest>());
    tests.push_back(make_unique<PushBackTest>());
    tests.push_back(make_unique<MedicTest>());
    for (auto const& test : tests)
        test->make_tests();
    return 0;
}
